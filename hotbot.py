#! /usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2011 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""tagfix.py fix erroneous tag on osm objects"""

import sys
import syslog
from optparse import OptionParser
import OsmApi
from tagfix.tagfix import TagFix
#
from WmmDatabase import WmmDatabase
from WmmConfig import WmmConfig

__version__ = "0.6"

def arg_parse():
    """ Parse command line arguments """
    arg_list = "-p PASSWORD -d DATABASE [-fhoprstv] [-c FILENAME]"
    usage = "Usage: %prog " + arg_list
    parser = OptionParser(usage, version=__version__)
    parser.add_option("-c", "--config-file", dest="cfgfile",
                      help="read a configuration file", 
                      default="default.cfg")
    parser.add_option("-p", "--password", dest="password",
                      help="OSM api password", 
                      default=None)
    parser.add_option("-d", "--database", dest="database",
                      help="database name", 
                      default=None)
    parser.add_option("-s", "--show-config", dest="showconf", 
                      action="store_true", 
                      help="show and check configuration values and quit", 
                      default=False)
    parser.add_option("--simulate", dest="simulate", 
                      action="store_true", 
                      help="do nothing, just show wich countries will be done",
                      default=False)
    parser.add_option("-u", "--username", dest="username",
                      help="OSM api username", 
                      default=None)
    parser.add_option("-v", "--verbose", dest="verbose",
                      action="store_true", 
                      help="run in verbose mode", 
                      default=False)
    return parser.parse_args()[0]

def log_start():
    """ Write an entry in log system """
    cmd = ''
    for arg in sys.argv:
        cmd = cmd + ' ' + arg
    syslog.openlog("hotbot.py", syslog.LOG_PID, syslog.LOG_EMERG)
    syslog.syslog(cmd)


def checkopt(opts):
    """Check if the minimal options are presents

    Exit if a mandatory option is missing
    """
    errors = []
    if opts.username == None:
        errors.append("Need -u or --username on the command line")
    if opts.password == None:
        errors.append("Need -p or --password on the command line")
    if opts.database == None:
        errors.append("Need -d or --database on the command line")

    if len(errors) > 0:
        for err in errors:
            print err
        sys.exit(1)


def main():
    """Main function"""
    options = arg_parse()
    log_start()
    conf = WmmConfig(options.cfgfile)
    checkopt(options)
    conf.check()
    wamdb = WmmDatabase(options.database, conf.sqfiles, conf.sqfilesopt )
    fix = TagFix(wamdb)
    noderows = fix.getnodes()
    wayrows = fix.getways()
    chgsetid = 0

    if options.simulate:
        for node in noderows:
            print 'Replace "%s" key tag by "%s" in node %s' % (node)
        for way in wayrows:
            print 'Replace "%s" key tag by "%s" in way %s' % (way)
        sys.exit(0)
    else:
        if (len(noderows) + len(wayrows)) > 0:
            api = OsmApi.OsmApi(username=options.username, 
                                password=options.password)
            comment = u"HOT bot for fixing bad tag syntax"
            api.ChangesetCreate({u"comment": comment})
            fix.api = api
            fix.update()
            chgsetid = api.ChangesetClose()
            print "Nodes changed : %d" % fix.nodechanged
            print "Ways changed  : %d" % fix.waychanged
            print "Changeset id : %s " % chgsetid

            fix.printoutput(chgsetid)
            print fix.output
        else:
            print "That's cool there is nothing todo"


if __name__ == '__main__':
    main()
