# -*- coding: utf-8 -*-
#
# Copyright (c) 2011 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""Read datas from a Postgresql database"""

import psycopg2

def connect(dbname):
    '''Open an connection to the database'''
    conn = psycopg2.connect("dbname = " + dbname)
    conn.set_isolation_level(0) # autocommit 
    return conn

def psql_query(dbname, query, args = None):
    '''Open a connection and execute a query'''
    conn = connect(dbname)
    cur = conn.cursor()

    if (args == None):
        cur.execute(query)
    else:
        cur.execute(query, args)
    rows = cur.fetchall()

    return rows
    

def readcontinent(dbname):
    '''Read continent in database and return an array with continent name'''
    rows = psql_query (dbname, "SELECT continent, url FROM active_continents")
    return rows


def readcountries_by_priority(dbname):
    '''Read countries in database and return an array with continent name'''
    rows = psql_query (dbname, """SELECT * FROM countries_by_priority """)
    return rows


def readcountries_by_name(dbname):
    '''Read countries in database and return an array with continent name'''
    rows = psql_query (dbname, """SELECT * FROM countries_by_name """)
    return rows

def readcountries_by_continent(dbname):
    '''Read countries in database and return an array with continent name'''
    rows = psql_query (dbname, """SELECT * FROM countries_by_continent """)
    return rows


def readcountries(dbname, limit = None, min_priority = 0):
    '''Read countries in database and return an array with continent name'''
    conn = connect(dbname)
    cur = conn.cursor()

    if limit > 0:
        query = """SELECT id FROM country
                   WHERE priority >= %s LIMIT %s"""
        cur.execute(query, (min_priority, limit, ) )
    else:
        query = """SELECT id FROM country 
                   WHERE priority >= %s """
        cur.execute(query, (min_priority, ) )

    rows = cur.fetchall()

    return rows

def continent_from_country(dbname, country):
    """Return continent name for a country

    - dbname (string) : the database name
    - country (string) : the country name
    """
    conn = connect(dbname)
    cur = conn.cursor()
    query = """SELECT continent,country FROM countries WHERE country = %s"""
    cur.execute(query, (country, )  )
    row = cur.fetchone()

    return row[0]

def update_gen(dbname, country, action):
    '''Update date in db for an action'''
    conn = connect(dbname)
    cur = conn.cursor()

    if (action == "begin"):
        query = """UPDATE country SET genbegin = now() WHERE dir = %s"""
    else:
        query = """UPDATE country SET genend = now() WHERE dir = %s"""

    cur.execute(query, (country, ) )

def dbexists(dbname, name):
    """Return True/False based if a database exists or not"""
    conn = connect(dbname)
    cur = conn.cursor()
    query = "SELECT COUNT(*) FROM pg_catalog.pg_database WHERE datname = %s"
    cur.execute(query, (name, ) )
    return (cur.fetchone()[0] > 0)


def dbcreate(dbname, name):
    """Create a new database"""
    conn = connect(dbname)
    cur = conn.cursor()
    query = u"CREATE DATABASE %s" % name
    cur.execute(query)
    return dbexists(dbname, name)


def dbdrop(dbname, name):
    """Create a new database"""
    conn = connect(dbname)
    cur = conn.cursor()
    query = u"DROP DATABASE %s" % name
    cur.execute(query)
    return dbexists(dbname, name)
