# -*- coding: utf-8 -*-
#
# Copyright (c) 2011 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# pylint: disable=R0902
# pylint: disable=R0912
# pylint: disable=R0915
"""Read datas from a Postgresql database"""
import os
import sys
import ConfigParser
import syslog

class WmmConfig():
    """Options in config file"""

    def __init__(self, conffile):
        """Initialisation"""

        self.conffile = conffile
        self.force = False
        self.garmin = False
        self.garminopts = {}
        self.shapes = False
        self.downthreads = 4
        self.verbose = False
        self.tablespace = "pg_default"
        self.conturl = "http://download.geofabrik.de/osm"
        self.urlbase = "http://cm-downloads0.s3.amazonaws.com"
        self.osmobin  = "/usr/local/bin/osmosis"
        self.mkgmap  = "/usr/local/bin/mkgmap.jar"
        self.dbname = None
        self.wwwdir = None
        self.styldir = None
        self.webfiles = None
        self.planethost = None
        self.planetdir = None
        self.repdir = None
        self.java = None
        self.javaopts = None
        self.osm2pgsql  = "external/osm2pgsql/osm2pgsql"
        self.pgsql2shp  = "/usr/bin/pgsql2shp"
        if not os.path.exists(conffile):
            sys.exit("Config file %s does not exists" % conffile)

        self.sqfiles = []
        self.sqfilesopt = []
        self.files = []
        self.exes = []
        self.values = []
        self.parseconf()
        self.values.append( ("generate shape files", str(self.shapes) ) )
        self.values.append( ("generate garmin files", str(self.garmin) ) )
        self.values.append( ("database name", self.dbname) )
        self.values.append( ("download threads", self.downthreads) )
        self.exes.append( ("osm2pgsql path", self.osm2pgsql, "osm2pgsql") )
        self.urls = [ ("Continent url", self.conturl),
                      ("Country url", self.urlbase) ]
        self.dirs = [ ("Replication dir", self.repdir, "wwwdir"),
                      ("WWW document root", self.wwwdir, "repdir"),
                      ("WWW templates files", self.webfiles, "webfiles"),
                      ("osm2pgsql style dir", self.styldir, "stylesdir") ]

        self.files.append( ("osmosis binary path", self.osmobin, "osmobin") )

    def show(self):
        """Show config values"""
        print "Wamuma configuration"
        print "===================="
        print "read from file : %s" % self.conffile
        alls = [ self.values, self.dirs, self.files, 
                 self.urls, self.exes ]
        for bloc in alls:
            print "-"
            for conf in bloc:
                print "%-30s : %s" % (conf[0], conf[1])
        print "Checking conf ..."
        print "================="
        if self.check() == 0:
            print "OK"
        self.checkdb()
        print "OK"

    def check(self):
        """Check validity of values founds in conf file"""
        res = 0
        for xdir in self.dirs + self.files:
            res = checkdirectory(xdir[2], xdir[1])
            if res[0] > 0:
                sys.exit(res[1])
        for xexe in self.exes:
            res = checkexecutable(xexe[2], xexe[1])
            if res[0] > 0:
                sys.exit(res[1])
        return 0

    def checkdb(self):
        """Check the db connection"""
        if self.dbname != None:
            import psycopg2
        dsn = "dbname=%s" % self.dbname
        if self.__dict__['dbuser'] != None:
            dsn += " user=%s" % self.__dict__['dbuser']
        if self.__dict__['dbhost'] != None:
            dsn += " host=%s" % self.__dict__['dbhost']
        if self.__dict__['dbport'] != None:
            dsn += " port=%s" % self.__dict__['dbport']
        if self.__dict__['dbpass'] != None:
            print "Trying to connect : '%s password=####'" % dsn
            dsn += " password=%s" % self.__dict__['dbpass']
        else:
            print "Trying to connect : '%s' WITHOUT password" % dsn    
        conn = psycopg2.connect(dsn)
        conn.close()

    def parseconf(self):
        """Parse configuration file"""
        opts = self.readconfig("wamuma")
        
        for dbkey in ['dbuser', 'dbname', 'dbpass', 'dbhost', 'dbport']:
            if dbkey in opts:
                self.__dict__[dbkey] = opts[dbkey]
                self.values.append( ("database %s " % dbkey, 
                                     self.__dict__[dbkey]) )
            else:
                self.__dict__[dbkey] = None

        if 'conturl' in opts:
            self.conturl = opts['conturl']
 
        if 'tablespace' in opts:
            self.tablespace = opts['tablespace']

        if 'urlbase' in opts:
            self.urlbase = opts['urlbase']

        if 'downthreads' in opts:
            self.downthreads = opts['downthreads']

        if 'osmobin' in opts:
            self.osmobin = os.path.expanduser(opts['osmobin'])

        if 'wwwdir' in opts:
            self.wwwdir = os.path.expanduser(opts['wwwdir'])

        if 'stylesdir' in opts:
            self.styldir = os.path.expanduser(opts['stylesdir'])

        if 'webfiles' in opts:
            self.webfiles = os.path.expanduser(opts['webfiles'])

        if 'planetdir' in opts:
            self.planetdir = os.path.expanduser(opts['planetdir'])

        if 'planethost' in opts:
            self.planethost = opts['planethost']

        if 'repdir' in opts:
            self.repdir = os.path.expanduser(opts['repdir'])

        if 'osm2pgsql' in opts:
            self.osm2pgsql = os.path.expanduser(opts['osm2pgsql'])

        if 'pgisql' in opts:
            self.sqfiles.append(os.path.expanduser(opts['pgisql']))

        if 'spatial' in opts:
            self.sqfiles.append(os.path.expanduser(opts['spatial']))

        if 'intarray' in opts:
            self.sqfiles.append(os.path.expanduser(opts['intarray']))

        if 'hstore' in opts:
            self.sqfilesopt.append(os.path.expanduser(opts['hstore']))

        if 'osmoschemain' in opts:
            self.sqfilesopt.append(os.path.expanduser(opts['osmoschemain']))
            self.files.append(("osmosis sql schema",
                               os.path.expanduser(opts['osmoschemain']),
                               "osmoschemain"))

        if 'osmoschebbox' in opts:
            self.sqfilesopt.append(os.path.expanduser(opts['osmoschebbox']))
            self.files.append(("osmosis sql schema bbox",
                               os.path.expanduser(opts['osmoschebbox']),
                               "osmoschebbox"))

        if 'osmoscheline' in opts:
            self.sqfilesopt.append(os.path.expanduser(opts['osmoscheline']))
            self.files.append(("osmosis sql schema linestring",
                               os.path.expanduser(opts['osmoscheline']),
                               "osmoscheline"))

        if 'garmin' in opts:
            self.garmin = self.parsegarmin(opts)

        if 'shapes' in opts:
            self.shapes = self.parseshapes(opts)

    def parsejava(self):
        """Parse java section of configuration file"""
        res = False
        opts = self.readconfig("java")
        if 'java' in opts: 
            self.java = os.path.expanduser(opts['java'])
        if 'javaopts' in opts: 
            self.javaopts = os.path.expanduser(opts['javaopts'])
        self.exes.append( ("java binary path", self.java, "java") )
        self.values.append( ("java options", self.javaopts, "javaopts") )
        return res


    def parseshapes(self, gopts):
        """Parse shape generation options"""
        res = False
        if strisyes(gopts['shapes']):
            res = True
        opts = self.readconfig("shapes")
        if 'pgsql2shp' in opts: 
            self.pgsql2shp = os.path.expanduser(opts['pgsql2shp'])
        self.exes.append( ("pgsql2shp path", self.pgsql2shp, "pgsql2shp") )
        return res


    def parsegarmin(self, gopts):
        """Parse garmin map generation options"""
        self.parsejava()
        if strisyes(gopts['garmin']):
            lopts = self.readconfig("garmin")
            if 'mkgmap' in lopts:
                self.mkgmap = os.path.expanduser(lopts['mkgmap'])
                self.files.append( ("mkgmap jar file path", 
                                    self.mkgmap, "mkgmap") )
            if 'multi-language' in lopts:
                self.garminopts['multilang'] = strisyes(lopts['multi-language'])
            else:
                self.garminopts['multilang'] = False
            return True
        else:
            return False
            

    def readconfig(self, section):
        """Read a configuration file and return an object"""
        syslog.syslog("read config in %s" % self.conffile)
        config = ConfigParser.ConfigParser()
        config.readfp(open(self.conffile))
        return dict(config.items(section))

def checkdirectory(valuename, dirname):
    """Check validity of values founds in conf file"""
    if dirname == None:
        syslog.syslog("%s is not defined in conf file" % valuename)
    else:
        if not os.path.exists(dirname):
            return (1, "ERROR %s does not exists" % dirname)
    return (0, None)


def strisyes(alpha):
    """Check if a param is a yes equivalent value"""
    res = False
    if alpha.strip().lower() == 'yes':
        res = True
    if alpha.strip().lower() == 'true':
        res = True
    if alpha.strip().lower() == 'on':
        res = True
    if alpha.strip().lower() == '1':
        res = True
    return res

def checkexecutable(valuename, exename):
    """Check executables"""
    if not is_exe(exename):
        msg = "%s is not executable" % exename
        msg += ", please check param %s in your config file" % valuename
        return (1, msg)
    else:
        return (0, None)


def is_exe(fpath):
    """Return true if a file is executable"""
    return os.path.exists(fpath) and os.access(fpath, os.X_OK)


if __name__ == "__main__":
    
    CONF = WmmConfig("default.cfg.dist")
    print CONF.dbname
