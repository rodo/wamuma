#! /usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2011 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""wamuma.py main script to generate osm exports"""

# Ajouter dand l'api lors du release de passer la prio de l'objet 
# au max+1 de la liste des objets non modifiés

import sys
import os
import syslog
import threading
import time
from optparse import OptionParser

#
import LibExosm
import LibInfoFiles
import WmmPsql

from WmmDatabase import WmmDatabase

from WmmCountry import Country
from WmmConfig import WmmConfig
from WmmStatistics import WmmStatistics
from WmmPolygon import WmmPolygon
from WmmGarmin import GenGarmin
from WmmShapes import GenShapes
from WmmOsmosis import GenCountry
from WmmDownload import WmmDownloadCountry, WmmDownloadContinent

__version__ = "0.6"

def thread_garmin(country_obj, dir_name, conf):
    """Garmin map generation thread"""
    msg = "%s : start new thread for garmin map generation"
    syslog.syslog(msg % country_obj.dirname)
    background = GenGarmin(country_obj.dirname, dir_name, 
                           conf.mkgmap, [ conf.java, conf.javaopts ])
    background.start()
    if len(country_obj.langs):
        for lang in country_obj.langs:
            background = GenGarmin(country_obj.dirname, dir_name, 
                                   conf.mkgmap, 
                                   [ conf.java, conf.javaopts ])
            background.lang = lang
            background.start()

def thread_shapes(countobj, conf):
    """Shapefiles generation thread"""
    msg = "%s : start new thread for shapefiles generation"
    syslog.syslog(msg % countobj.dirname)
    background = GenShapes(conf, countobj)
    background.start()

def thread_bbox(country_name, dir_name):
    """Generate bboxfile"""
    syslog.syslog("%s : start new thread for bbox generation" % country_name)
    polyfile = os.path.join(dir_name, country_name + ".poly")
    bboxfile = os.path.join(dir_name, country_name + ".bbox")
    background = WmmPolygon(country_name, polyfile, bboxfile)
    background.start()

def thread_generate(contfilename, country_name, dir_name, osmobin):
    """Start new generation thread"""
    syslog.syslog("%s : start new thread" % country_name)
    background = GenCountry(contfilename, country_name, dir_name, 
                            osmobin)
    background.start()

def thread_statistics(conf, path, scountry):
    """Start new generation thread"""
    syslog.syslog("%s : start new thread for statistics" % scountry.dirname)
    osmfilename = "%s/%s.osm" % (path, scountry.dirname)
    background = WmmStatistics(conf, osmfilename, scountry, path)
    background.start()


def thread_download(url_base, country_name, dir_name, fullurl):
    """ Start new download thread """
    syslog.syslog("%s : start new thread for downloading files" % country_name)
    background = WmmDownloadCountry(url_base, country_name, dir_name, fullurl)
    background.start()

def thread_downloadcont(url, dir_name, continent_name):
    """ Start new thread """
    syslog.syslog("%s : start new continent_thread for download" % url)
    background = WmmDownloadContinent(url, dir_name, continent_name)
    background.start()

def thread_replicate(host, repdir, localdir):
    """ Start new thread """
    from WmmReplicate import ThreadReplicate
    syslog.syslog("start new replicate thread")
    rep = ThreadReplicate(host, repdir, localdir)
    rep.start()

def arg_parse():
    """ Parse command line arguments """
    arg_list = "-d -g [-fhoprstv] [-p NUM] [-c FILENAME]"
    usage = "Usage: %prog " + arg_list
    parser = OptionParser(usage, version=__version__)
    parser.add_option("-c", "--config-file", dest="cfgfile",
                      help="read a configuration file", 
                      default="default.cfg")
    parser.add_option("-d", "--download", dest="download",
                      action="store_true", 
                      help="downloads data files from internet", 
                      default=False)
    parser.add_option("-f", "--force", 
                      action="store_true", dest="force", 
                      help="force to regenerate files even if they exists",
                      default=False)
    parser.add_option("-g", "--generate", 
                      action="store_true", dest="generate", 
                      help="filename", default=False)
    parser.add_option("-i", "--only-index", dest="index", 
                      action="store_true", 
                      help="generate the indexes html files and quit",
                      default=False)
    parser.add_option("--init-db", dest="initdb", 
                      action="store_true", 
                      help="initialize the main database",
                      default=False)
    parser.add_option("-o", "--only-one", dest="onlyone", 
                      action="store_true",
                      help="generate only the first", 
                      default=False)
    parser.add_option("-p", "--min-priority", dest="min_priority",
                      help="minimal priority (default 2)", default=2)
    parser.add_option("-r", "--replicate", dest="replicate",
                      action="store_true", 
                      help="use replicate files", 
                      default=False)
    parser.add_option("-s", "--show-config", dest="showconf", 
                      action="store_true", 
                      help="show and check configuration values and quit", 
                      default=False)
    parser.add_option("--simulate", dest="simulate", 
                      action="store_true", 
                      help="do nothing, just show wich countries will be done",
                      default=False)
    parser.add_option("--no-statistics", dest="dostats", 
                      action="store_false", 
                      help="do not genereate statistics, (default False)",
                      default=True)
    parser.add_option("-t", "--gen-thread", 
                      dest="genthread", 
                      help="maximum number of generation thread to launch",
                      default=4)
    parser.add_option("-v", "--verbose", dest="verbose",
                      action="store_true", 
                      help="run in verbose mode", 
                      default=False)
    return parser.parse_args()[0]

def log_start():
    """ Write an entry in log system """
    cmd = ''
    for arg in sys.argv:
        cmd = cmd + ' ' + arg

    syslog.openlog("wamuma.py", syslog.LOG_PID, syslog.LOG_EMERG)
    syslog.syslog(cmd)

def wait_threads(nbslots=1):
    """ Wait for thread to finish

    Sleeps until there is a number of running child thread superior of nbslots

    Parms :
     - nbslots : (integer)
    """
    counter = 0
    nbt = threading.activeCount()
    while (nbt > nbslots):
        nbt = threading.activeCount()
        counter += 1
        time.sleep(1)
        if (counter > 120): # wait two minute between each log insert
            counter = -1
            syslog.syslog("Waiting for %d threads to finish" % ( nbt - 1 ))
            for thr in threading.enumerate():
                if thr.name != "MainThread":
                    syslog.syslog("Waiting for thread %s to finish" % thr.name)

def download_continent(conf):
    """Download continent datas"""
    dbname = conf.dbname 
    host = conf.conturl
    nbslots = conf.downthreads
    rows = WmmPsql.readcontinent(dbname)
    syslog.syslog("Read %d continent's name" % len(rows))
    for row in rows:
        continent_url = "%s/%s.osm.bz2" % (host, row[0])
        dir_name = os.path.join(conf.wwwdir, row[0])
        wait_threads(nbslots)
        thread_downloadcont(continent_url, dir_name, row[0])

def write_indexes(dir_name, dbname):
    """Write indexes"""
    LibInfoFiles.countries_by_name(dir_name, dbname)
    LibInfoFiles.countries_by_priority(dir_name, dbname)
    LibInfoFiles.countries_by_continent(dir_name, dbname)

def write_description(dir_name, countobj):
    """Write indexes"""

    extensions = [ ("osm.bz2", "OSM compressed datas"),
                   ("img.zip", "Garmin maps") ]
    shp = GenShapes()

    for style in shp.getstyles():
        extensions.append(("%s.zip" % (style),
                           "Shapefiles")
                          )
    for lang in countobj.langs:
        extensions.append(("img.%s.zip" %lang, "Garmin maps"))

    with open(os.path.join(dir_name, ".htaccess"),'w') as htac:
        for filext in extensions:
            htac.write('AddDescription "%s" %s.%s' % (filext[1], 
                                                      countobj.dirname, 
                                                      filext[0])
                       )
            htac.write("\n")

def country(pays, opts, conf ):
    """Generate country data

    Params:
     - pays : Country object()
     
    """
    WmmPsql.update_gen(conf.dbname, pays.dirname, "begin")
    dirname = os.path.join(conf.wwwdir, pays.continent, pays.dirname)
    LibExosm.checkdir(dirname)
    LibExosm.checkdir(os.path.join(dirname,'logs'))
    wait_threads(opts.genthread)
    write_description(dirname, pays)
    if conf.garmin:
        thread_garmin(pays, dirname, conf)
    if conf.shapes:
        thread_shapes(pays, conf)
    if opts.dostats:
        thread_statistics(conf, dirname, pays)

    WmmPsql.update_gen(conf.dbname, pays.dirname, "end")        

def gendown(pays, opts, conf ):
    """Generate country data

    Params:
     - pays : Country Object()
     - opts
     - conf
    """
    continent = pays.continent
    dirname = os.path.join (conf.wwwdir, continent, pays.dirname)
    LibExosm.checkdir(dirname)

    urlbase = "%s/%s/%s" % (conf.urlbase, continent, pays.dirname)

    contfilename = "%s/%s/%s.osm.bz2" % (conf.wwwdir , 
                                         continent, 
                                         continent)
    wait_threads(opts.genthread)
    if opts.generate:
        thread_generate(contfilename, pays.dirname, dirname, 
                        conf.osmobin)
    if opts.download:
        thread_download(urlbase, pays.dirname, dirname, pays.url)

    thread_bbox(pays.dirname, dirname)

def showconf(conf):
    """Show the configuration and exit"""
    conf.show()
    sys.exit()

def initmaindb(conf):
    """Create the main database"""
    pgdb = WmmDatabase("postgres", conf.sqfiles, conf.sqfilesopt )
    pgdb.create(conf.dbname, True)
    wamdb = WmmDatabase(conf.dbname, conf.sqfiles, conf.sqfilesopt )
    wamdb.initnew(True)
    wamdb.initmaindb(True)
    sys.exit(0)

def main():
    """Main function"""
    options = arg_parse()
    log_start()
    conf = WmmConfig(options.cfgfile)
    conf.force = options.force
    conf.verbose = options.verbose
    if options.showconf:
        showconf(conf)
    conf.check()
    if options.initdb:
        initmaindb(conf)
    write_indexes(conf.wwwdir, conf.dbname)
    if options.index:
        sys.exit(0)
    if options.replicate:
        thread_replicate(conf.planethost, conf.planetdir, conf.repdir)
    if options.download and options.generate:
        download_continent(conf)
    wait_threads()
    wamdb = WmmDatabase(conf.dbname, conf.sqfiles, conf.sqfilesopt )
    wamdb.tablespace = conf.tablespace
    rows = WmmPsql.readcountries(conf.dbname, 
                                 options.onlyone, options.min_priority)
    syslog.syslog("Run for %d countries" % len(rows))
    if options.simulate:
        for data in rows:
            pays = Country(data[0], wamdb, options.verbose)
            print '- %s : %s %s' % (pays.dirname, pays.dbname, pays.url)
        sys.exit(0)
    for data in rows:
        pays = Country(data[0], wamdb, options.verbose)
        pays.initdb()
        gendown(pays, options, conf)
    wait_threads()
    for data in rows:
        pays = Country(data[0], wamdb, options.verbose)
        country(pays, options, conf)
    syslog.syslog("End of work")

if __name__ == '__main__':
    main()
