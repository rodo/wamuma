/**
 *
 * Copyright (c) 2011 Rodolphe Quiédeville <rodolphe@quiedeville.org>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

var graphs_loaded = new Array()
graphs_loaded['#tabs-orphan'] = 0;
graphs_loaded['#tabs-users'] = 0 ;
graphs_loaded['#tabs-nodes'] = 0 ;
graphs_loaded['#tabs-ways'] = 0 ;
graphs_loaded['#tabs-relations'] = 0 ;

$(function() {
    $('#tabs' ).tabs();
});

$('#tabs').bind('tabsselect', function(event, ui) {

    // Objects available in the function context:
    // ui.tab     // anchor element of the selected (clicked) tab
    // ui.panel   // element, that contains the selected/clicked tab contents
    // ui.index   // zero-based index of the selected (clicked) tab

    var name = nameFromTabs(ui.tab);

    if (name == '#tabs-orphan' && graphs_loaded[name] == 0) {
	$.getJSON("datas.php?data=nodes_orphaned_without_tag&format=json",
		  function(d) {
		      $.plot($("#graphorphnodes"), 
			     d,
			     { 
				 xaxis: { mode: "time" },
				 yaxis: { position: "right" } ,
				 grid: { markings: markings },
				 legend: { position: "nw" }
			     });
		  });    
    }
    
    if (name == '#tabs-users' || name == '#tabs-nodes' || name == '#tabs-relations' || name == '#tabs-ways') {

	id = idFromName(name);

	if (graphs_loaded[name] == 0) {
	    loadgraph(id);
	}
    }
    
    graphs_loaded[name] = 1;
});

function nameFromTabs(tabs) {

    var val = tabs + '';
    return val.split('/').pop();
    
}

function idFromName(name) {
    
    return name.split('-').pop();
    
}
