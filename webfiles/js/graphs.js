/**
 *
 * Copyright (c) 2011 Rodolphe Quiédeville <rodolphe@quiedeville.org>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

var markings = [
    { color: '#ff7c7c', yaxis: { from: 1000 } },
    { color: '#ffffbb', yaxis: { from: 500, to: 1000 } },
    { color: '#7cff7c', yaxis: { to: 500 } }
];

$(function () {
    /*
     *
     */
    $.getJSON("datas.php?data=nodes_duplicated&format=json",
	      function(d) {		  
		  $.plot($("#graphdupnodes"), 
			 d, 
			 { 
			     xaxis: { mode: "time" },
                             yaxis: { position: "right" },
			     grid: { markings: markings },
			     legend: { position: "nw" }
			 });
	      });    

    /*
     *
     */

});

function loadgraph(name) {

    id = "#graph" + name;

    $.getJSON("datas.php?data="+name+"&format=json",
	      function(d) {
		  
		  $.plot($(id), 
			 d, 
			 { 
			     color: "#FF0000",
			     xaxis: { mode: "time" },
                             yaxis: { position: "right" } ,
			     legend: { position: "nw" }
			 });

		  $("#whole" + name).click(function () {
		      $.plot($(id), d, 
			     { xaxis: { mode: "time" },
                               yaxis: { position: "right" } 
			     });
		  });
		  
		  $("#oneday" + name).click(function () {
		      $.plot($(id), d, {
			  xaxis: {
			      mode: "time",
			      min: (new Date()).getTime() - ( 24 * 3600 * 1000),
			      max: (new Date()).getTime()
			  } ,
                          yaxis: { position: "right" } ,
		      });
		  });

		  $("#oneweek" + name).click(function () {
		      $.plot($(id), d, {
			  xaxis: {
			      mode: "time",
			      min: (new Date()).getTime() - ( 7 * 24 * 3600 * 1000),
			      max: (new Date()).getTime()
			  } ,
                          yaxis: { position: "right" } ,
		      });
		  });

	      });    
};