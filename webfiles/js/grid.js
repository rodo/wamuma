/**
 *
 * Copyright (c) 2011 Rodolphe Quiédeville <rodolphe@quiedeville.org>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

$('.flexme').flexigrid(
    {	
	url: 'tabkeys.json',
	method: 'GET',
	dataType: 'json',
	colModel : [
	    {display: 'key', name : 'key', width : 280, sortable : true, align: 'left'},
	    {display: 'number', name : 'number', width : 140, sortable : true, align: 'right'},	    
	],
	
	searchitems : [
	    {display: 'key', name : 'k'},
	],

	usepager: true,
	singleSelect: false,
	title: 'Keys found in nodes/ways',
	useRp: true,
	rp: 15,
	showTableToggleBtn: true,
	width: 700,
	height: 400	
    });

$('.flexshape').flexigrid(
    {	
	url: 'shp_convert.json',
	method: 'GET',
	dataType: 'json',
	colModel : [
	    {display: 'osm key', name : 'osm', width : 280, sortable : true, align: 'left'},
	    {display: 'shapefile attribute', name : 'shp', width : 140, sortable : true, align: 'left'},
	],
	
	searchitems : [
	    {display: 'osm', name : 'osm'},
	],

	usepager: true,
	singleSelect: false,
	title: 'OSM key name, shapefile attribute conversion',
	useRp: true,
	rp: 15,
	showTableToggleBtn: true,
	width: 700,
	height: 400	
    });

$.getJSON("shp_convert.json",
	  function(d) {	
	      var anchorTag = document.createElement('a');
	      
	      anchorTag.appendChild(document.createTextNode("datas.csv"));
	      
	      anchorTag.setAttribute('name', 'Example');
	      anchorTag.setAttribute('class', 'data');
	      anchorTag.setAttribute('href', 'json2csv.php?rp=' + d.total + '&json=shp_convert');
	      
	      document.getElementById('tabs-shape').appendChild(anchorTag);    
	  });    



function readvalues(key) {
    
    $('.flexvalues').flexigrid(
	{	
	    url: 'tabkeys.json?value=' + key,
	    method: 'GET',
	    dataType: 'json',
	    colModel : [
		{display: 'value', name : 'value', width : 280, sortable : false, align: 'left'},
		{display: 'number', name : 'number', width : 140, sortable : false, align: 'right'},	    
	    ],
	    usepager: true,
	    singleSelect: false,
	    title: 'Values for ' + key,
	    useRp: true,
	    rp: 15,
	    showTableToggleBtn: true,
	    width: 700,
	    height: 400	
	});

    $('.flexvalues').flexReload(
	{	
	    url: 'tabkeys.json?value=' + key,
	    method: 'GET',
	    dataType: 'json',
	    colModel : [
		{display: 'value', name : 'value', width : 280, sortable : false, align: 'left'},
		{display: 'number', name : 'number', width : 140, sortable : false, align: 'right'},	    
	    ],
	    usepager: true,
	    singleSelect: false,
	    title: 'Values for ' + key,
	    useRp: true,
	    rp: 15,
	    showTableToggleBtn: true,
	    width: 700,
	    height: 400	
	});

    
}