/**
 *
 * Copyright (c) 2011 Rodolphe Quiédeville <rodolphe@quiedeville.org>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

var map;

function displayLayer(layername, visible) {
    /*
      Display or hide a layer
     */
    var name = 'Haiti:' + layername;

    var lay = map.getLayersByName(name);

    lay[0].setVisibility ( visible );
}


function addGeoServerLayer(layername, opacity) {
    
    opacity = typeof(opacity) != 'undefined' ? opacity : 1.0;


    var shelter = new OpenLayers.Layer.WMS(layername,
					   "http://osm.fsffrance.org:8080/geoserver/gwc/service/wms",
					   {
					       layers: layername, 
					       format: 'image/png' 
					   },
					   
					   { tileSize: new OpenLayers.Size(256,256),
					     isBaseLayer: false,
					     buffer: 0,
					     opacity: opacity
					   }
					  );

    return shelter;    
}

function init(zoom, lon, lat){

    var mapOptions = { 
	resolutions: [156543.03390625, 78271.516953125, 39135.7584765625, 19567.87923828125, 9783.939619140625, 4891.9698095703125, 2445.9849047851562, 1222.9924523925781, 611.4962261962891, 305.74811309814453, 152.87405654907226, 76.43702827453613, 38.218514137268066, 19.109257068634033, 9.554628534317017, 4.777314267158508, 2.388657133579254, 1.194328566789627, 0.5971642833948135, 0.29858214169740677, 0.14929107084870338, 0.07464553542435169, 0.037322767712175846, 0.018661383856087923, 0.009330691928043961, 0.004665345964021981],
	projection: new OpenLayers.Projection('EPSG:900913'),
	maxExtent: new OpenLayers.Bounds(-2.003750834E7,-2.003750834E7,2.003750834E7,2.003750834E7),
	units: "meters",
	controls: [ new OpenLayers.Control.Navigation(), 
		    new OpenLayers.Control.PanZoomBar(), 
		    new OpenLayers.Control.LayerSwitcher(),
		    new OpenLayers.Control.Attribution() ]
    };

    map = new OpenLayers.Map('map', mapOptions );

    // Base Layer from OSM
    var layerOSM = new OpenLayers.Layer.OSM('OSM',
					    'http://b.tile.cartosm.eu/${z}/${x}/${y}.png',
					    { isBaseLayer: true,
					      buffer: 0
					    });
    



    // Add base layer
    map.addLayer( layerOSM );

    //    
    map.addLayer( addGeoServerLayer( 'Haiti:orc_line', 0.5 ));
    map.addLayer( addGeoServerLayer( 'Haiti:ctc_line', 0.5 ));
    map.addLayer( addGeoServerLayer( 'Haiti:ctu_line', 0.5 ));
    map.addLayer( addGeoServerLayer( 'Haiti:disaster_shelter_line', 0.5 ));


    map.addLayer( addGeoServerLayer( 'Haiti:orc' ));
    map.addLayer( addGeoServerLayer( 'Haiti:ctc' ));
    map.addLayer( addGeoServerLayer( 'Haiti:ctu' ));
    map.addLayer( addGeoServerLayer( 'Haiti:disaster_shelter' ));


    map.addLayer( addGeoServerLayer( 'Haiti:well' ));
    map.addLayer( addGeoServerLayer( 'Haiti:tap' ));
    map.addLayer( addGeoServerLayer( 'Haiti:toilet' ));
    map.addLayer( addGeoServerLayer( 'Haiti:shower' ));
    map.addLayer( addGeoServerLayer( 'Haiti:bladder' ));
    map.addLayer( addGeoServerLayer( 'Haiti:borehole' ));

    var center = new OpenLayers.LonLat(lon,lat).transform(new OpenLayers.Projection("EPSG:4326"), new OpenLayers.Projection("EPSG:900913"));

    map.setCenter(center, zoom);
}

