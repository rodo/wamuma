<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><!-- -*- mode: html -*- -->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
<?PHP include('conf.php'); ?>
 <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Data extraction and statistics for <?PHP print $name; ?></title>
    <link type="text/css" rel="stylesheet" href="css/layout.css" />

    <link type="text/css" rel="stylesheet" href="css/flexigrid.css" />
    <link type="text/css" rel="stylesheet" href="css/jquery.splitter.css" />
    <link type="text/css" rel="stylesheet" href="css/Aristo/jquery-ui-1.8.7.custom.css" />
    <script language="javascript" type="text/javascript" src="js/OpenLayers.js"></script>
    <!-- wamuma libs -->
    <script language="javascript" type="text/javascript" src="js/map.js"></script>
    <script language="javascript" type="text/javascript" src="js/extlibs/date.js"></script>
    <!-- jquery tools -->
    <script language="javascript" type="text/javascript" src="js/extlibs/jquery.js"></script>
    <script language="javascript" type="text/javascript" src="js/extlibs/jquery-ui.min.js"></script>
    <script language="javascript" type="text/javascript" src="js/extlibs/jquery.flot.js"></script>
    <script language="javascript" type="text/javascript" src="js/extlibs/jquery.splitter.js"></script>
    <script language="javascript" type="text/javascript" src="js/extlibs/flexigrid.js"></script>
 </head>
 <body onload="init(<?PHP print $mapcenter; ?>)">
   <div>
     <h1><a href="../"><span class="ui-icon ui-icon-arrowthick-1-n"></span></a>Data extraction and statistics for <?PHP print $name; ?></h1>
   </div>
   
   <div class="container">

     <div id="tabs">
       <ul>
	 <li><a href="#tabs-map">Humanitarian map</a></li>
	 <li><a href="#tabs-1">Graph keys/nodes</a></li>
	 <li><a href="#tabs-keys">Keys</a></li>
	 <li><a href="#tabs-nodes">Nodes</a></li>
	 <li><a href="#tabs-ways">Ways</a></li>
	 <li><a href="#tabs-relations">Relations</a></li>
	 <li><a href="#tabs-orphan">Orphan nodes</a></li>
	 <li><a href="#tabs-7">Dupl. nodes</a></li>
	 <li><a href="#tabs-users">Users</a></li>
	 <li><a href="#tabs-shape">Shapefiles</a></li>
       </ul>

       <div id="tabs-map">
	 <div id="maplegend">
	   <ul>
	     <li>
	       <input type="checkbox" onClick="displayLayer('ctu', this.checked)" checked="true" />
	       <img src="http://osm.fsffrance.org:8080/geoserver/wms?REQUEST=GetLegendGraphic&VERSION=1.0.0&FORMAT=image/png&WIDTH=20&HEIGHT=20&STRICT=false&style=square_green" />
	       Cholera treatment unit
	     </li>
	     <li>
	       <input type="checkbox" onClick="displayLayer('ctc', this.checked)" checked="true" />
	       <img src="http://osm.fsffrance.org:8080/geoserver/wms?REQUEST=GetLegendGraphic&VERSION=1.0.0&FORMAT=image/png&WIDTH=20&HEIGHT=20&STRICT=false&style=square_yellow" />
	       Cholera treatment center</li>
	     <li>
	       <input type="checkbox" onClick="displayLayer('orc', this.checked)" checked="true" />
	       <img src="http://osm.fsffrance.org:8080/geoserver/wms?REQUEST=GetLegendGraphic&VERSION=1.0.0&FORMAT=image/png&WIDTH=20&HEIGHT=20&STRICT=false&style=square_blue" />
	       Oral rehydratation centre
	     </li>
	   </ul>
	   <ul>
	     <li>
	       <input type="checkbox" onClick="displayLayer('disaster_shelter', this.checked)" checked="true" />
	       <img src="http://osm.fsffrance.org:8080/geoserver/wms?REQUEST=GetLegendGraphic&VERSION=1.0.0&FORMAT=image/png&WIDTH=20&HEIGHT=20&STRICT=false&style=triangle_green" />
	       Disaster shelter</li>
	   </ul>
	   <ul>
	     <li>
	     <li>
	       <input type="checkbox" onClick="displayLayer('well', this.checked)" checked="true" />
	       <img src="http://osm.fsffrance.org:8080/geoserver/wms?REQUEST=GetLegendGraphic&VERSION=1.0.0&FORMAT=image/png&WIDTH=20&HEIGHT=20&STRICT=false&style=circle_yellow" />Well
	     </li>
	     <li>
	       <input type="checkbox" onClick="displayLayer('borehole', this.checked)" checked="true" />
	       <img src="http://osm.fsffrance.org:8080/geoserver/wms?REQUEST=GetLegendGraphic&VERSION=1.0.0&FORMAT=image/png&WIDTH=20&HEIGHT=20&STRICT=false&style=circle_blue" />Borehole</li>
	     <li>
	       <input type="checkbox" onClick="displayLayer('tap', this.checked)" checked="true" />
	       <img src="http://osm.fsffrance.org:8080/geoserver/wms?REQUEST=GetLegendGraphic&VERSION=1.0.0&FORMAT=image/png&WIDTH=20&HEIGHT=20&STRICT=false&style=circle_red" />Tap</li>
	     <li>
	       <input type="checkbox" onClick="displayLayer('bladder', this.checked)" checked="true" />
	       <img src="http://osm.fsffrance.org:8080/geoserver/wms?REQUEST=GetLegendGraphic&VERSION=1.0.0&FORMAT=image/png&WIDTH=20&HEIGHT=20&STRICT=false&style=circle_green" />Bladder</li>
	     <li>
	       <input type="checkbox" onClick="displayLayer('shower', this.checked)" checked="true" />
	       <img src="http://osm.fsffrance.org:8080/geoserver/wms?REQUEST=GetLegendGraphic&VERSION=1.0.0&FORMAT=image/png&WIDTH=20&HEIGHT=20&STRICT=false&style=circle_pink" />
	       Shower
	     </li>
	     <li>
	       <input type="checkbox" onClick="displayLayer('toilet', this.checked)" checked="true" />
	       <img src="http://osm.fsffrance.org:8080/geoserver/wms?REQUEST=GetLegendGraphic&VERSION=1.0.0&FORMAT=image/png&WIDTH=20&HEIGHT=20&STRICT=false&style=circle_orange" />
	       Toilet
	     </li>
	   </ul>
	 </div>
	 <div id="map"></div>

	 <div id="mapfoot">
	   Made with : <a href="http://geoserver.org/">GeoServer</a>, <a href="http://www.openlayers.org">OpenLayers</a>, <a href="http://geowebcache.org/">GeoWebCache</a>
	 </div>

       </div>
       	 
       <div id="tabs-1">
	 <div id="placeholder" style="width:720px;height:400px;"></div>
   
	 <p class="button">Zoom to : 
	   <button id="whole">Whole period</button> 
	   <button id="today">today</button> 
	 </p>
	 <p class="urldatas">
	   Datas in JSON format : <a class="data" href="datas.php?data=nodes_keys_distinct&format=json">datas.json</a>
	 </p>
       </div>
       <div id="tabs-keys">
	   <table class="flexme"></table>
	   <a class="data" href="tabkeys.json">datas.json</a>,
	   <a class="data" href="keys-csv.php">CSV datas</a>
	   <table class="flexvalues"></table>
       </div>
       <div id="tabs-nodes">
	 <div id="graphnodes" style="width:720px;height:400px;"></div>
	 <p class="button">Zoom to: 
	   <button id="wholenodes">Whole period</button> 
	   <button id="onedaynodes">Last day</button> 
	   <button id="oneweeknodes">Last week</button> 
	 </p>
	 <p>
	   <a class="data" href="datas.php?data=nodes&format=json">datas</a>
	 </p>
       </div>

       <div id="tabs-ways">
	 <div id="graphways" style="width:720px;height:400px;"></div>
	 <p class="button">Zoom to: 
	   <button id="wholeways">Whole period</button> 
	   <button id="onedayways">Last day</button> 
	   <button id="oneweekways">Last week</button> 
	 </p>
	 <p>
	   <a class="data" href="datas.php?data=ways&format=json">datas</a>
	 </p>
       </div>

       <div id="tabs-relations">
	 <div id="graphrelations" style="width:720px;height:400px;"></div>
	 <p class="button">Zoom to: 
	   <button id="wholerelations">Whole period</button> 
	   <button id="onedayrelations">Last day</button> 
	   <button id="oneweekrelations">Last week</button> 
	 </p>
	 <p>
	   <a class="data" href="datas.php?data=relations&format=json">datas</a>
	 </p>
       </div>

       <div id="tabs-orphan">
	 <div id="graphorphnodes" style="width:720px;height:400px;"></div>
	 <p>
	   <a class="data" href="datas.php?data=nodes_orphaned_without_tag&format=json">datas</a>
	 </p>
       </div>

       <div id="tabs-7">
	 <div id="graphdupnodes" style="width:720px;height:400px;"></div>
	 <p>
	   <a class="data" href="datas.php?data=nodes_duplicated&format=json">datas</a>
	 </p>
       </div>

       <div id="tabs-users">
	 <div id="graphusers" style="width:720px;height:400px;"></div>
	 <p class="button">Zoom to: 
	   <button id="wholeusers">Whole period</button> 
	   <button id="onedayusers">Last day</button> 
	   <button id="oneweekusers">Last week</button> 
	 </p>
	 <p>
	   <a class="data" href="datas.php?data=users&format=json">datas</a>
	 </p>
       </div>

       <div id="tabs-shape">
	   <table class="flexshape"></table>
	   <a class="data" href="shp_convert.json">datas.json</a>,
       </div>

     </div>
     
   </div><!-- End container -->


   <script language="javascript" type="text/javascript" src="js/tabs.js"></script>
   <script language="javascript" type="text/javascript" src="js/grid.js"></script>
   <script language="javascript" type="text/javascript" src="js/graphs.js"></script>
   <script language="javascript" type="text/javascript" src="js/keys.js"></script>
 </body>
</html>
