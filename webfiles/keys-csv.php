<?PHP // -*- mode: php; -*-
/* 
 * Copyright (c) 2011 Rodolphe Quiédeville <rodolphe@quiedeville.org>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 *
 */
header('Content-type: application/csv');
header("Cache-Control: no-cache, must-revalidate");
header("Expires: Mon, 25 Apr 2011 05:00:00 GMT"); // 

include("libs.php");
include("dblibs.php");

$conn = dbconn($dsn);

$flexi = new FlexiParser($_GET);
$options = $flexi->options;

if ($conn) {

  $query = sprintf("SELECT k, count(*) FROM stat GROUP BY k ORDER BY %s %s", 
		   $options['sortname'], 
		   $options['sortorder']);
  
  $result = pg_query($conn, $query);
  if (!$result) {
    echo "Une erreur est survenue.\n";
    exit;
  } else {
    while ($row = pg_fetch_array($result)) {
      printf ('"%s",%s', $row[0], $row[1] );
      print "\n";
    }
    
  }
}
?>
