<?PHP
/*
 * Copyright (c) 2011 Rodolphe Quiédeville <rodolphe@quiedeville.org>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * conf.php contains two vars definition like 
 * $dsn = "host=localhost dbname=foobar user=bar password=foo";
 * $logfile = "../logs/stats_foobar.txt";
 * $mapcenter = "12,-72,18";
 */
if (file_exists('conf.php')) {
  include('conf.php');
}

include('phplibs/FlexiParser.php');
include('phplibs/LogAnalyzer.php');


Class GraphDatasJSON
{
  public $out;
  public $version;

  public function __construct($legend, $datas, $color = null) {
    /*
     * Return an array
     */
    $this->version = "1.0.1";

    header('Content-type: application/json');
    header("Cache-Control: no-cache, must-revalidate");
    header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");

    $out = $this->jsonformat($this->splitdatas($datas), $legend, $color);
    
    $this->out = $out;
  }


  public function jsonformat($datas, $legend, $color) {

    $leg[0] = " (in nodes)";
    $leg[1] = " (in ways)";

    if (sizeof($datas[0]) > 2) {

      for ($i = 0 ; $i < sizeof($datas) ; $i++) {
	$out[$i] = array("label" => $legend . $leg[$i],
			 "data" => $datas[$i]);
	
      }
    } else {

      $out[0] = array("label" => $legend,
		      "data" => $datas,
		      "color" => $color);

    }
    
    return json_encode($out);
  }



  public function splitdatas($datas) {

    $sl = sizeof($datas);
    $sc = sizeof($datas[0]) - 1;
    $i = 0;
    if ($sc > 1) {
      while ($i < $sl) {

	$exda[0][$i][0] = $datas[$i][0];
	$exda[0][$i][1] = $datas[$i][1];

	$exda[1][$i][0] = $datas[$i][0];
	$exda[1][$i][1] = $datas[$i][2];

	$i++;
      }

    } else {
      $exda = $datas;
    }

    return $exda;
  }
}


Class SqlQueries
{

  public function __construct($conn = null, $memcache = False) {
    $this->version = "1.1.0";
    $this->conn = $conn;
    $this->memcache = $memcache;
    $this->rows = null;
    $this->cached = null;
    $this->key = null;
  }

  public function cache($datas) {
    if ( $this->memcache ) {
      $this->memcache->set($this->key, $datas, 0, 60 * 10);
    }
  }


  public function pg_query_all($query) {

    $result = -1;

    $this->key = $this->md5query($query);

    if ( $this->memcache ) {
      $val = $this->memcache->get($this->key);
      if ( $val ) {
	$this->cached = $val;
	$result = 0;
      }
    }

    if ( $result == -1 ) {
      $result = pg_query($this->conn, $query);
      $this->rows = pg_fetch_all($result);
      $result = 1;
    }

    return $result;
  }

  public function whorder($options) {
    /* Return the sql query string */
    $query = '';

    if ( $options['where'] != null ) {
      $query .= " " . $options['where'];
    }

    $query .= sprintf(" ORDER BY %s %s LIMIT %s OFFSET %s", 
		      $options['sortname'], 
		      $options['sortorder'], 
		      $options['limit'], 
		      $options['offset'] );

    return $query;    
  }


  public function tabkeys($options) {
    /* Return the sql query string */
    $query = sprintf("SELECT k, count(*) FROM stat");

    if ( $options['where'] != null ) {
      $query .= " " . $options['where'];
    }

    $query .= sprintf(" GROUP BY k ORDER BY %s %s LIMIT %s OFFSET %s", 
		      $options['sortname'], 
		      $options['sortorder'], 
		      $options['limit'], 
		      $options['offset'] );

    return $query;    
  }

  public function tabvalues($options) {
    /* Return the sql query string */
    $query = sprintf("SELECT v, count(*) FROM stat");

    if ( $options['where'] != null ) {
      $query .= " WHERE k = '" . $options['value']."'";
    }

    $query .= sprintf(" GROUP BY v ORDER BY count(*) DESC "); 

    return $query;    
  }


  public function md5query($query) {
    return md5($query);
  }

}
?>