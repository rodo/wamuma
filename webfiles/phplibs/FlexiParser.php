<?PHP
/*
 * Copyright (c) 2011 Rodolphe Quiédeville <rodolphe@quiedeville.org>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
Class FlexiParser
{
  public $options;

  public function __construct($parms = null, $default = null) {

    $page = 0;
    $query = null;
    $where = null;
    $this->values = 0;

    if ($parms == null) {
      $parms = Array();
    }

    if ($default == null) {
      $default = Array('sortname' => 'k');
    }

    if (array_key_exists("value", $parms)) {
      $this->values = 1;
      $this->key = $parms["value"];
    }

    if (array_key_exists("page", $parms)) {
      $page = substr($parms['page'],0,6);
    }
    
    if ($page < 1 ) {
      $page = 1;
    }

    if (array_key_exists("rp", $parms)) {
      $limit = substr($parms['rp'],0,3);
    } else {
      $limit = 15;
    }
    
    if ($limit < 1) {
      $limit = 15;
    }

    $offset = $limit * ($page - 1);
    
    if (array_key_exists("sortname", $parms)) {
      $sortname = substr($parms['sortname'],0,10);
    } else {
      $sortname = $default['sortname'];
    }
    if (array_key_exists("sortorder", $parms)) {
      $sortorder = substr($parms['sortorder'],0,10);
    } else {
      $sortorder = "asc";
    }
    
    if ($sortorder == 'undefined') {
      $sortorder = "asc";
    }

    if ($sortname == 'undefined') {
      $sortname = $default['sortname'];
    }
    
    if ($sortname == 'number') {
      $sortname = 'count(*)';
    }

    if (array_key_exists("query", $parms)) {
      /* search value */
      $query = pg_escape_string(utf8_encode(substr(urldecode($parms['query']), 0, 15)));

      $query = str_replace('*','%', $query);

      if (array_key_exists("qtype", $parms) && strlen(trim($query)) > 0 ) {
	/* Field name */
	$qtype = pg_escape_string(utf8_encode(substr($parms['qtype'], 0, 15)));
	if ( strlen(trim($qtype)) > 0 ) {
	    $where = sprintf("WHERE %s LIKE '%s'", $qtype, $query);
	  }
	
      }
    }
    
    $this->options = Array('limit' => $limit,
			   'page' => $page,
			   'sortname' => $sortname,
			   'sortorder' => $sortorder,
			   'offset' => $offset,
			   'where' => $where); 
  }

}

?>