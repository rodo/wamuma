<?PHP
/*
 * Copyright (c) 2011 Rodolphe Quiédeville <rodolphe@quiedeville.org>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

Class LogAnalyzer
{
  // properties
  public $nbvals = 0;
  public $datas = 0;
  public $color = "#0000FF";
  public $legend = 'Undefined';

  public function __construct($logfile, $vars) {
    /*
     * vars is $_GET
     *
     *
     */
    $datadef = null;

    if (array_key_exists('data', $vars)) {
      $datadef = $vars['data'];
    }

    if (array_key_exists('color', $vars)) {
      $this->color = substr($vars['color'],0,7);
    }


    if ($datadef != null) {
      $this->getLegend($datadef);
    }

    if ($datadef != null && file_exists($logfile) ) {
      $this->readfile($logfile, $datadef);
    } else {
      print file_exists($logfile);
    }

  }

    
  public function readfile($logfile, $key) {
    /*
     * Return an array
     */
    $handle = fopen ($logfile,'r');

    $i = 0;
    $old = 0;    

    $datas = array();    

    while(!feof($handle)) {
      $buffer = explode(' ',fgets($handle));
      if (sizeof($buffer) > 1) {
	
	if ($buffer[1] == $key) {
	  
	  if (intval($buffer[2]) == 0)  {
	    $buffer[2] = $old;
	  } else {
	    $old = intval($buffer[2]);
	  }
	  $this->nbvals++;
	  $datas[$i][0] = $buffer[0] * 1000;
	  $datas[$i][1] = intval(trim($buffer[2]));
	  if (sizeof($buffer) > 3) {
	    $datas[$i][2] = intval(trim($buffer[3]));
	  }
	  $i++;
	}
      }
    }
    
    fclose($handle);
    
    $this->datas = $datas;
  }

  public function getLegend($key) {

    $keys = array('nodes_orphaned_without_tag' => '# of orphan nodes', 
		  'nodes_duplicated' => '# of duplicate nodes', 
		  'nodes_keys_distinct' => '# of distincts keys in nodes',
		  'nodes' => '# of nodes',
		  'ways' => '# of ways',
		  'relations' => '# of relations',
		  'users' => '# of users');
    
    if (array_key_exists($key, $keys)) {

      $this->legend = $keys[$key];

    } else {

      $this->legend = 'Not found';

    }
  }


}
?>