<?PHP
/* 
 * Copyright (c) 2011 Rodolphe Quiédeville <rodolphe@quiedeville.org>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 *
 */
header('Content-type: text/plain');
header("Cache-Control: no-cache, must-revalidate");
header("Expires: Fri, 04 Jun 2004 05:00:00 GMT"); // 

include("libs.php");

$url = "http://" . $_SERVER['SERVER_NAME'] . dirname($_SERVER['PHP_SELF']) . '/' . substr($_GET['json'],0,30) . ".json";
$rp = substr($_GET['rp'],0,5);

$jsonurl = $url . "?rp=" . $rp;

 
$options = array(CURLOPT_URL            => $jsonurl,
		 CURLOPT_RETURNTRANSFER => true,
		 CURLOPT_HEADER         => false );
  
$CURL=curl_init();
 
curl_setopt_array($CURL,$options);
 
$content=curl_exec($CURL);

curl_close($CURL);

$json_output = json_decode($content);

foreach ($json_output->{'rows'} as $row) {
  foreach ($row->{'cell'} as $cell) {
    printf('"%s",', $cell);
  }
  print "\n";
}

?>
