<?PHP
/**
 *
 * Copyright (c) 2011 Rodolphe Quiédeville <rodolphe@quiedeville.org>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once 'PHPUnit/Framework.php';

require_once("libs.php");

class TestLogAnalyzer extends PHPUnit_Framework_TestCase
{

  public function testLogAnalyzer()
  {

    $foo = new LogAnalyzer("statistics.txt-dist",
			   array("data" => "nodes_duplicated"));

    $this->assertEquals($foo->nbvals, 0);

  }

  public function testLogAnalyzerNoData()
  {

    $foo = new LogAnalyzer("statistics.txt-dist",
			   array("foo" => "bar"));

    $this->assertEquals($foo->nbvals, 0);

  }


  public function testLogAnalyzerRightFile()
  {
    $fh = fopen("/dev/shm/statistics.txt","w");
    fwrite($fh, "1300000000 nodes_duplicated 1000\n");
    fwrite($fh, "1300000300 nodes_duplicated 1000\n");
    fwrite($fh, "1300000600 nodes_duplicated 1100\n");
    fwrite($fh, "1300000900 nodes_duplicated 1100\n");
    fwrite($fh, "1300001200 nodes_duplicated 1100\n");
    fwrite($fh, "1300001500 nodes_duplicated 1100\n");
    fclose($fh);

    $foo = new LogAnalyzer("/dev/shm/statistics.txt",
			   array("data" => "nodes_duplicated"));

    $this->assertEquals($foo->nbvals, 6);
    $this->assertEquals($foo->legend, '# of duplicate nodes');
  }

  public function testLogAnalyzerRightFileWithHole()
  {
    $fh = fopen("/dev/shm/statistics.txt","w");
    fwrite($fh, "1300000000 nodes_duplicated 1000\n");
    fwrite($fh, "1300000300 nodes_duplicated 0\n");
    fwrite($fh, "1300000900 nodes_duplicated 1100\n");
    fwrite($fh, "1300001200 nodes_duplicated 0 0\n");
    fwrite($fh, "1300001500 nodes_duplicated 1100\n");
    fclose($fh);

    $foo = new LogAnalyzer("/dev/shm/statistics.txt",
			   array("data" => "nodes_duplicated"));

    $this->assertEquals($foo->nbvals, 5);
  }



  public function testLogAnalyzerWrongFile()
  {

    $foo = new LogAnalyzer("/foobar/statistics.txt",
			   array("data" => "foobar"));

    $this->assertEquals($foo->nbvals, 0);

  }


  public function testLogAnalyzerColor()
  {

    $foo = new LogAnalyzer("/foobar/statistics.txt",
			   array("data" => "foobar",
				 "color" => "#fcfcfc")
			   );

    $this->assertEquals($foo->color, "#fcfcfc");
  }

  public function testLogAnalyzerDefaultColor()
  {

    $foo = new LogAnalyzer("/foobar/statistics.txt",
			   array("data" => "foobar")
			   );

    $this->assertEquals($foo->color, "#0000FF");

  }


}
?>
