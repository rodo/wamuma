<?PHP
/**
 *
 * Copyright (c) 2011 Rodolphe Quiédeville <rodolphe@quiedeville.org>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once 'PHPUnit/Framework.php';

require_once("libs.php");

class TestSqlQueries extends PHPUnit_Framework_TestCase
{

  public function testSqlQueriesInstantiate()
  {
    // Test the class instantiation and default values
    $sql = new SqlQueries();

    $this->assertEquals($sql->version, "1.1.0");
  }

  public function testSqlQueries()
  {
    // Test some values
    $datas = array("limit" => 2,
		   "offset" => 4,
		   "sortorder" => "desc",
		   "sortname" => "number",
		   "page" => 10,
		   "where" => null);

    $sql = new SqlQueries();

    $query = $sql->tabkeys($datas);
    $expected = "SELECT k, count(*) FROM stat GROUP BY k ORDER BY number desc LIMIT 2 OFFSET 4";

    $this->assertEquals($query, $expected);
  }

  public function testSqlQueriesWithWhere()
  {
    // Test some values
    $datas = array("limit" => 2,
		   "offset" => 4,
		   "sortorder" => "desc",
		   "sortname" => "number",
		   "page" => 10,
		   "where" => "WHERE k LIKE 'milk'");

    $sql = new SqlQueries();

    $query = $sql->tabkeys($datas);
    $expected = "SELECT k, count(*) FROM stat WHERE k LIKE 'milk' GROUP BY k ORDER BY number desc LIMIT 2 OFFSET 4";

    $this->assertEquals($query, $expected);
  }

  public function testSqlQueriesWithWhereBad()
  {
    // Test some values
    $datas = array("limit" => 2,
		   "offset" => 4,
		   "sortorder" => "desc",
		   "sortname" => "number",
		   "page" => 10,
		   "where" => "WHERE k LIKE 'milk'");

    $sql = new SqlQueries();

    $query = $sql->tabkeys($datas);
    $expected = "SELECT k, count(*) FROM stat WHERE k LIKE 'milk' GROUP BY k ORDER BY number desc LIMIT 2 OFFSET 4";

    $this->assertEquals($query, $expected);
  }

  public function testSqlQueriesmd5query()
  {
    // Test some values
    $sql = new SqlQueries();
    $qry = "SELECT k, count(*) FROM stat WHERE k LIKE 'milk' GROUP BY k ORDER BY number desc LIMIT 2 OFFSET 4";

    $this->assertEquals($sql->md5query($qry), "b5d10f5a81d76cdbf440b58fdf14dad3");
    $this->assertNotEquals($sql->md5query("SELECT a"), $sql->md5query("SELECT b"));
  }

  public function testSqlQuerieswhorder()
  {
    // Test some values
    $sql = new SqlQueries();

    $datas = array("limit" => 42,
		   "offset" => 4,
		   "sortorder" => "desc",
		   "sortname" => "foo",
		   "page" => 10,
		   "where" => "WHERE k LIKE 'milk'");

    $qry = " WHERE k LIKE 'milk' ORDER BY foo desc LIMIT 42 OFFSET 4";

    $this->assertEquals($sql->whorder($datas), $qry);
  }

}
?>
