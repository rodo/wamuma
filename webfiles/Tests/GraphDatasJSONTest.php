<?PHP
/**
 *
 * Copyright (c) 2011 Rodolphe Quiédeville <rodolphe@quiedeville.org>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once 'PHPUnit/Framework.php';

require_once("libs.php");

class TestGraphDatasJSON extends PHPUnit_Framework_TestCase
{

  public function testGDJInstantiate()
  {
    // We test the right version
    $datas = array(12,12);
    $foo = new GraphDatasJSON("foobar", $datas);
    $this->assertEquals($foo->version, "1.0.1");
  }

  public function testGraphDatasJSON()
  {
    $datas = array(12,12);
    $foo = new GraphDatasJSON("foobar", $datas);

    $expect = json_decode($foo->out, true);

    $this->assertEquals($expect[0]['label'], "foobar");
    $this->assertEquals($expect[0]['data'], array(12,12) );
  }

  public function testGraphDatasJSONDouble()
  {
    $datas = array(array(12,12,12),
		   array(12,12,12));
    $foo = new GraphDatasJSON("foobar", $datas);

    $expect = json_decode($foo->out, true);

    $this->assertEquals($expect[0]['label'], "foobar");

  }


  public function testGraphDatasJSONWithColor()
  {
    $datas = array(12,12);
    $foo = new GraphDatasJSON("foobar", $datas, "blue");

    $expect = json_decode($foo->out, true);

    $this->assertEquals($expect[0]['color'], "blue");

  }

  public function testSplitDatas()
  {
    $data = array( array (13456, 4, 5),
		   array (13457, 6, 7)
		   );

    $foo = new GraphDatasJSON("/foobar/statistics.txt",
			      $data   );

    $res = $foo->splitdatas($data);

    $this->assertEquals($res[0][0][0], 13456);
    $this->assertEquals($res[0][0][1], 4);
    $this->assertEquals($res[0][1][1], 6);

    $this->assertEquals($res[1][0][0], 13456);
    $this->assertEquals($res[1][0][1], 5);
    $this->assertEquals($res[1][1][1], 7);

  }


  public function testjsonformat()
  {
    $datas = array(array(12,12,12),
		   array(12,12,12));

    $foo = new GraphDatasJSON("foobar", $datas, "blue");

    $expect = $foo->jsonformat($datas, "foober", "#de45ed");

    $this->assertEquals($datas[0], array(12,12,12));
    print $expect;
  }

}
?>
