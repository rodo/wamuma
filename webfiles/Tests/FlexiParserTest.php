<?PHP
/**
 *
 * Copyright (c) 2011 Rodolphe Quiédeville <rodolphe@quiedeville.org>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once 'PHPUnit/Framework.php';

require_once("libs.php");

class TestFlexiParser extends PHPUnit_Framework_TestCase
{

  public function testFlexiParserInstantiate()
  {
    // Test the class instantiation and default values
    $foo = new FlexiParser();

    $this->assertEquals($foo->options['limit'], 15);
    $this->assertEquals($foo->options['sortname'], 'k');
    $this->assertEquals($foo->options['sortorder'], 'asc');
    $this->assertEquals($foo->options['page'], 1);
    $this->assertEquals($foo->options['where'], null);
  }

  public function testFlexiParserWithValues()
  {
    // Test some values
    $datas = array("rp" => 25,
		   "sortorder" => "desc",
		   "sortname" => "number",
		   "page" => 10);

    $foo = new FlexiParser($datas);

    $this->assertEquals($foo->options['limit'], 25);
    $this->assertEquals($foo->options['page'], 10);
    $this->assertEquals($foo->options['offset'], 225);
    $this->assertEquals($foo->options['sortname'], "count(*)");
    $this->assertEquals($foo->options['sortorder'], "desc");
  }

  public function testFlexiParserWithOtherValues()
  {
    // Other values
    $datas = array("sortorder" => "undefined",
		   "sortname" => "undefined");

    $foo = new FlexiParser($datas);

    $this->assertEquals($foo->options['sortname'], "k");
    $this->assertEquals($foo->options['sortorder'], "asc");
  }

  public function testFlexiParserWithBadValues()
  {
    // We test the right version
    $datas = array("rp" => -25,
		   "sortorder" => "desc",
		   "page" => -42);

    $foo = new FlexiParser($datas);

    $this->assertEquals($foo->options['limit'], 15);
    $this->assertEquals($foo->options['page'], 1);
    $this->assertEquals($foo->options['offset'], 0);
  }

  public function testFlexiParserQuery()
  {
    // We test the right version
    $datas = array("query" => "milk",
		   "qtype" => "foobar");

    $foo = new FlexiParser($datas);

    $this->assertEquals($foo->options['where'], "WHERE foobar LIKE 'milk'");
  }

  public function testFlexiParserQueryDefValWo()
  {
    // We test the right version
    $datas = array("sortname" => "undefined");

    $foo = new FlexiParser($datas);

    $this->assertEquals($foo->options['sortname'], "k");
  }


  public function testFlexiParserQueryDefVal()
  {
    // We test the right version
    $datas = array("sortname" => "undefined");
    $def = array ('sortname' => 'foo');

    $foo = new FlexiParser($datas, $def);

    $this->assertEquals($foo->options['sortname'], "foo");
  }

  public function testFlexiParserQueryValue()
  {
    // We test the right version
    $datas = array("value" => "foobar");

    $foo = new FlexiParser($datas);

    $this->assertEquals($foo->values, 1);
  }


  public function testFlexiParserQueryReplace()
  {
    // We test the right version
    $datas = array("query" => "*milk*",
		   "qtype" => "foobar");

    $foo = new FlexiParser($datas);

    $this->assertEquals($foo->options['where'], "WHERE foobar LIKE '%milk%'");
  }

  public function testFlexiParserQueryReplaceBadChars()
  {
    // We test the right version
    $datas = array("query" => urlencode("%milk%"),
		   "qtype" => "foobar");

    $foo = new FlexiParser($datas);

    $this->assertEquals($foo->options['where'], "WHERE foobar LIKE '%milk%'");

  }


}
?>
