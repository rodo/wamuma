# -*- coding: utf-8 -*-
#
# Copyright (c) 2011 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# pylint: disable=W0511
"""Main functions"""
import sys
import os
import syslog
import re
import shutil
import subprocess
import urllib2

import LibExosm
import LibCompress
from WmmDatabase import WmmDatabase

def rmexfile(fpath):
    '''Remove a file if it's exists'''
    if os.path.exists(fpath):
        os.unlink(fpath)

def hdmsql(dirname, country, conf):
    """Do all action for sql based shapefile generation"""
    osmfile_name = "%s/%s.osm.bz2" % (dirname, country.dirname)
    LibExosm.checkdir(dirname)
    for shp in readshapesqueries(country.dbname):
        prefix = shp[0]
        style = os.path.join(conf.styldir, shp[2])
        zipfile_name = "%s/%s.%s.zip" % (dirname, country.dirname, prefix)
        LibExosm.checkdir(os.path.join(dirname, prefix))
        if os.path.exists(osmfile_name):    
            if newgeneration(osmfile_name, zipfile_name) or conf.force:
                rmexfile(zipfile_name)
                res = hdmosm2pgsql(conf, country.dbname, style, 
                                   osmfile_name, prefix + "_osm")
                if res == 0:
                    mkshapesquery(dirname, country, conf.pgsql2shp, shp)
                else:
                    msg = "%s : error osm2pgsql process, %s" % (country.dirname,
                                                                str(res))
                    syslog.syslog(msg)
            else:
                msg = '%s : Shapefiles %s are up to date' % (country.dirname, 
                                                             prefix)
                syslog.syslog(msg)
        else:
            syslog.syslog("%s : File %s does not exists" % (country.dirname,
                                                            osmfile_name))

def addxy(dbname, prefix):
    """Add columns X and Y on tables"""
    dbshp = WmmDatabase(dbname, None, None)
    for obj in ['point']:
        tname = prefix + "_" + obj
        dbshp.exeq("ALTER TABLE %s ADD x float" % tname )
        dbshp.exeq("ALTER TABLE %s ADD y float" % tname )
        dbshp.exeq("UPDATE %s SET x=ST_X(way), y=ST_Y(way)" % tname )

def convertfields(dbname, prefix):
    """Add columns X and Y on tables"""
    dbshp = WmmDatabase(dbname, None, None)
    conversions = fieldstoconvert()
    for obj in ['point']:
        for fields in conversions:
            qry = 'ALTER TABLE %s RENAME "%s" TO "%s"'
            dbshp.exeq(qry % (prefix + "_" + obj,
                              fields[0],
                              fields[1]) )

def fieldstoconvert():
    """Fields name to convert"""
    dbshp = WmmDatabase("osm", None, None)
    conversions = dbshp.fetchquery("SELECT osm, shp FROM shp_field_conv")
    return conversions

def hdm(dirname, country, conf, style, prefix):
    """Do all action"""
    osmfile_name = "%s/%s.osm.bz2" % (dirname, country.dirname)
    zipfile_name = "%s/%s.%s.zip" % (dirname, country.dirname, prefix)

    LibExosm.checkdir(dirname)
    LibExosm.checkdir(os.path.join(dirname, prefix))
    if os.path.exists(osmfile_name):    
        if newgeneration(osmfile_name, zipfile_name) or conf.force:
            rmexfile(zipfile_name)
            # Load osm datas in postgis
            res = hdmosm2pgsql(conf, country.dbname, style, osmfile_name)
            if res == 0:
                copystyle(style, os.path.join(dirname, prefix))
                mkshapes(dirname, country.dirname, prefix, 
                         country.dbname, conf.pgsql2shp)
            else:
                msg = "%s : error in osm2pgsql process, %s" % (country.dirname,
                                                               str(res))
                syslog.syslog(msg)
        else:
            msg = '%s : Shapefiles %s are up to date' % (country.dirname, 
                                                         prefix)
            syslog.syslog(msg)
    else:
        syslog.syslog("%s : File %s does not exists" % (country.dirname,
                                                        osmfile_name))

def copystyle(style, dirname):
    """Copy the style filename to the output directory"""
    shutil.copy(style, dirname)

def mkshapes(dirname, country, prefix, dbname, pg2shp):
    """Make shapefiles"""
    objects = ('line', 'point', 'polygon')
    zipfile_name = "%s/%s.%s.zip" % (dirname, country, prefix)
    LibExosm.checkdir(os.path.join(dirname, prefix))
    for objname in objects:                
        log = "%s/logs/%s.%s" % (dirname, prefix, objname)
        shpfile = "%s/%s/%s.%s.%s." % (dirname, prefix, country, 
                                       prefix, objname)
        makeshapetable(pg2shp, dbname, objname, shpfile, log)
        compfiles(zipfile_name, shpfile)
    logconv = shploggenerator(dirname, country, prefix, objects)
    LibCompress.zipaddfile(zipfile_name, logconv)

def makeshapetable(pg2shp, dbname, objname, filename, logs):
    """Create a shapefile from table"""
    logname = "%s.txt" % (logs)
    logerr = "%s.err.txt" % (logs)
    if not os.path.exists(pg2shp):
        syslog.syslog("%s does not exists" % (pg2shp))
        sys.exit(5)
    tablename = "planet_osm_%s" % objname
    args = [pg2shp, '-f', filename, dbname, tablename]
    return execcmd(args, logname, logerr)


def readshapesqueries(dbname):
    '''Read definition of shape queries in database

    Return an array of tuples
    '''
    queries = []
    for query in readshapesqueriesfromdb(dbname):
        fields = readshapesqueriesfields(dbname, query[0])
        fqu = "SELECT %s FROM %s_osm_%s WHERE %s" % (shapesqueriesbuild(fields),
                                                     query[0],
                                                     query[1],
                                                     query[3])
        queries.append ( (query[0], query[1], query[2], fqu) )
    return queries

def readshapesqueriesfromdb(dbname):
    '''Read definition of shape queries in database

    Return an array of tuples
    '''
    dbshp = WmmDatabase(dbname, None, None)
    qry = "SELECT label, tgeom, stylefile, query FROM query_shapes"
    queries = dbshp.fetchquery(qry)
    return queries


def readshapesqueriesfields(dbname, queryname):
    '''Fiels list from database

    Return an array of tuples
    '''
    dbshp = WmmDatabase(dbname, None, None)
    qry = "SELECT field FROM query_shapes_fields WHERE query = %s"
    queries = dbshp.fetchquery(qry, (queryname, ) )
    return queries


def shapesqueriesbuild(fieldlist):
    '''Transform an array of tuple in a string

    Return : string
    '''
    fstr = ''
    for field in fieldlist:
        fstr = fstr + field[0] + ','
    return fstr[:-1]



def mkshapesquery(dirname, country, pg2shp, shp):
    """Make shapefiles from sql queries defined in db"""
    addxy(country.dbname, shp[0] + "_osm")
    convertfields(country.dbname, shp[0] + "_osm")
    zipfile_name = "%s/%s.%s.zip" % (dirname, country.dirname, shp[0])
    LibExosm.checkdir(os.path.join(dirname, shp[0]))
    log = "%s/logs/%s.%s" % (dirname, shp[0], shp[1])
    shpfile = "%s/%s/%s.%s.%s." % (dirname, shp[0], country.dirname, 
                                   shp[0], shp[1])
    makeshapequery(pg2shp, country.dbname, shp[3], shpfile, log)
    compfiles(zipfile_name, shpfile)
    logconv = shploggenerator(dirname, country.dirname, shp[1], [shp[0]])
    LibCompress.zipaddfile(zipfile_name, logconv)

def makeshapequery(pg2shp, dbname, query, filename, logs):
    """Create shapefile from sql query"""
    logname = "%s.txt" % (logs)
    logerr = "%s.err.txt" % (logs)
    if not os.path.exists(pg2shp):
        syslog.syslog("%s does not exists" % (pg2shp))
        sys.exit(5)
    args = [pg2shp, '-f', filename, dbname, query]
    return execcmd(args, logname, logerr)


def newgeneration(osmfile_name, zipfile_name):
    """Check if a new generation is needed"""
    zipfile_time = 0
    if os.path.exists(osmfile_name):    
        osmfile_time = LibExosm.filedate(osmfile_name)
        if os.path.exists(zipfile_name):
            zipfile_time = LibExosm.filedate(zipfile_name)
        if osmfile_time > zipfile_time:
            return True
        else:
            return False
    else:
        return False

def compfiles(zipfilename, shpfile):
    """Compress files in one zip file"""
    if os.path.exists(zipfilename):
        LibCompress.zipaddfile(zipfilename, "%sshp" % (shpfile))
    else:
        LibCompress.zipnewfile(zipfilename, "%sshp" % (shpfile))
    for extension in ('dbf', 'prj', 'shx'):
        if os.path.exists("%s%s" % (shpfile, extension)):
            LibCompress.zipaddfile(zipfilename, "%s%s" % (shpfile, extension))

def hdmosm2pgsql(conf, dbname, style, filename, tprefix = None):
    """Load datas in database"""
    osm2p = conf.osm2pgsql
    dirname = os.path.dirname(filename)
    logdir = LibExosm.checkdir(os.path.join(dirname,"logs"))
    logname = os.path.join(logdir,"osm2psgsql.log.txt")
    logerr = os.path.join(logdir,"osm2psgsql.err.txt")

    if not os.path.exists(style):
        syslog.syslog("%s : style %s does not exists" % (dbname, style))
        sys.exit(2)
    if not os.path.exists(dirname):
        syslog.syslog("%s does not exists" % (dirname))
        sys.exit(2)
    if not os.path.exists(filename):
        syslog.syslog("%s does not exists" % (filename))
        sys.exit(3)
    if not os.path.exists(osm2p):
        syslog.syslog("%s does not exists" % (osm2p))
        sys.exit(4)
    if tprefix == None:
        args = [osm2p, '-l', '-s', '-d', dbname, 
                '-S', style, filename]
    else:
        args = [osm2p, '-l', '-s', '-d', dbname, 
                '-p', tprefix,
                '-S', style, filename]
    return execcmd(args, logname, logerr, False, dbname)


def execcmd(args, stdoutname=None, stderrname=None, shell=False, lprefix=None):
    """Execute a command as subprocess and logs actions"""
    cmd = 'Execute'
    f_out = None
    f_err = None

    for arg in args:
        cmd = cmd + ' ' + arg
    if lprefix:
        syslog.syslog("%s : %s" % (lprefix, cmd))
    else:
        syslog.syslog(cmd)
    if not stdoutname == None:
        if os.path.exists(stdoutname):
            os.unlink(stdoutname)
        f_out = open(stdoutname, 'w')

    if not stderrname == None:
        if os.path.exists(stderrname):
            os.unlink(stderrname)

        f_err = open(stderrname, 'w')

    res = subprocess.call(args, 0, None, None, f_out, f_err, shell=shell)

    if f_out:
        f_out.close()

    if f_err:
        f_err.close()

    if (res == None):
        res = 0

    if res != 0:
        syslog.syslog("Error in : %s" % (cmd))

    clean_files(stdoutname, stderrname)
    return res

def clean_files(out_name, err_name):
    """Remove files if they are empty"""
    if not out_name == None:
        if os.path.exists(out_name):
            if int(os.path.getsize(out_name)) == 0:
                os.unlink(out_name)

    if not err_name == None:
        if os.path.exists(err_name):
            if int(os.path.getsize(err_name)) == 0:
                os.unlink(err_name)

def refreshfile(url, filename, logfile):
    """Refresh a local file by downloading a remote one"""
    if url.startswith('http'):
        if not os.path.exists(filename):
            downloadfile(url, filename, logfile)
        else:
            if (LibExosm.testurl(url) > LibExosm.filedate(filename) ):
                downloadfile(url, filename, logfile)
            else:
                syslog.syslog("%s is up to date" % filename)
    else:
        syslog.syslog('bad url %s' % url)

def fresherfile(url, filename):
    """Check if a remote file is fresher than the one we have on filesystem"""
    fresher = False
    if not os.path.exists(filename):
        fresher = True
    else:
        if (LibExosm.testurl(url) > LibExosm.filedate(filename) ):
            fresher = True
    return fresher


def wgetfile(url, pathname, logname=None):
    """Fetch a remote file using wget binary"""
    tempname = pathname + ".parts"
    syslog.syslog("open %s" % url)
    if logname == None:
        args = ["/usr/bin/wget", "-q", url, "-O", tempname ]
    else:
        args = ["/usr/bin/wget", url, "-O", tempname, "-o", logname ]
    res = execcmd(args, logname)
    if (res == 0):
        try:
            shutil.move(tempname, pathname)
        except StandardError:
            syslog.syslog("ERROR unable to move %s to %s" % 
                          (tempname, pathname)
                          )
    return res


def downloadfile(url, pathname, logname):
    """Download a remote file

    # TODO add a user-agent !

    user_agent = 'WaMuMa'
    headers = { 'User-Agent' : user_agent }
    values = { 'language' : 'Python' }
    data = urllib.urlencode(values)
    req = urllib2.Request(url, data, headers)

    """
    syslog.syslog("open %s" % url)
    req = urllib2.Request(url)
    try:
        response = urllib2.urlopen(req)
        datas = response.read()
        heads = response.info()
        writedatas(pathname, datas)
    except urllib2.URLError, err:
        heads = {'errno': err.errno, 'message': err.code}

    return writelogs(logname, heads)


def writelogs(pathname, datas):
    """Write log information in syslog"""
    syslog.syslog("Write logfile %s " % pathname)
    with open(pathname, 'w') as fgpx:
        for data in datas:
            fgpx.write("%s: %s\n" % (data, datas[data]))
        fgpx.close()
    
    return 0

def writedatas(pathname, datas):
    """Write datas in file"""
    syslog.syslog("Write datas in %s " % pathname)
    fgpx = open(pathname, 'w')    
    fgpx.write(datas)
    fgpx.close()

def shplogparser(filename):
    """Parse shapemake logs"""
    res = []
    with open(filename, 'r') as fgpx:
        for line in fgpx:
            rem = re.match(r"""^Warning, field (.*) renamed to (.*)$""", line)
            if rem:
                res.append( rem.group(2, 1) )


    return sorted(res)

def shploggenerator(dirname, country, prefix, objects):
    """Generate a file that contains all conversion"""
    LibExosm.checkdir(dirname)
    LibExosm.checkdir(os.path.join(dirname, prefix))
    filename = "%s/%s/%s.%s.fields_conversion.txt" % (dirname, 
                                                      prefix, 
                                                      country, 
                                                      prefix)

    title = """Fields name conversion beetween 
            key name in osm and attribute in shapefile\n"""
    if os.path.exists(filename):
        os.unlink(filename)
    with open(filename, 'w') as fih:
        fih.write(title)        
        for objname in objects:
            fih.write("\n%s\n------------------------\n" % objname)
            logerr = "%s/logs/%s.%s.err.txt" % (dirname, prefix, objname)
            if os.path.exists(logerr):
                datas = shplogparser(logerr)        
                if datas:
                    for line in datas:
                        fih.write("%s : %s\n" % line)    
    return filename
