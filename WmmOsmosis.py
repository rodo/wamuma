# -*- coding: utf-8 -*-
#
# Copyright (c) 2011 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""Osmosis class"""
import os
import sys, shutil
import syslog
import threading

import LibExosm
import LibHDM

class GenCountry(threading.Thread):
    """Country parts"""

    def __init__(self, contfilename, country, dirname, osmosis):
        """Initialization"""
        threading.Thread.__init__(self)

        self.contfilename = contfilename
        self.country = country
        self.dirname = dirname
        self.osmfilename = "%s/%s.osm" % (dirname, country)
        self.polyfile = "%s/%s.poly" % (dirname, country)
        self.osmosis = osmosis

    def run(self):
        """Do the job"""
        res = 0

        logname = "%s/logs/osmosis.log" % (self.dirname)
        logerr = "%s/logs/osmosis.err" % (self.dirname)
        tempname = self.osmfilename + ".parts"

        if self.checkdepends():
            try:
                contfiletime = LibExosm.filedate(self.contfilename)
            except StandardError:
                contfiletime = 0
            
            try:
                osmfiletime = LibExosm.filedate(self.osmfilename)
            except StandardError:
                osmfiletime = 0

            if (contfiletime > osmfiletime):
                args = [self.osmosis, 
                        "--read-xml", "file=%s" % self.contfilename, 
                        "--bounding-polygon", "file=%s" % self.polyfile,  
                        "--write-xml", "file=%s" % tempname
                        ]

                res = LibHDM.execcmd(args, logname, logerr, False)
                if res == 0:
                    try:
                        shutil.move(tempname, self.osmfilename)
                    except StandardError:
                        syslog.syslog("ERROR unable to move %s to %s" % 
                                      (tempname, self.osmfilename)
                                      )

            else:
                syslog.syslog("File %s is up to date" % self.osmfilename)
        if res > 0:
            print "Error in generating %s from %s " % (self.osmfilename, 
                                                       self.contfilename)
            sys.exit(res)
        return res


    def checkdepends(self):
        """Check if all dependents files and dirs are present"""
        res = True
        LibExosm.checkdir(self.dirname)
        LibExosm.checkdir("%s/logs" % self.dirname)
        if not os.path.exists(self.dirname):
            res = False
            msg = "%s : output directory %s does not exists" % (self.country,
                                                                self.dirname)
            syslog.syslog(msg)

        if not os.path.exists(self.polyfile):
            res = False
            msg = "%s : Polygon file %s does not exists" % (self.country, 
                                                            self.polyfile)
            syslog.syslog(msg)

        if not os.path.exists(self.contfilename):
            res = False
            msg = "%s : Continent file %s does not exists" % (self.country,
                                                              self.contfilename)
            syslog.syslog(msg)

        return res
