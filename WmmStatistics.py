# -*- coding: utf-8 -*-
#
# Copyright (c) 2011 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# pylint: disable=R0902
# pylint: disable=C0301
"""Osmosis class"""
import threading
import os, syslog
from stat import ST_MTIME
import shutil
from time import time
import LibHDM

from statistics import utils

from WmmDatabase import WmmDatabase

class WmmStatistics(threading.Thread):
    """Country parts"""
    _country = None
    _osmdatafile = None
    _dbname = None
    _dbuser = None
    _dbpassword = None
    _path = None
    _webfilespath = None

    def __init__(self, conf, osmdatafile, country, path):
        """Initialization"""
        threading.Thread.__init__(self)

        self.osmosis = conf.osmobin
        self._osmdatafile = osmdatafile
        self._dbname = country.dbstatname
        self._dbuser = conf.dbuser
        self._dbpass = conf.dbpass
        self._dbhost = 'localhost'
        self._dbport = '5432'
        self._path = path
        self._webfilespath = conf.webfiles
        self._country = country

    def run(self):
        """Do the job"""
        res = 0
        res = self._loadosm()
        if res == 0:
            res = self._fillsql()
        if res == 0:
            res = self.statsql()
        if self._webfilespath != None:            
            self._webfiles()
        else:
            syslog.syslog("%s : webfilepaths is not defined " % (self._dbname ))
        odb = WmmDatabase(self._dbname, None, None)
        statdir = os.path.join(self._path, "stats")
        if not os.path.exists(statdir):
            os.makedirs(statdir)
        utils.stats_auto(odb, statdir)

        return res

    def _webfiles(self):
        """Create web files"""
        webdir = os.path.join(self._path, "cleaning")
        if not os.path.exists(webdir):
            os.makedirs(webdir)

        if not os.path.exists(webdir):
            syslog.syslog("%s : dir %s does not exists, do no copy webfiles" % (self._dbname, webdir ))
        else:
            phpconf = os.path.join(webdir, "conf.php")
            dsn = "host=%s port=%s dbname=%s user=%s password=%s" % (self._dbhost,
                                                                     self._dbport,
                                                                     self._dbname,
                                                                     self._dbuser,
                                                                     self._dbpass)
            logfile = '../logs/statistics-%s.txt' % self._dbname
            try:
                fphp = open (phpconf, 'w')
                fphp.write("<?PHP\n/* Auto generated file do not edit */\n")
                fphp.write("$dsn='%s';\n" % dsn) 
                fphp.write("$logfile='%s';\n" % logfile)
                fphp.write("$name='%s';\n" % self._dbname)
                fphp.write("$mapcenter='%s';\n" % self._country.mapcenter())
                fphp.write("?>")
            except IOError, err:
                print err

            if os.path.exists(self._webfilespath):
                for infile in os.listdir(self._webfilespath):
                    src = os.path.join(self._webfilespath, infile)
                    if infile.endswith('.php') and not infile.startswith('.') and not infile == 'conf.php':
                        shutil.copy(src, webdir)
                    if infile.endswith('.html') and not infile.startswith('.'):
                        shutil.copy(src, webdir)
                    if infile.endswith('.json') and not infile.startswith('.'):
                        shutil.copy(src, webdir)
                self.copyrecurse('js')
                self.copyrecurse('css')
                self.copyrecurse('phplibs')
            
    def copyrecurse(self, path):
        """Create path"""
        dstdir = os.path.join(self._path, "cleaning", path)        
        if not os.path.exists(dstdir):
            os.makedirs(dstdir)
        srcdir = os.path.join(self._webfilespath, path)
        if os.path.exists(srcdir):
            for infile in os.listdir(srcdir):
                if os.path.isdir(os.path.join(srcdir, infile)):
                    self.copyrecurse(os.path.join(path, infile))
                else:
                    src = os.path.join(srcdir, infile)
                    if infile.endswith('.js') and not infile.startswith('.'):
                        shutil.copy(src, dstdir)
                    if infile.endswith('.css') and not infile.startswith('.'):
                        shutil.copy(src, dstdir)
                    if infile.endswith('.gif') and not infile.startswith('.'):
                        shutil.copy(src, dstdir)
                    if infile.endswith('.png') and not infile.startswith('.'):
                        shutil.copy(src, dstdir)
                    if infile.endswith('.jpeg') and not infile.startswith('.'):
                        shutil.copy(src, dstdir)
                    if infile.endswith('.jpg') and not infile.startswith('.'):
                        shutil.copy(src, dstdir)

    def droptables(self):
        """Drop osmosis tables"""
        odb = WmmDatabase(self._dbname, None, None)

        tables = ['nodes', 'relation_members', 'relations',
                  'users', 'way_nodes','ways' ]

        for table in tables:
            odb.query("DELETE FROM %s" % table)

    def logtables(self):
        """Drop osmosis tables"""
        odb = WmmDatabase(self._dbname, None, None)
        qry = "INSERT INTO wamuma.files_loaded (fpath, ftime)"
        qry = qry + " VALUES (%s, %s) "
        odb.exeq(qry, (self._osmdatafile, int(time())) )


    def _readlogtables(self):
        """Read the last time the data file was imported in db"""
        odb = WmmDatabase(self._dbname, None, None)
        qry = " SELECT case WHEN MAX(ftime) IS NULL THEN 0 ELSE MAX(ftime) END"
        qry = qry + " FROM wamuma.files_loaded WHERE fpath=%s"
        ftime = odb.fetchquery(qry, (self._osmdatafile, ))[0]
        return ftime[0]

    def _filedate(self):
        """Return the last modification time for data file"""
        mtime = os.stat(self._osmdatafile)[ST_MTIME]
        return mtime

    def _filefreshdb(self):
        """Compare the db to filedate"""
        test = self._filedate() > self._readlogtables()
        if not test:
            syslog.syslog("%s : data file is older than database" % self._dbname)
        return test

    def _loadosm(self):
        """Load the osm file in database"""
        fresher = False
        if os.path.exists(self._osmdatafile):
            fresher = self._filefreshdb()
        if fresher:
            self.logtables()
            self.droptables()
            args = [self.osmosis, 
                    "--read-xml", "file=%s" % self._osmdatafile, 
                    "--write-pgsql", "database=%s" % self._dbname,
                    "user=%s" % self._dbuser,
                    "password=%s" % self._dbpass
                    ]
            
            res = LibHDM.execcmd(args,
                                 os.path.join(self._path, 'logs', 
                                              self._dbname + '.log'),
                                 os.path.join(self._path, 'logs', 
                                              self._dbname + '.err.log'))
            
            if res != 0:
                msg = "Failed to load osm datas in %s : [%s]" % (self._dbname, res)
                syslog.syslog(msg)
        else:
            res = 0
        return res



    def statsql(self):
        """Do some stats"""
        tms = int(time())
        odb = WmmDatabase(self._dbname, None, None)

        # Add here something like haiti_stat=# SELECT viewname from pg_catalog.pg_views where viewname like 'wmc_%' ;

        vst = [ ("nodes", "SELECT count(*) FROM nodes"),
                ("ways", "SELECT count(*) FROM ways"),
                ("relations", "SELECT count(*) FROM relations"),
                ("way_nodes", "SELECT count(*) from way_nodes"),
                ("nodes_keys_distinct", "SELECT count(distinct(k)) FROM stat_nodes"),
                ("ways_keys_distinct", "SELECT count(distinct(k)) FROM stat_ways"),
                ("nodes_uniq_in_way", "SELECT count(distinct(node_id)) from way_nodes"),
                ("nodes_orphaned_without_tag", "SELECT count(id) FROM wmm_nodes_orphan"),
                ("nodes_duplicated", "SELECT count(*) from wmm_duplicated_nodes where count > 1"),
                ("ways_length_total", "SELECT sum(st_length(linestring)) FROM ways"),
                ("users", "SELECT count(distinct(user_id)) FROM nodes", "SELECT count(distinct(user_id)) FROM ways")
                ]

        for xvst in vst:
            self.stat(tms, odb, xvst)

    
        return 0

    def stat(self, tms, odb, vst):
        """Execute a sql query and make a log entry in stat file"""
        label = vst[0]
        query = vst[1]
        nkd = "UNK"
        row = odb.fetchquery(query)
        if row == None:
            nkd = 0
        else:
            nkd = row[0][0]
        try:
            row = odb.fetchquery(vst[2])
            if row == None:
                nk2 = 0
            else:
                nk2 = row[0][0]
        except IndexError:
            nk2 = None

        fpath = os.path.join(self._path, 
                             'logs', 
                             'statistics-%s.txt' % self._dbname )
        if os.path.exists(fpath):
            mode = 'a'
        else:
            mode = 'w'
        with open(fpath, mode) as logfile:
            if nk2 != None:
                logfile.write("%d %s %s %s\n" % (tms, label, nkd, nk2))
            else:
                logfile.write("%d %s %s\n" % (tms, label, nkd))

    def _fillsql(self):
        """Fill the tables"""
        odb = WmmDatabase(self._dbname, None, None)
        odb.query("DELETE FROM stat")
        query = """INSERT INTO stat_nodes (node_id, lon, lat, k, v)
 ( SELECT id, st_x(geom), st_y(geom), (each(tags)).key, (each(tags)).value  
   FROM nodes )
"""
        odb.query(query)
        query = """INSERT INTO stat_ways (node_id, lon, lat, k, v)
 ( SELECT id, st_x(st_endpoint(linestring)), st_y(st_endpoint(linestring)), (each(tags)).key, (each(tags)).value 
   FROM ways )
"""
        odb.query(query)
        odb.query("DELETE FROM stat_tags")
        query = """INSERT INTO stat_tags 
( SELECT k, count(k) FROM stat GROUP BY k)"""
        odb.query(query)

        return 0

