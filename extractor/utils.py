# -*- coding: utf-8 -*-
#
# Copyright (c) 2011 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import urllib2

def downloadfile(url, pathname):
    """Download a remote file
    
    # TODO add a user-agent !
    
    user_agent = 'WaMuMa'
    headers = { 'User-Agent' : user_agent }
    values = { 'language' : 'Python' }
    data = urllib.urlencode(values)
    req = urllib2.Request(url, data, headers)
        
    """
    req = urllib2.Request(url)
    try:
        response = urllib2.urlopen(req)
        datas = response.read()
        heads = response.info()
        writefile(pathname, datas)
    except urllib2.URLError, err:
        heads = {'errno': err.errno, 'message': err.code}
        

def writefile(pathname, datas):
    """Write datas in file"""
    fgpx = open(pathname, 'w')    
    fgpx.write(datas)
    fgpx.close()


def loadfile(pathname):
    """Load a part of a file

    Remove the first 2 lines and the last line of a file
    """
    text = ''
    fgpx = open(pathname, 'r')
    lines = fgpx.readlines()
    i = 2
    while (i < len(lines) - 1):
        text += lines[i]
        i += 1
    fgpx.close()
    return text
