# -*- coding: utf-8 -*-
#
# Copyright (c) 2011 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""Read osm object ids from a Postgresql database and write them in a zipfile"""
import os
import sys
import psycopg2
import psycopg2.extensions
from zipfile import ZipFile, ZIP_DEFLATED
import bz2
from utils import downloadfile, loadfile, writefile
__version__ = "0.0.3"

class Extractor():
    """The database object"""

    def __init__(self, dbname=None, path=None):
        """Class initialisation"""
        self.name = dbname
        self._conn = None
        self.path = path
        self.host = "api.openstreetmap.org"
        if dbname != None:
            self.connect(dbname)
        self.version = __version__

    def download(self, tag):
        """down obj"""
        content = ""
        content = self.download_nodes(tag)
        content += self.download_ways(tag)
        self.writecontent(tag, content)

    def nodedownload(self, nodeid):
        """Downloads nodes"""
        content = ''
        url = 'http://%s/api/0.6/nodes?nodes=%s' % (self.host, nodeid)
        filename = os.path.join(self.path, "node-%s.osm" % nodeid )
        downloadfile(url, filename)
        content = content + loadfile(filename)
        return content
    
    def download_nodes(self, tag):
        """Downloads nodes"""
        content = ''
        print "%s nodes found" % self.countnodes(tag)
        rows = self.readnodes(tag)
        for row in rows:
            content += self.nodedownload(row[0])
        return content

    def download_ways(self, tag):
        """Downloads ways"""
        content = ''
        print "%s ways found" % self.countways(tag)
        rows = self.readways(tag)
        for row in rows:
            url = 'http://%s/api/0.6/way/%s/full' % (self.host, row[0])
            filename = os.path.join(self.path, "node-%s.osm" % row[0] )
            downloadfile(url, filename)
            content = content + loadfile(filename)
        return content


    def readnodes(self, tag):
        """read data from db"""
        qry = "SELECT node_id FROM stat_nodes WHERE k = %s LIMIT 100"
        rows = self.fetchquery(qry, (tag, ))
        return rows


    def countnodes(self, tag):
        """read data from db"""
        qry = "SELECT count(node_id) FROM stat_nodes WHERE k = %s "
        rows = self.fetchquery(qry, (tag, ))
        return rows[0][0]


    def countways(self, tag):
        """Count the number of ways with a tag"""
        qry = "SELECT count(node_id) FROM stat_ways WHERE k = %s "
        rows = self.fetchquery(qry, (tag, ))
        return rows[0][0]


    def readways(self, tag):
        """read data from db"""
        qry = "SELECT node_id FROM stat_ways WHERE k = %s"
        rows = self.fetchquery(qry, (tag, ))
        return rows


    def officialvalues(self):
        """read data from db"""
        keva = {}
        keys = []
        rows = self.readoffkeys()
        for row in rows:
            keys.append(row[0])

        for key in keys:
            vals = []
            values = self.readoffvalues(key)
            for value in values:
                vals.append(value[0])
            keva[key] = vals
        return keva

    def readoffkeys(self):
        qry = "SELECT DISTINCT(k) FROM official_key_value "
        rows = self.fetchquery(qry)
        return rows

    def readoffvalues(self, key):
        """Read official values for a key"""
        subqry = "SELECT v FROM official_key_value WHERE k = %s"
        values = self.fetchquery(subqry, ( key, ))
        return values

    def readalls(self, key, datas):
        """read data from db"""
        content = ''
        values = str(datas)[1:-1]
        qry = "SELECT DISTINCT(node_id) FROM ("
        qry += "SELECT node_id FROM stat_nodes WHERE k = %s "
        qry += " AND v NOT IN (%s) ) AS toto" % values
        rows = self.fetchquery(qry, (key, ))
        for row in rows:
            content += self.nodedownload(row[0])
        return content

    def badvalues(self):
        """down obj"""
        rows = self.officialvalues()
        for key in rows.keys():
            content = self.readalls(key, rows[key])
            self.writecontent("bad-values-%s" % key, content)

    def readnodes_orphan(self, offset):
        """read data from db"""
        qry = "SELECT id FROM wmm_nodes_orphan ORDER BY geom LIMIT 85 OFFSET %s"
        rows = self.fetchquery(qry, (offset, ))
        return rows

    def nodes_orphan(self):
        """down obj"""
        ite = 0
        rows = self.readnodes_orphan(ite)
        while (len(rows) > 84 and ite < 3):
            xnod = 0
            content = ''
            for row in rows:
                xnod += 1
                if xnod > 84:
                    continue
                content += self.nodedownload(row[0])
            self.writecontent("nodes-orphan-%s" % ite, content)
            ite += 1
            rows = self.readnodes_orphan(ite)

    def writecontent(self, tag, content):
        """Write the full file"""
        if len(content) > 0:
            datas = "<?xml version='1.0' encoding='UTF-8'?>\n"
            datas += "<osm version='0.6' generator='Osmosis 0.36'>\n"
            datas += content
            datas += "</osm>\n"
            fpath = os.path.join(self.path, "%s.osm" % tag )
            writefile(fpath, datas)
            f_zip = ZipFile(fpath+".zip", "w", ZIP_DEFLATED)
            f_zip.write(fpath, os.path.basename(fpath))
            bzdatas = bz2.compress(datas)
            writefile(os.path.join(self.path, "%s.osm.bz2" % tag), bzdatas)
            print "Wrote %s" % fpath


    def connect(self, dbname):
        '''Open a connection to the database'''
        try:
            conn = psycopg2.connect("dbname = " + dbname)
            conn.set_isolation_level(0)
        except psycopg2.OperationalError, error:
            print error
            sys.exit(1)
        self._conn = conn


    def fetchquery(self, query, args = None):
        """Execute a query on the database"""
        cur = self._conn.cursor()
        cur.execute(query, args )
        rows = cur.fetchall()
        return rows
