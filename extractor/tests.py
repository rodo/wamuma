#! /usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2011 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""Tests for tagfix.py command line tool"""
import os
import unittest

from extractor import Extractor
from utils import loadfile

class ExtractorTests(unittest.TestCase):
    """Test class"""
    def setUp (self):
        for fi in ['/dev/shm/foobar.osm.zip', '/dev/shm/foobar.osm']:
            if os.path.exists(fi):
                os.unlink(fi)
        self.extract = Extractor()

    def test_loadclass(self):
        """Test if we can load the class"""
        self.assertTrue(self.extract.version == "0.0.3",
                        "Version number")

    def test_writecontent(self):
        """Test if we can load the class"""
        self.extract.path = "/dev/shm"
        self.extract.writecontent("foobar", "foo")
        self.assertTrue(os.path.exists("/dev/shm/foobar.osm"),
                        "Version number")

    def test_writecontent2(self):
        """Test if we can load the class"""
        self.extract.path = "/dev/shm"
        self.extract.writecontent("foobar", "foo")
        self.assertTrue(os.path.exists("/dev/shm/foobar.osm.zip"),
                        "Version number")

    def test_writecontent3(self):
        """Null content do not create files"""
        self.extract.path = "/dev/shm"
        self.extract.writecontent("foobar", "")
        self.assertFalse(os.path.exists("/dev/shm/foobar.osm.zip"),
                        "Version number")


    def test_utils_loadfile(self):
        """Null content do not create files"""
        with open('/dev/shm/foobar','w') as ffoo:
            ffoo.write("Line 1\n")
            ffoo.write("Line 2\n")
            ffoo.write("Line 3\n")
            ffoo.write("Line 4\n")
            ffoo.write("Line 5\n")

        text = loadfile('/dev/shm/foobar')
        self.assertTrue(text ==  "Line 3\nLine 4\n",
                        text)



if __name__ == '__main__':
    unittest.main()
