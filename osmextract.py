#! /usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2011 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""osmextract.py extract osm objects with api"""
import sys
from optparse import OptionParser
from extractor.extractor import Extractor

__version__ = "1.0.0"

def main():
    """Main function"""
    arg_list = "-d DATABASE -p PATHNAME -k KEY"
    usage = "Usage: %prog " + arg_list
    parser = OptionParser(usage, version=__version__)
    parser.add_option("-d", "--database", dest="dbname",
                      help="database to connect to", 
                      default=None)
    parser.add_option("-b", "--bad-values", dest="badvalues",
                      help="key to extract", 
                      action="store_true",
                      default=False)
    parser.add_option("-o", "--nodes-orphan", dest="orphan",
                      help="nodes orphan", 
                      action="store_true",
                      default=False)
    parser.add_option("-k", "--key", dest="key",
                      help="key to extract", 
                      default=None)
    parser.add_option("-p", "--path", dest="path",
                      help="pathname to store osm files", 
                      default=None)
    options = parser.parse_args()[0]
    if options.dbname == None:
        sys.exit("Missing -d option")
    if options.key == None and not options.badvalues and not options.orphan:
        sys.exit("Missing -k option")
    if options.path == None:
        sys.exit("Missing -p option")

    print "Run extractor %s on %s" % (__version__, options.dbname)
    ex = Extractor(options.dbname, options.path)
    if options.key != None:
        ex.download(options.key)
    if options.badvalues == True:
        ex.badvalues()
    if options.orphan == True:
        ex.nodes_orphan()

if __name__ == '__main__':
    main()
