# -*- coding: utf-8 -*-
#
# Copyright (c) 2011 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""Read datas from a Postgresql database"""
import sys
import syslog
from psycopg2 import connect, OperationalError, ProgrammingError, InternalError
#import psycopg2.extensions

class WmmDatabase():
    """The database object"""

    _dbuser = None
    _dbpass = None
    _dbhost = None
    _dbport = None


    def __init__(self, dbname, files, filesopt, autoconnect=True):

        self.name = dbname
        self.conn = None
        self.files = files
        self.filesopt = filesopt
        self.tablespace = "pg_default"
        
        if autoconnect:
            self.connect(dbname)


    def connect(self, dbname):
        '''Open a connection to the database'''
        try:
            conn = connect(self.dsnget())
            conn.set_isolation_level(0)
        except OperationalError, error:
            print "%s %s " % (dbname, error)
            sys.exit(1)
        self.conn = conn


    def dsnget(self):
        """Return the full database source name"""
        dsn = "dbname = %s" % ( self.name )
        if self._dbuser != None:
            dsn = "%s user=%s" % (dsn, self._dbuser)
        if self._dbpass != None:
            dsn = "%s user=%s" % (dsn, self._dbpass)
        if self._dbhost != None:
            dsn = "%s host=%s" % (dsn, self._dbhost)
        if self._dbport != None:
            dsn = "%s port=%s" % (dsn, self._dbport)
        return dsn


    def exeq(self, query, args = None):
        """Execute a query on the database without output

        Return 1 in case of error 0 if not

        """
        res = 0
        cur = self.conn.cursor()
        try:
            cur.execute(query, args)
        except ProgrammingError, error:
            print "%s %s\n%s" % (self.name, 
                                 cur.mogrify(query, args),
                                 error)
            res = 1
        except InternalError, error:
            print "InternalError %s in : %s\n%s " % (self.name,
                                                     cur.mogrify(query, args),
                                                     error)
            res = 1
        return res


    def query(self, query, args = None):
        """Execute a query on the database"""
        cur = self.conn.cursor()
        if (args == None):
            cur.execute(query)
            rows = None
        else:
            cur.execute(query, args)
            rows = cur.fetchall()
        return rows

    def fetchquery(self, query, args = None):
        """Execute a query on the database"""
        cur = self.conn.cursor()
        try:
            cur.execute(query, args)
            rows = cur.fetchall()
        except ProgrammingError, error:
            print self.name
            print error
            print cur.mogrify(query, args)
            rows = []
        except TypeError, error:
            print self.name
            print error
            print cur.mogrify(query, args)
            rows = []
        return rows

    
    def exists(self, name):
        """Return True/False based if a database exists or not"""
        query = "SELECT COUNT(*) FROM pg_catalog.pg_database WHERE datname = %s"
        res = self.query(query, (name, ))
        return (res[0][0] > 0)

    def initnew(self, verbose=False):
        """Initialize a new db"""
        syslog.syslog("Initialize database : %s" % self.name)
        if verbose:
            print "Initialize database : %s" % self.name
        if not self.langexists():
            self.createlang(verbose)
        self.playfiles(self.files, verbose)

    def initstat(self, verbose=False):
        """Init a stat database"""
        self.playfiles(self.filesopt, verbose)
        files = ['sql/tables_stat.sql',
                 'sql/views_stat.sql',
                 'sql/data-wellknown-keys.sql']
        self.playfiles(files, verbose)

    def updatestat(self, verbose=False):
        """Update a statistical database"""
        files = [ 'sql/views_stat_wms.sql' ]
        self.playfiles(files, verbose)

    def playfiles(self, files, verbose=False):
        """Run scripts on database"""
        for sqfile in files:
            if verbose:
                print "Play script file : %s" % sqfile
            cur = self.conn.cursor()
            procedures  = open(sqfile,'r').read() 
            cur.execute(procedures)


    def create(self, name, verbose=False):
        """Create a new database"""
        syslog.syslog("Create database : %s" % name)
        if verbose:
            print "Create database : %s" % name
        query = u"CREATE DATABASE %s TABLESPACE %s" % ( name,
                                                      self.tablespace )
        self.query(query)
        return self.exists(name)


    def createlang(self, verbose=False):
        """Create a new lang in the db"""
        syslog.syslog("Create language : plpgsql")
        if verbose:
            print "Create language : plpgsql"
        query = u"CREATE LANGUAGE plpgsql"
        self.query(query)
        return True

    def langexists(self, lang = 'plpgsql'):
        """Return True/False based if a database exists or not"""
        query = "SELECT COUNT(*) FROM pg_catalog.pg_language WHERE lanname = %s"
        res = self.query(query, ( lang, ))
        return (res[0][0] > 0)


    def dbdrop(self, name):
        """Create a new database"""
        query = u"DROP DATABASE %s" % name
        self.query(query)
        return self.exists(name)

    def readcountry(self, countid):
        """Return True/False based if a database exists or not"""
        query = "SELECT name, dir,continent_id FROM country WHERE id = %s"
        rows = self.query(query, (countid, ))
        return rows[0]


    def readcountryurl(self, countid):
        """Return the special url for a country"""
        url = None
        query = "SELECT url FROM country_url WHERE country_id = %s"
        rows = self.query(query, (countid, ))
        if len(rows) > 0:
            url = rows[0][0]
        return url


    def readcountrylangs(self, countid):
        """Return the langs defined for a country"""
        langs = []
        query = "SELECT lang_code FROM country_lang WHERE country_id = %s"
        rows = self.query(query, (countid, ))
        for row in rows:
            langs.append(row[0])
        return langs

    def readcountrymap(self, countid):
        """Return the langs defined for a country"""
        query = "SELECT map_zoom, map_lon, map_lat  FROM country WHERE id = %s"
        rows = self.query(query, (countid, ))
        row = rows[0]

        return (row)


    def readcontinent(self, contid):
        """Return True/False based if a database exists or not"""
        query = "SELECT name, dir FROM continent WHERE id = %s"
        rows = self.query(query, (contid, ))
        return rows[0]

    def initmaindb(self, verbose=False):
        """Initialize the main database"""
        files = [ 'tables', 'views', 'datas' ]
        for sqfile in files:
            filename = "sql/%s.sql" % sqfile
            if verbose:
                print "Play script file : %s" % filename
            cur = self.conn.cursor()
            script  = open(filename,'r').read() 
            cur.execute(script)
