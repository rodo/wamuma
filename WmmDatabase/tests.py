#! /usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2011 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# pylint: disable=C0103
"""Unit tests for WmmDatabase class

All the tests are made on a postgresql database called 'foo'

"""

import os, sys

sys.path.append('../')

import unittest

from __init__ import WmmDatabase

__dbname__ = "foo"

class TestsWmmPsql(unittest.TestCase):
    """Database parts"""

    def test_dsnget(self):
        """Connection to db"""
        wmm = WmmDatabase(__dbname__, None, None)
        self.assertTrue(wmm.name == __dbname__, 
                        'Class Instantation')
        
    def test_exeq(self):
        """Successful execute statement without output"""
        wmm = WmmDatabase(__dbname__, None, None)
        res = wmm.exeq("DROP TABLE IF EXISTS footable")
        res = wmm.exeq("CREATE TABLE footable (foo int)")
        self.assertTrue(res  == 0, 
                        'Create a table res : %s' % res)

    def test_exeq_we(self):
        """Execute statement without output"""
        wmm = WmmDatabase(__dbname__, None, None)
        res = wmm.exeq("DROP TABLE IF EXISTS footable")
        res = wmm.exeq("CREATE TABLE footable (foo intoo)")
        self.assertTrue(res  == 1, 
                        'Create a table res : %s' % res)


    def test_exeq_ins(self):
        """Successful execute statement with output"""
        wmm = WmmDatabase(__dbname__, None, None)
        res = wmm.exeq("CREATE TABLE IF NOT EXISTS footable (foo int)")
        res = wmm.exeq("INSERT INTO footable values (42)")
        self.assertTrue(res  == 0, 
                        'Insert a record : %s' % res)

    def test_exeq_update(self):
        """Successful execute statement with output"""
        wmm = WmmDatabase(__dbname__, None, None)
        res = wmm.exeq("CREATE TABLE IF NOT EXISTS footable (foo int)")
        res = wmm.exeq("UPDATE footable SET foo = 44 WHERE foo = 42")
        self.assertTrue(res  == 0, 
                        'Insert a record : %s' % res)



    def test_fetchquery(self):
        """Successful execute statement with output"""
        arr = [(42,'foobar')]
        wmm = WmmDatabase(__dbname__, None, None)
        red = wmm.exeq("DROP TABLE IF EXISTS footable")
        rec = wmm.exeq("CREATE TABLE footable (foo int, bar varchar(24))")
        rei = wmm.exeq("INSERT INTO footable values (42,'foobar')")
        res = wmm.fetchquery("SELECT * FROM footable")
        self.assertTrue(red  == 0, 'Drop table was unsuccessful')
        self.assertTrue(rec  == 0, 'Create table was unsuccessful')
        self.assertTrue(rei  == 0, 'Insert was not done')
        self.assertTrue(res  == arr, 'Fetch one line %s' % res)

    def test_fetchquery_arg(self):
        """Successful execute statement with output"""
        arr = [(42,'foobar'),(42,'barfoo')]
        wmm = WmmDatabase(__dbname__, None, None)
        red = wmm.exeq("DROP TABLE IF EXISTS footable")
        rec = wmm.exeq("CREATE TABLE footable (foo int, bar varchar(24))")
        rei = wmm.exeq("INSERT INTO footable values (44,'barfoo')")
        rei = wmm.exeq("INSERT INTO footable values (42,'foobar')")
        rei = wmm.exeq("INSERT INTO footable values (42,'barfoo')")
        self.assertTrue(red  == 0, 'Drop table was unsuccessful')
        self.assertTrue(rec  == 0, 'Create table was unsuccessful')
        self.assertTrue(rei  == 0, 'Insert was not done')
        res = wmm.fetchquery("SELECT * FROM footable WHERE foo = %s", (42, ))
        self.assertTrue(res  == arr,  'Fetch one line %s' % res)

    def test_fetchquery_arg1(self):
        """Successful execute statement with output and one arg"""
        arr = [(42,'foobar')]
        wmm = WmmDatabase(__dbname__, None, None)
        red = wmm.exeq("DROP TABLE IF EXISTS footable")
        rec = wmm.exeq("CREATE TABLE footable (foo int, bar varchar(24))")
        rei = wmm.exeq("INSERT INTO footable values (44,'barfoo')")
        rei = wmm.exeq("INSERT INTO footable values (42,'foobar')")
        rei = wmm.exeq("INSERT INTO footable values (42,'barfoo')")
        self.assertTrue(red  == 0, 'Drop table was unsuccessful')
        self.assertTrue(rec  == 0, 'Create table was unsuccessful')
        self.assertTrue(rei  == 0, 'Insert was not done')
        res = wmm.fetchquery("SELECT * FROM footable WHERE bar=%s", ("foobar", ))
        self.assertTrue(res  == arr,  'Not the expected result')


    def test_fetchquery_arg2(self):
        """Successful execute statement with 2 args"""
        arr = [(42,'foobar')]
        wmm = WmmDatabase(__dbname__, None, None)
        red = wmm.exeq("DROP TABLE IF EXISTS footable")
        rec = wmm.exeq("CREATE TABLE footable (foo int, bar varchar(24))")
        rei = wmm.exeq("INSERT INTO footable values (44,'barfoo')")
        rei = wmm.exeq("INSERT INTO footable values (42,'foobar')")
        rei = wmm.exeq("INSERT INTO footable values (42,'barfoo')")
        self.assertTrue(red  == 0, 'Drop table was unsuccessful')
        self.assertTrue(rec  == 0, 'Create table was unsuccessful')
        self.assertTrue(rei  == 0, 'Insert was not done')
        res = wmm.fetchquery("SELECT * FROM footable WHERE foo=%s AND bar=%s", (42, "foobar" ) )
        self.assertTrue(len(res) == 1, 'Two many lines returned')
        self.assertTrue(res  == arr, 'Result is not the one we thought')





if __name__ == '__main__':
    unittest.main()
