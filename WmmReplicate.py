# -*- coding: utf-8 -*-
#
# Copyright (c) 2011 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""Diff files"""

import os
import re
import threading
import LibHDM
import syslog

class ThreadReplicate(threading.Thread):
    """Launch a new thread"""

    def __init__(self, host, dirname, localdir):
        '''Initialization'''
        threading.Thread.__init__(self)
        self.host = host
        self.dirname = dirname
        self.localdir = localdir
        self.actualsequ = 0

    def run(self):
        """Run the thread"""
        obj = WmmReplicate(self.host, self.dirname, self.localdir)
        self.actualsequ = obj.parsestate()
        obj.fetchstate()
        sequ = obj.parsestate()
        while (self.actualsequ < sequ):            
            obj.fetchosc(self.actualsequ)
            self.actualsequ = self.actualsequ + 1

class WmmReplicate():
    """Options in config file"""

    def __init__(self, host, repdir, dirname):
        """Initialisation"""

        self.host = host
        self.dirname = dirname
        self.repdir = repdir
        self.statename = os.path.join(self.dirname, "state.txt")

    def parsestate(self):
        """Check if line match the regexp"""
        datas = re.compile(r"^sequenceNumber=(\d+)", re.DOTALL)
        for lix in open(self.statename).readlines():
            vals = datas.match(lix)
            if vals != None:
                sequ = int(vals.group(1))
                syslog.syslog("Local last sequence is %d" % sequ)
        return sequ


    def fetchstate(self):
        """Check if line match the regexp"""
        url = self.stateurl()
        filename = os.path.join(self.dirname, "state.txt")
        return LibHDM.wgetfile(url, filename)


    def fetchosc(self, sequ):
        """Fetch diff data files"""
        url = self.oscurl(sequ)
        txturl = self.txturl(sequ)
        filename = os.path.join(self.dirname, seqpath(sequ))
        if not os.path.exists(os.path.dirname(filename)):
            os.makedirs(os.path.dirname(filename))
        if not os.path.exists(filename + ".osc.gz"):
            LibHDM.wgetfile(url, filename + ".osc.gz")
        if not os.path.exists(filename + ".state.txt"):
            LibHDM.wgetfile(txturl, filename + ".state.txt")

    def oscurl(self, sequ):
        """Build statfile url"""
        fpath = seqpath(sequ) +".osc.gz"
        return os.path.join(self.host, self.repdir, fpath)

    def txturl(self, sequ):
        """Build state txt url"""
        fpath = seqpath(sequ) +".state.txt"
        return os.path.join(self.host, self.repdir, fpath)

    def stateurl(self):
        """Build statfile url"""
        url = self.host + "/" + self.repdir + "/state.txt"
        return url

def seqpath(sequ):
    """Calculate path for a sequence"""
    seqx = "%09d" % sequ
    return os.path.join(seqx[0:3], seqx[3:6], seqx[6:9])

if __name__ == "__main__":
    
    CONF = WmmReplicate("benin.poly")

