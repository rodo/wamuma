#! /usr/bin/python
# -*- coding: utf-8 -*-

import os, sys
import syslog
sys.path.append('../')

import unittest

from LibHDM import execcmd, shplogparser, shploggenerator, hdmosm2pgsql
from LibExosm import filedate


class TestsCmd(unittest.TestCase):

    def setUp (self):
        """Delete files"""
        for fi in ['/dev/shm/foobar', 
                   '/dev/shm/bar.log.txt', 
                   '/dev/shm/bar.txt']:
            if os.path.exists(fi):
                os.unlink(fi)

    def test_execcmd(self):
        """Execute an external command"""
        args = ["""/usr/bin/touch""","""/dev/shm/foobar"""]
        res = execcmd(args)
        assert (res == 0), 'execute external command'

    def test_execlog(self):
        """Execute an external command with log

        Assume that a log file will be create
        """
        args = ["""ls""","""-l"""]
        filen = "/dev/shm/bar.log.txt"
        execcmd(args, filen)
        assert os.path.exists(filen), 'exec command and output in file'


    def test_execlogerr(self):
        """Execute an external command that will failed

        Assume that an error log file will be create
        """
        args = ["""ls""","""-lKK"""]
        filea = "/dev/shm/bar.log.txt"
        filee = "/dev/shm/bar.logerr.txt"
        execcmd(args, filea, filee)
        assert os.path.exists(filee), 'exec command and output in file'


    def test_execperl(self):
        args = ["""/usr/bin/perl""", """-e""", """'4 / 0'"""]
        res = execcmd(args)
        assert (res == 0), 'execute external perl command'
        
    def hdmosm2pgsql(self):

        dbname = "gis"
        style = "style_hdm_3.0_light.style"
        dirname = "/tmp"
        filename = "benin.osm.bz2"

        res = hdmosm2pgsql(dbname, style, dirname, filename)

        assert (res == 0), 'osm2pgsql'

    def test_syslog(self):
        syslog.syslog('runtest')
        assert (syslog.LOG_ALERT == 1), 'syslog is functionnal'

    def test_filedate(self):
        res = execcmd(["/usr/bin/touch", "/dev/shm/bar"])
        file1 = "/dev/shm/bar"
        file2 = "TestsCmd.py"
        assert (filedate(file1) > filedate(file2)), 'filedate'

    def test_shplogparser(self):
        res = shplogparser("shplog.err")

        assert (len(res) == 15), 'parse shape logs'

    def test_shplogparsersorted(self):
        res = shplogparser("shplog.err")

        assert (res[1] == ('BOUNDARY_A', 'boundary_admin_level3_name') ), 'parse shape logs'

    def test_shploggenerator(self):
        objects = ('line', 'point')
        filen = "/dev/shm/shplogdir/foo/sherr.foo.fields_conversion.txt"
        res = shploggenerator("/dev/shm/shplogdir", 
                                     "sherr",
                                     "foo", objects)

        assert res == filen, 'parse shape logs'

if __name__ == '__main__':
    unittest.main()
