#! /usr/bin/python
# -*- coding: utf-8 -*-

import os, sys
sys.path.append('../')
import unittest
import zipfile

from LibCompress import *

class TestsCompressor(unittest.TestCase):
    """
    def setUp (self):
        for fi in ['/dev/shm/foobar.zip', '/dev/shm/foo.txt', '/dev/shm/bar.txt', '/dev/shm/benin.osm']:
            if os.path.exists(fi):
                os.unlink(fi)
    """        

    def test_zip_file(self):
        with open("""/dev/shm/bigfoo.txt""",'w') as foo:
            i = 0
            while (i < 1024 * 1024):
                foo.write("1")
                i += 1
        zip_file('/dev/shm/foobar.zip', 'w', '/dev/shm/bigfoo.txt')

        z = zipfile.ZipFile('/dev/shm/foobar.zip','r')
        l = z.namelist()        
        assert l[0] == 'bigfoo.txt', 'test zip_file'

    def test_newfile(self):
        open("""/dev/shm/foo.txt""",'w').write("foo")
        zipnewfile('/dev/shm/foobar.zip','/dev/shm/foo.txt')
        z = zipfile.ZipFile('/dev/shm/foobar.zip','r')
        l = z.namelist()        
        assert l[0] == 'foo.txt', 'new zip file'

    def test_addfile(self):
        with open("""/dev/shm/bigfoo.txt""",'w') as foo:
            i = 0
            while (i < 1024 * 1024):
                foo.write("1")
                i += 1
        open("""/dev/shm/bar.txt""",'w').write("bar")
        zipnewfile('/dev/shm/barfoo.zip','/dev/shm/bigfoo.txt')
        zipaddfile('/dev/shm/barfoo.zip','/dev/shm/bar.txt')
        z = zipfile.ZipFile('/dev/shm/barfoo.zip','r')
        l = z.namelist()        
        assert l[1] == 'bar.txt', 'add zip file'

    def test_ubz2(self):
        outfile = """/dev/shm/benin.osm"""

        unbzip2("""benin.osm.bz2""", outfile)
        
        assert (os.path.exists(outfile)), 'Uncompress bz2 file'

if __name__ == '__main__':
    unittest.main()
