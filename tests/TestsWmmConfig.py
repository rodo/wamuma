#! /usr/bin/python
# -*- coding: utf-8 -*-

import os, sys, stat
import syslog

sys.path.append('../')

import unittest

from WmmConfig import *


class TestsWmmConfig(unittest.TestCase):
    """Testg the config file reader"""
    def setUp (self):
        for fi in ['/dev/shm/foobar.cfg', '/dev/shm/fooexec' ]:
            if os.path.exists(fi): os.unlink(fi)
        dirname = "/dev/shm"
        self.cfgfile = os.path.join(dirname, "foo.cfg")
        with open(self.cfgfile,'w') as foo:
            foo.write("[wamuma]\n")
            foo.write("dbname=bar\n")
            foo.write("wwwdir=/dev/shm\n")

    def test_readconfig1(self):
        """Read configuration file"""
        conf = WmmConfig(self.cfgfile)
        assert conf.dbname == 'bar', 'Read configuration file'

    def test_readconfig2(self):
        """Read configuration file"""
        conf = WmmConfig(self.cfgfile)
        assert conf.downthreads == 4, 'Default parms'


    def test_checkdirectory1(self):
        """Directory exists"""
        res = checkdirectory("foobar","/dev/shm")
        self.assertTrue( res[0] == 0, "Dir must exists")

    def test_checkdirectory2(self):
        """Directory does not exists"""
        res = checkdirectory("foobar","/foobar/foobar")
        self.assertTrue( res[0] == 1, "Dir must not exists")

    def test_checkexecutbale(self):
        """Configuration file"""
        fpath = "/dev/shm/fooexec"
        with open(fpath,'w') as foo:
            foo.write("echo\n")
        os.chmod(fpath, stat.S_IWUSR + stat.S_IRUSR + stat.S_IXUSR)
        res = checkexecutable("foobar",fpath)
        self.assertTrue( res[0] == 0 , 'The file is not executable')

    def test_isyes1(self):
        self.assertTrue(strisyes('yes'), 'yes is true')

    def test_isyes2(self):
        self.assertTrue(strisyes('Yes'), 'yes is true')

    def test_isyes3(self):
        self.assertTrue(strisyes(' YES '), 'yes is true')

    def test_isyes4(self):
        self.assertFalse(strisyes('No'), 'Yes, no is not yes')

    def test_isyes5(self):
        self.assertTrue(strisyes('1'), 'yes is true')

    def test_isyes6(self):
        self.assertTrue(strisyes('true'), 'yes is true')

    def test_isyes7(self):
        self.assertTrue(strisyes('on'), 'yes is true')

    def test_parsegarmin (self):
        dirname = "/dev/shm"
        self.cfgfile = os.path.join(dirname, "foo.cfg")
        with open(self.cfgfile,'a') as foo:
            foo.write("garmin = yes\n")
            foo.write("[garmin]\n")
            foo.write("mkgmap=bar\n")
            foo.write("[java]\n")
            foo.write("java=foo\n")
        conf = WmmConfig(self.cfgfile)
        self.assertTrue(conf.garmin, 'Garmin enabled')

    def test_parsegarmin2 (self):
        dirname = "/dev/shm"
        self.cfgfile = os.path.join(dirname, "foo.cfg")
        with open(self.cfgfile,'a') as foo:
            foo.write("garmin = yes\n")
            foo.write("[garmin]\n")
            foo.write("mkgmap=bar\n")
            foo.write("multi-language=yes\n")
            foo.write("[java]\n")
            foo.write("java=foo\n")
        conf = WmmConfig(self.cfgfile)
        self.assertTrue(conf.garminopts['multilang'], 
                        'Garmin multi lang enabled')

    def test_parsegarmin3 (self):
        dirname = "/dev/shm"
        self.cfgfile = os.path.join(dirname, "foo.cfg")
        with open(self.cfgfile,'a') as foo:
            foo.write("garmin = yes\n")
            foo.write("[garmin]\n")
            foo.write("mkgmap=bar\n")
            foo.write("[java]\n")
            foo.write("java=foo\n")
        conf = WmmConfig(self.cfgfile)
        self.assertFalse(conf.garminopts['multilang'], 
                         'Garmin multi-lang disaabled')


    def test_parseshapes (self):
        dirname = "/dev/shm"
        self.cfgfile = os.path.join(dirname, "foo.cfg")
        with open(self.cfgfile,'a') as foo:
            foo.write("shapes = on\n")
            foo.write("[shapes]\n")
            foo.write("pgsql2shp=bar\n")
        conf = WmmConfig(self.cfgfile)
        self.assertTrue(conf.pgsql2shp == "bar", 'Shapes enabled')


if __name__ == '__main__':
    unittest.main()
