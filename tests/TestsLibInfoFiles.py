#! /usr/bin/python
# -*- coding: utf-8 -*-

import os, sys
import syslog
import unittest
sys.path.append('../')

from LibInfoFiles import *

class TestsWmmInfofiles(unittest.TestCase):
    """Info files"""

    def test_countries_by_continent(self):
        """Configuration file"""        
        dbname = "osm"
        dirname = "/dev/shm"
        countries_by_continent(dirname, dbname)
        self.assertTrue(os.path.exists(os.path.join(dirname, 
                                                    'countries_by_continent.txt')))


if __name__ == '__main__':
    unittest.main()
