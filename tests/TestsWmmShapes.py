#! /usr/bin/python
# -*- coding: utf-8 -*-

import os, sys
import syslog
import unittest
sys.path.append('../')

from WmmShapes import *

class TestsLibHDM(unittest.TestCase):
    def setUp (self):
        for fi in ['/dev/shm/foo.style', '/dev/shm/bar.style' ]:
            if os.path.exists(fi): os.unlink(fi)
        dirname = "/dev/shm"
        self.cfgfile = os.path.join(dirname, "foo.cfg")


    def test_filterfile1(self):
        self.assertTrue(filterfile('foobar.style'), 'True test')


    def test_filterfile2(self):
        self.assertFalse(filterfile('foobar.st'), 'True test')


    def test_filterfile3(self):
        self.assertFalse(filterfile('.style'), 'True test')


    def test_readstyles(self):
        shp = GenShapes()
        with open('/dev/shm/foo.style','w') as foo:
            foo.write("foobar\n")
        with open('/dev/shm/bar.style','w') as foo:
            foo.write("foobar\n")
        styles = readstyles('/dev/shm/')
        self.assertTrue(len(styles) == 2, '2 styles files exists')


    def test_getstyles1(self):
        shp = GenShapes()
        with open('/dev/shm/foo.style','w') as foo:
            foo.write("foobar\n")
        with open('/dev/shm/bar.style','w') as foo:
            foo.write("foobar\n")
        styles = shp.getstyles('/dev/shm/')
        self.assertTrue(len(styles) == 2, '2 styles files exists')

    def test_getstyles2(self):
        shp = GenShapes()
        with open('/dev/shm/foo.style','w') as foo:
            foo.write("foobar\n")
        with open('/dev/shm/badfoo.styl','w') as foo:
            foo.write("foobar\n")
        styles = shp.getstyles('/dev/shm/')
        self.assertTrue(len(styles) == 1, '1 styles files exists')

if __name__ == '__main__':
    unittest.main()
