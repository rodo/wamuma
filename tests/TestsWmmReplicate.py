#! /usr/bin/python
# -*- coding: utf-8 -*-

import os, sys
import syslog

sys.path.append('../')

import unittest

from WmmReplicate import *


class TestsWmmReplicate(unittest.TestCase):
    """Database parts"""

    def test_url(self):
        """Configuration file"""
        obj = WmmReplicate("http://foo.bar","day","/dev/shm")
        url = obj.stateurl()
        self.assertEqual(url, "http://foo.bar/day/state.txt",
                         'Url')

    def test_fetchstate(self):
        """Configuration file"""
        try:
            unlink("/dev/shm/state.txt")
        except:
            pass
        obj = WmmReplicate("http://planet.openstreetmap.org",
                           "hour-replicate",
                           "/dev/shm")
        res = obj.fetchstate()
        self.assertTrue(os.path.exists("/dev/shm/state.txt"),
                         'Url')

    def test_parsestate(self):
        """Configuration file"""
        try:
            unlink("/dev/shm/state.txt")
        except:
            pass
        with open("/dev/shm/state.txt","w") as state:
            state.write("#Sun Mar 13 11:02:09 UTC 2011\n")
            state.write("sequenceNumber=11485\n")
            state.write("timestamp=2011-03-13T11:00:00Z\n")
        obj = WmmReplicate("http://planet.openstreetmap.org",
                           "hour-replicate",
                           "/dev/shm")
        seq = obj.parsestate()
        self.assertEqual(seq, 11485, 'Bad parse value')

    def test_oscurl(self):
        """Osc File Url"""
        obj = WmmReplicate("http://foo",
                           "bar",
                           "/dev/shm")
        url = obj.oscurl(11485)
        self.assertEqual(url, "http://foo/bar/000/011/485.osc.gz",
                         'Osc file url : %s' % url)

if __name__ == '__main__':
    unittest.main()
