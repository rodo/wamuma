#! /usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2011 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""Tests for tagfix.py command line tool"""
import os, sys
import syslog
sys.path.append('../')

import unittest

from tagfix import *


class TestsMain(unittest.TestCase):
    """Test class"""

    def test_fake(self):
        """New tag is present"""
        self.assertTrue(True,
                        'Successful test')


if __name__ == '__main__':
    unittest.main()
