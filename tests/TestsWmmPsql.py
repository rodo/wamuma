#! /usr/bin/python
# -*- coding: utf-8 -*-

import os, sys

sys.path.append('../')

import unittest

from WmmPsql import connect, dbexists, dbcreate, dbdrop


class TestsWmmPsql(unittest.TestCase):
    """Database parts"""
    def setUp (self):
        for fi in ['/dev/shm/foobar.cfg' ]:
            if os.path.exists(fi): os.unlink(fi)

    def test_connect(self):
        """Connection to db"""
        bid = 0
        conn = connect("osm")
        bid = conn.get_backend_pid()
        assert bid > 1, 'db connection'

    def test_dbexistsTrue(self):
        """Connection to db"""
        conn = connect("osm")
        self.assertTrue(dbexists("osm","osm"), 
                        'db exists osm : true')

    def test_dbexistsFalse(self):
        """Connection to db"""
        conn = connect("osm")
        self.assertFalse(dbexists("osm","foobar"), 
                         'db exists foorbar : false')

    def test_dbdrop(self):
        """Connection to db"""
        conn = connect("osm")
        dbcreate("osm","foobar")
        dbdrop("osm","foobar")
        check = dbexists("osm","foobar")
        self.assertFalse(check, 'dbdrop')


    def test_dbcreate(self):
        """Connection to db"""
        conn = connect("osm")
        dbcreate("osm","foobar")
        check = dbexists("osm","foobar")
        dbdrop("osm","foobar")

        self.assertTrue(check, 'dbcreate')
        

if __name__ == '__main__':
    unittest.main()
