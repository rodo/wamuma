#! /usr/bin/python
# -*- coding: utf-8 -*-

import os, sys
import syslog
import unittest
from time import sleep
sys.path.append('../')
from LibHDM import *

class TestsLibHDM(unittest.TestCase):

    def setUp (self):
        for fi in ['/dev/shm/foobar', '/dev/shm/foo', 
                   '/dev/shm/bar', '/dev/shm/foold', '/dev/shm/foonew' ]:
            if os.path.exists(fi): os.unlink(fi)
        dirname = "/dev/shm"
        self.cfgfile = os.path.join(dirname, "foo.cfg")
        with open(self.cfgfile,'w') as foo:
            foo.write("[wamuma]\n")
            foo.write("dbname=bar\n")
            foo.write("wwwdir=/dev/shm\n")

    def test_execcmd(self):
        args = [ 'ls', '-a', '-l' ]
        res = execcmd(args, '/dev/shm/logexec', '/dev/shm/logexecerr', True)
        self.assertTrue(res == 0, 'Exec in shell')

    def test_execcmd1(self):
        args = [ 'cat', '/foobar/foo' ]
        res = execcmd(args, '/dev/shm/logexec', '/dev/shm/logexecerr', False)
        self.assertTrue(res <> 0, 'Exec in shell')

    def test_execcmd2(self):
        args = [ 'cat', '/etc/hostname' ]
        res = execcmd(args, '/dev/shm/logexec', '/dev/shm/logexecerr', False)
        self.assertTrue(res == 0, 'Exec in shell')

    def test_newgeneration1(self):
        with open('/dev/shm/fooold','w') as foo:
            foo.write("foobar\n")
        sleep(1)
        with open('/dev/shm/foonew','w') as foo:
            foo.write("foobar\n")

        self.assertTrue(newgeneration('/dev/shm/foonew','/dev/shm/fooold'), 'The first is newer')

    def test_newgeneration2(self):
        with open('/dev/shm/foo','w') as foo:
            foo.write("foobar\n")
        sleep(1)
        with open('/dev/shm/bar','w') as foo:
            foo.write("foobar\n")

        self.assertFalse(newgeneration('/dev/shm/foo','/dev/shm/bar'), 'The first is older')

    def test_newgeneration3(self):
        with open('/dev/shm/foo','w') as foo:
            foo.write("foobar\n")

        self.assertTrue(newgeneration('/dev/shm/foo','/dev/shm/NoneBar'), 'The second does not exists')

    def test_shapesqueriesbuild(self):
        data = [ ('foobar', ), ('foo', ), ('foobar', ) ]
        result = shapesqueriesbuild(data)
        self.assertTrue(len(data) == 3, 'arr size')
        self.assertTrue(len(data[0]) == 1, 'arr size')
        self.assertTrue(result == 'foobar,foo,foobar', 
                        'The right thing %s ' %  result)



if __name__ == '__main__':
    unittest.main()
