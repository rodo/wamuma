#! /usr/bin/python
# -*- coding: utf-8 -*-

import os, sys
import syslog
import unittest
sys.path.append('../')

from WmmPolygon import *

class TestsWmmPolygon(unittest.TestCase):
    """Database parts"""
    def test_match_pos(self):
        """Configuration file"""
        obj = WmmPolygon("foobar","benin.poly","/dev/shm/foobar.bbox")
        res = obj.match('7.7457505 8.475554')
        self.assertEqual(res.group(1), '7.7457505',
                         'Read configuration file')

    def test_match_neg(self):
        """Configuration file"""
        obj = WmmPolygon("foobar","benin.poly","/dev/shm/foobar.bbox")
        res = obj.match('-7.7457E+01 -8.475554')
        self.assertEqual(res.group(1), '-7.7457E+01',
                         'Read configuration file')

    def test_match_exp(self):
        """Configuration file"""
        obj = WmmPolygon("foobar","benin.poly","/dev/shm/foobar.bbox")
        res = obj.match('-7.7457E+01 8.47E+01')
        self.assertEqual(res.group(2), '8.47E+01',
                         'test_match_exp')

    def test_readfile(self):
        """Configuration file"""
        cfgfile = "benin.poly"
        obj = WmmPolygon("foobar","benin.poly","/dev/shm/foobar.bbox")
        obj.readfile(cfgfile)
        self.assertTrue(obj.lowerleft[0] < 7.7457505,
                         'Read configuration file')


    def test_evallowerleft(self):
        """Configuration file"""
        cfgfile = "benin.poly"
        obj = WmmPolygon("foobar","benin.poly","/dev/shm/foobar.bbox")
        obj.lowerleft = (42, 42)
        obj.evallowerleft( (-3, -3) )
        self.assertTrue(obj.lowerleft[0] == -3,
                         'Eval lower')

    def test_evalupperright(self):
        """Configuration file"""
        obj = WmmPolygon("foobar","benin.poly","/dev/shm/foobar.bbox")
        obj.upperright = (42, 42)
        obj.evalupperright( (43, 43) )
        self.assertTrue(obj.upperright[0] == 43,
                         'Eval lower')



if __name__ == '__main__':
    unittest.main()
