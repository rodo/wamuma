#! /usr/bin/python
# -*- coding: utf-8 -*-

import os, sys
import syslog
import httplib
sys.path.append('../')

import unittest

from LibHDM import writelogs, downloadfile
from LibExosm import filedate, getserver, testurl

class TestsExoms(unittest.TestCase):
    def test_server(self):
        (server, name, filename ) = getserver("http://cartosm.eu/bureaux.html")
        assert isinstance(server, httplib.HTTP) == True, 'server class'


    def test_filename(self):
        (server, name, filename ) = getserver("http://cartosm.eu/bureaux.html")
        assert filename == '/bureaux.html', 'filename'


    def test_servername(self):
        (server, name, filename ) = getserver("http://cartosm.eu/bureaux.html")
        assert name == ('cartosm.eu'), 'server name'


    def test_testurl(self):
        (lastmod) = testurl("http://cartosm.eu/bureaux.html")
        assert lastmod > 1295600000, 'bad lastmod'




if __name__ == '__main__':
    unittest.main()
