#! /usr/bin/python
# -*- coding: utf-8 -*-

import os, sys
import syslog
import urllib2
sys.path.append('../')

import unittest

from LibHDM import writelogs, downloadfile, filterfile, readstyles
from LibExosm import filedate

class TestsHdm(unittest.TestCase):
    def setUp (self):
        for fi in ['/dev/shm/foobar.zip', 
                   '/dev/shm/foo.txt', 
                   '/dev/shm/bar.txt', 
                   '/dev/shm/benin.osm',
                   '/dev/shm/foobar/foo.style',
                   '/dev/shm/foobar/barstyle',
                   '/dev/shm/foobar/foobar.style'
                   ]:
            if os.path.exists(fi):
                os.unlink(fi)
        for fi in ['/dev/shm/foobar']:
            try: os.rmdir(fi)
            except: pass

    def test_writelogs(self):
        req = urllib2.Request("http://beta.cartosm.eu/bureaux.html")
        response = urllib2.urlopen(req)
        datas = response.read()
        heads = response.info()        
        res = writelogs('/tmp/toto.log', heads)
        assert (res == 0), 'server class'

    def test_badfile(self):
        res = downloadfile("http://beta.cartosm.eu/bureaux.htmlTOTO","/tmp/data.txt","/tmp/log.txt")

        assert (res == 0), 'server class'

    def test_filterfile(self):
        filename = "foo.style"
        assert filterfile(filename) == True, 'Correct test'
        filename = "foobar"
        assert filterfile(filename) == False, 'Bad test'
        filename = ".style"
        assert filterfile(filename) == False, 'Bad test'

    def test_readstyles(self):
        dirname = "/dev/shm/foobar"
        os.mkdir(dirname)
        with open(os.path.join(dirname, "foo.style"),'w') as foo:
            foo.write("foo")
        with open(os.path.join(dirname, "barstyle"),'w') as bar:
            bar.write("bar")
        with open(os.path.join(dirname, "foobar.style"),'w') as foobar:
            foobar.write("foobar")
        files = readstyles(dirname)
        assert len(files) == 2, 'Number of files'
        assert files[0] == 'foobar', '%s == foobar' % files[1]


if __name__ == '__main__':
    unittest.main()
