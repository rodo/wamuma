# -*- coding: utf-8 -*-
#
# Copyright (c) 2011 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""Garmin generation"""
import os
import syslog
import threading

import LibCompress
import LibExosm
import LibHDM

class GenGarmin(threading.Thread):
    """Generate garmin maps"""

    def __init__(self, countryname, dirname, mkgmap, java):
        """Initialization"""
        threading.Thread.__init__(self)
        self.countryname = countryname
        self.dirname = dirname
        self.tmpdirname = os.path.join(dirname, "garmin") 
        self.mkgmap = mkgmap
        self.args = java
        self.lang = None
        LibExosm.checkdir(self.tmpdirname)

    def run(self):
        """Generate a Garmin image file"""
        osmtime = 0
        imgtime = 0
        osmfilename = "%s/%s.osm" % (self.dirname, self.countryname)
        imgname = "%s/%s.img.zip" % (self.dirname, self.countryname)
        imgsrc = "%s/gmapsupp.img" % (self.tmpdirname)

        if self.lang != None:
            self.tmpdirname = os.path.join(self.tmpdirname, self.lang)
            LibExosm.checkdir(self.tmpdirname)
            imgsrc = "%s/gmapsupp.img" % (self.tmpdirname)
            imgname = "%s/%s.img.%s.zip" % (self.dirname, 
                                            self.countryname, self.lang)


        syslog.syslog('%s : Do garmin map in %s' % (self.countryname, 
                                                    imgname))

        if os.path.exists("%s.bz2" % osmfilename):
            osmtime = LibExosm.filedate("%s.bz2" % osmfilename)
        if os.path.exists(imgname):
            imgtime = LibExosm.filedate(imgname)
        if osmtime:
            if osmtime > imgtime:
                if os.path.exists(osmfilename):
                    self.generate(self.lang)
                    LibCompress.zipnewfile(imgname, imgsrc)
                else:
                    msg = '%s : garmin file is newer / osm' % self.countryname
                    syslog.syslog(msg)
            else:
                msg = '%s : Garmin map is up to date' % self.countryname
                syslog.syslog(msg)
        else:
            syslog.syslog('%s does not exists' % (osmfilename))


    def generate(self, lang=None):
        """Generate map in a specified lang"""
        osmfilename = "%s/%s.osm" % (self.dirname, self.countryname)
        args = self.args + [ "-jar", self.mkgmap ]
        args = args + [ "--gmapsupp", 
                        "--output-dir=%s" % self.tmpdirname,
                        "--country-name=%s" % self.countryname,
                        ]
        if lang == None:
            logname = "%s/logs/garmin.log" % (self.dirname)
            logerr = "%s/logs/garmin.err" % (self.dirname)
            args = args + [ osmfilename ]
        else:
            logname = "%s/logs/garmin.%s.log" % (self.dirname, lang)
            logerr = "%s/logs/garmin.%s.err" % (self.dirname, lang )
            args = args + [ "--name-tag-list=name:%s,int_name,name" % lang,
                            osmfilename ]
        return LibHDM.execcmd(args, logname, logerr)
