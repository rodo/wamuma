#!/bin/sh

#
# Retrieve the last version of osmosis and store it in ~
#
#
LOCALDIR=`pwd`
LOCALUSER=`whoami`

URL=http://dev.openstreetmap.org/~bretth/osmosis-build/osmosis-latest.tgz
WGET=/usr/bin/wget
CURL=/usr/bin/curl
SVN=/usr/bin/svn


if [ -f default.cfg.dist ]; then
    cp default.cfg.dist default.cfg
else
    echo "Wrong directory, please execute from wamuma root dir"
    exit 1
fi

# Make external dir to download and install tools
mkdir -p external


if [ ! -d external/osmosis-0.38 ]; then

    if [ -x $WGET ]; then
	$WGET $URL -O external/osmosis-latest.tgz
    else
	if [ -x $CURL ]; then
	    $CURL $URL -o external/osmosis-latest.tgz
	else
	    echo "Error, install wget or curl before using this script please"
	    exit 1
	fi
    fi

    # uncompress the archive
    /bin/tar -xv -f external/osmosis-latest.tgz -C external/

    if [ ! -d external/osmosis-0.38/bin/osmosis ]; then
	echo "Please update your configuration file"
    else
	sed -i -e "s|osmobin=|osmobin=$LOCALDIR/external/osmosis-0.38/bin/osmosis|"
    fi
fi


# osm2pgsql parts

if [ ! -d external/osm2pgsql ]; then

    OSM2PGSRC=http://svn.openstreetmap.org/applications/utils/export/osm2pgsql/

    if [ -x $SVN ]; then
	$SVN co $OSM2PGSRC external/osm2pgsql
    else
	echo "Error, you need to install subversion to fetch osm2pgsql sources"	
    fi
fi


#
#createlang PLPGSQL osm
