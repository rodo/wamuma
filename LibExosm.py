# -*- coding: utf-8 -*-
#
# Copyright (c) 2011 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Main Library 

"""
import os
import syslog
import time
import httplib
from stat import ST_MTIME

def checkdir(dirname, verbose=False):
    """Create a directory if it not exists"""
    if not os.path.exists(dirname):
        if verbose:
            syslog.syslog("Make dir %s" % (dirname))
        os.makedirs(dirname)
    return dirname

def dircountries(continent, country):
    """Return the directory name for a continent and country"""
    return '%s/%s' % (continent, country)    

def getserver(url, host=None):
    """Return server for an url"""
    port = None

    netloc = httplib.urlsplit(url)[1]
    filename = httplib.urlsplit(url)[2]

    try:
        servername, port = netloc.split(':')[0], netloc.split(':')[1]
    except IndexError:
        servername  = netloc

    if host != None:
        servername = host

    if port == None:
        server = httplib.HTTP(servername)
    else:
        server = httplib.HTTP(servername, port)

    return server, servername, filename

def testurl(url, debug = 0, host= None):
    """Return the last modification time for a remote ressource"""
    if debug:
        print "Get url : %s" % url

    lastmod = None

    server, servername, filename = getserver(url, host)
    server.putrequest('HEAD', filename)
    server.putheader("User-Agent", "LibExosm.py")
    server.putheader("Host", servername)
    server.endheaders( )

    errcode, errmsg, replyheader = server.getreply( ) # read reply info headers

    if debug:
        print replyheader

    if errcode == 200:
        # replyheader is HTTPMessage object "
        try:
            lastmod = int(time.mktime(replyheader.getdate('Last-Modified')))
        except ValueError:
            pass
        except TypeError:
            pass

        if debug:
            print replyheader.getdate('Last-Modified')
            print lastmod
    elif errcode == 302:
        lastmod = testurl(replyheader.getheader('Location'))
    else:
        msg = 'Error sending request [%d] %s : %s ' % (errcode, errmsg, url)
        print msg
        syslog.syslog(msg)

    return lastmod


def filedate(pathname):
    """Return the last modification time for a file"""
    ctime = os.stat(pathname)[ST_MTIME]
    return ctime


if __name__ == '__main__':
    BENIN = "http://downloads.cloudmade.com/africa/benin/benin.osm.bz2"
    print testurl(BENIN, 0)
    print testurl("http://blog.rodolphe.quiedeville.org/public/smallold.png", 0)

    filedate('/etc/hosts')

