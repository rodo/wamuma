#! /usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2011 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""Tests for tagfix.py command line tool"""
import sys
sys.path.append('../')

import unittest
import psycopg2
from tagfix import TagFix
from utils import keyreplace
from WmmDatabase import *

class FooDb(WmmDatabase):
    """Fake class for Db"""
    def __init__(self, name = None):
        
        self.name = name

    def exeq(self, query, args = None):
        """Allways return ok"""
        return 0

    def fetchquery(self, query, args = None):
        res = [(42),(43)]
        if query == "SELECT src, dst, osmid FROM node_tagsub LIMIT %s":
            res = [('Source', 'source', 42),
                   ('source', 'source', 43),
                   ('Source', 'source', 44),
                   ('Foobar', 'foobar', 42),
                   ]

        if query == "SELECT src, dst, osmid FROM way_tagsub LIMIT %s":
            res = [('Source', 'source', 42),
                   ('source', 'source', 43),
                   ('Foo', 'foo', 44)
                   ]

        return res

class FakeOsmApi():
    """Fake OsmApi class"""
    def NodeGet(self, oid = None):
        """Return an OsmApi node"""
        nodes = {'42': {'lon' : 42, 'lat' : 42,
                        'tag' : {'Foo' : 'bar', 'Source' : 'bar' } },
                 '43': {'lon' : 43, 'lat' : 43,
                        'tag' : {'Foo' : 'bar', 'Source' : 'bar' } },
                 '44': {'lon' : 43, 'lat' : 43,  'tag' : {'Foo' : 'bar', 'source' : 'table', 'Source' : 'bar' } },
                 '450': {'lon' : 43, 'lat' : 43, 'tag' : {'Foo' : 'bar', 'source' : 'table', 'Source' : 'table' } },
                 '451': {'lon' : 43, 'lat' : 43, 'tag' : {'Foo' : 'bar', 'source' : 'table' } },
                 '460': {'lon' : 43, 'lat' : 43, 'tag' : {'Foo' : 'bar', 'source' : 'table', 'Source' : 'Table' } },
                 '461': {'lon' : 43, 'lat' : 43, 'tag' : {'Foo' : 'bar', 'source' : 'table', 'Source' : 'Table' } }
                 }

        return nodes[str(oid)]

    def WayGet(self, oid = None):
        """Return an OsmApi node"""
        nodes = {'42': {'lon' : 42, 'lat' : 42,
                        'tag' : {'Foo' : 'bar', 'Source' : 'bar' } },
                 '43': {'lon' : 43, 'lat' : 43,
                        'tag' : {'Foo' : 'bar', 'Source' : 'bar' } },
                 '44': {'lon' : 43, 'lat' : 43,  'tag' : {'Foo' : 'bar', 'source' : 'table', 'Source' : 'bar' } },
                 '450': {'lon' : 43, 'lat' : 43, 'tag' : {'Foo' : 'bar', 'source' : 'table', 'Source' : 'table' } },
                 '451': {'lon' : 43, 'lat' : 43, 'tag' : {'Foo' : 'bar', 'source' : 'table' } },
                 '460': {'lon' : 43, 'lat' : 43, 'tag' : {'Foo' : 'bar', 'source' : 'table', 'Source' : 'Table' } },
                 '461': {'lon' : 43, 'lat' : 43, 'tag' : {'Foo' : 'bar', 'source' : 'table', 'Source' : 'Table' } }
                 }

        return nodes[str(oid)]

    def WayUpdate(self, oid = None):
        """Return an OsmApi node"""
        return 0

    def NodeUpdate(self, oid = None):
        """Return an OsmApi node"""
        return 0



class TestsCmd(unittest.TestCase):
    """Test class"""
    def setUp (self):
        odb = FooDb()        
        self.fix = TagFix(odb)
        self.fix.api = FakeOsmApi()


    def test_classload(self):
        """The class is loaded"""
        version = self.fix.getversion()
        self.assertTrue(version == "1.0.1",
                        "Version return : %s" % self.fix.getversion())
        self.assertTrue(self.fix.nodechanged == 0,
                        "Node counter not right initialized")
        self.assertTrue(self.fix.waychanged == 0,
                        "Ways counter not right initialized")


    def test_getnodes(self):
        """Get all nodes"""
        res = self.fix.getnodes()
        self.assertTrue(len(res) == 4,
                        'The result has not the right size')
        self.assertTrue(len(res[0]) == 3,
                        'The result has not the right format')


    def test_getnode3(self):
        """Fetch the right node"""
        node = {'lon' : 42, 'lat' : 42,
                'tag' : {'Foo' : 'bar', 'Source' : 'bar' }}
        self.assertTrue(node == self.fix.getnode(42),
                        "The node is not as expected")


    def test_getways(self):
        """Get all ways"""
        res = self.fix.getways()
        self.assertTrue(len(res) == 3,
                        'The result has not the right size')
        self.assertTrue(len(res[0]) == 3,
                        'The result has not the right format')


    def test_keyreplace1(self):
        """New tag is present"""
        node = {'lon' : 42, 'lat' : 42,
                'tag' : {'Foo' : 'bar' }
                }
        new = {}
        new = keyreplace('Foo', 'foo', node)
        self.assertTrue(new['tag']['foo'] == 'bar',
                        'Successful test')


    def test_keyreplace2(self):
        """does not replace a tag"""
        node = {'lon' : 42, 'lat' : 42,
                'tag' : {'foo' : 'bar' }
                }
        new = keyreplace('foo', 'foo', node)
        self.assertTrue(new['tag']['foo'] == 'bar',
                        'Successful test')

    def test_keyreplace3(self):
        """Old tag is deleted"""
        node = {'lon' : 42, 'lat' : 42,
                'tag' : {'Foo' : 'bar' }
                }
        new = keyreplace('Foo', 'foo', node)
        self.assertFalse('Foo' in new['tag'],
                        'Successful test')

    def test_keyreplace4(self):
        """Tag not present not reapled"""
        node = {'lon' : 42, 'lat' : 42,
                'tag' : {'Foo' : 'bar' }
                }
        new = keyreplace('bar', 'foo', node)
        self.assertTrue( new == node,
                        'Successful test')

    def test_keyreplace5(self):
        """Tag not present not reapled"""
        node = {'lon' : 42, 'lat' : 42,
                'tag' : {'Foo' : 'bar' }
                }
        ref = {'lon' : 42, 'lat' : 42,
                'tag' : {'foo' : 'bar' }
                }
        new = keyreplace('Foo', 'foo', node)
        self.assertTrue( new == ref,
                        'Successful test')

    def test_keyreplace6(self):
        """Tag not present not replaced"""
        node = {'lon' : 42, 'lat' : 42,
                'tag' : {'Foo' : 'bar' }
                }
        new = keyreplace('Foo', 'foo', node)
        self.assertTrue( new != node,
                        'Successful test')

    def test_keyreplace7(self):
        """Tag not present not replaced"""
        foo = {'lon' : 42, 'lat' : 42,
                'tag' : {'Foo' : 'bar', 'foo' : 'bar' }
                }
        bar = {'lon' : 42, 'lat' : 42,
                'tag' : {'foo' : 'bar' }
                }
        new = keyreplace('Foo', 'foo', foo)
        self.assertTrue( new == bar,
                        'Successful test')



        


    def test_update_obj2(self):
        """Count Object modification"""
        self.fix.update_obj('node', 'Foo', 'foo', 42)
        self.assertTrue(self.fix.nodechanged == 1,
                        "Nodes changed : %s" % self.fix.nodechanged)

    def test_update_obj3(self):
        """Node was not changed because dst tag exists"""
        self.fix.update_obj('node', 'Source', 'source', 44)
        self.assertTrue(self.fix.nodechanged == 0,
                        "Nodes changed : %s" % self.fix.nodechanged)

    def test_update_obj4(self):
        """Object was not changed because dst tag exists"""
        old = self.fix.getnode(44)
        new = self.fix.update_obj('node', 'Source', 'source', 44)
        self.assertTrue(old == new,
                        "Nodes changed : %s" % self.fix.nodechanged)

    def test_update_obj5(self):
        """Object was not changed because dst tag exists"""
        self.fix.update_obj('node', 'Source', 'source', 460)
        self.assertTrue(self.fix.nodechanged == 0,
                        "Nodes changed : %s" % self.fix.nodechanged)

    def test_update(self):
        """Number of nodes changed"""
        self.fix.update()
        self.assertTrue(self.fix.nodechanged == 1,
                        "Nodes changed : %s" % self.fix.nodechanged)
        
    def test_update_node(self):
        """Node was not changed because dst tag exists"""
        new = self.fix.update_node('Source', 'source', 44)
        self.assertTrue(self.fix.nodechanged == 0,
                        "Nodes changed : %s" % self.fix.nodechanged)

    def test_update_node1(self):
        """Object was not changed because dst tag exists"""
        old = self.fix.getnode(451)
        new = self.fix.update_node('Source', 'source', 450)
        self.assertTrue(old == new,
                        "old : %s new : %s" % (old, new) )


    def test_update_way_wc(self):
        """Object was not changed because src tag does not exists"""
        old = self.fix.getway(451)
        new = self.fix.update_way('Source', 'source', 450)
        self.assertTrue(old == new,
                        "old : %s new : %s" % (old, new) )

    def test_update_way_woc(self):
        """Way was changed because src exists"""
        old = self.fix.getway(451)
        new = self.fix.update_way('Foo', 'foo', 450)
        self.assertTrue(old != new,
                        "old : %s new : %s" % (old, new) )

    def test_addchange(self):
        """addchange for one key"""
        old = self.fix.addchange('node','foo','foobar')
        self.assertTrue(old == 1,
                        "ok" )

    def test_addchange2(self):
        """addchange for 2 keys"""
        old = self.fix.addchange('node','bar','foo')
        old = self.fix.addchange('node','bar','foo')
        self.assertTrue(old == 2,
                        "ok : %d" % old)

    def test_addchange3(self):
        """addchange for 2 keys"""
        old = self.fix.addchange('node','foobar','foo')
        old = self.fix.addchange('node','foobar','foo')
        old = self.fix.addchange('way','foobar','foo')
        self.assertTrue(old == 3,
                        "ok : %d" % old)

    def test_addchange4(self):
        """normal output"""
        old = self.fix.addchange('node','bar','foo')
        old = self.fix.addchange('node','bar','foo')
        old = self.fix.addchange('way','Foobar','foobar')
        self.fix.printoutput(42)
        print "\n"
        print self.fix.output
        self.assertTrue(len(self.fix.output) > 20,
                        "ok : %d" % old)


    def test_mark_way_fixed(self):
        """normal output"""
        conn = psycopg2.connect("dbname=foobar")
        conn.set_isolation_level(0)
        cur = conn.cursor()
        cur.execute('DROP TABLE IF EXISTS stat_ways')
        cur.execute('CREATE TABLE stat_ways (node_id int, k text, got boolean, fixed boolean)')
        cur.execute("INSERT INTO stat_ways VALUES (%s, %s, %s, %s)", (42, 'foo', False, False) )
        cur.execute("INSERT INTO stat_ways VALUES (%s, %s, %s, %s)", (44, 'foo', False, False) )
        cur.execute("INSERT INTO stat_ways VALUES (%s, %s, %s, %s)", (45, 'foo', False, False) )
        conn.close()

        odb = WmmDatabase('foobar',None,None)
        fix = TagFix(odb)
        fix.api = FakeOsmApi()

        fix.mark_way_fixed(42,'foo')

        conn = psycopg2.connect("dbname=foobar")
        conn.set_isolation_level(0)
        cur = conn.cursor()
        cur.execute('SELECT count(*) FROM stat_ways WHERE fixed=%s', (True, ))
        res = cur.fetchall()
        conn.close()
        self.assertTrue(res[0][0] == 1, "Ways left")

    def test_mark_way_got(self):
        """normal output"""
        conn = psycopg2.connect("dbname=foobar")
        conn.set_isolation_level(0)
        cur = conn.cursor()
        cur.execute('DROP TABLE IF EXISTS stat_ways')
        cur.execute('CREATE TABLE stat_ways (node_id int, k text, got boolean, fixed boolean)')
        cur.execute("INSERT INTO stat_ways VALUES (%s, %s, %s, %s)", (42, 'foo', False, False) )
        cur.execute("INSERT INTO stat_ways VALUES (%s, %s, %s, %s)", (44, 'foo', False, False) )
        cur.execute("INSERT INTO stat_ways VALUES (%s, %s, %s, %s)", (45, 'foo', False, False) )
        conn.close()

        odb = WmmDatabase('foobar',None,None)
        fix = TagFix(odb)
        fix.api = FakeOsmApi()

        fix.mark_way_got(42,'foo')

        conn = psycopg2.connect("dbname=foobar")
        conn.set_isolation_level(0)
        cur = conn.cursor()
        cur.execute('SELECT count(*) FROM stat_ways WHERE got=%s', (False, ))
        res = cur.fetchall()
        conn.close()
        self.assertTrue(res[0][0] == 2, "Ways left")


    def test_mark_node_fixed(self):
        """normal output"""
        conn = psycopg2.connect("dbname=foobar")
        conn.set_isolation_level(0)
        cur = conn.cursor()
        cur.execute('DROP TABLE IF EXISTS stat_nodes')
        cur.execute('CREATE TABLE stat_nodes (node_id int, k text, got boolean, fixed boolean)')
        cur.execute("INSERT INTO stat_nodes VALUES (%s, %s, %s, %s)", (42, 'foo', False, False) )
        cur.execute("INSERT INTO stat_nodes VALUES (%s, %s, %s, %s)", (44, 'foo', False, False) )
        conn.close()
        odb = WmmDatabase('foobar',None,None)
        fix = TagFix(odb)
        fix.api = FakeOsmApi()
        fix.mark_node_fixed(42, "foo")
        left = fix.nb_nodes_left("foo")
        self.assertTrue(left[0][0] == 1, "Nodes left")


    def test_mark_node_got(self):
        """normal output"""
        conn = psycopg2.connect("dbname=foobar")
        conn.set_isolation_level(0)
        cur = conn.cursor()
        cur.execute('DROP TABLE IF EXISTS stat_nodes')
        cur.execute('CREATE TABLE stat_nodes (node_id int, k text, got boolean, fixed boolean)')
        cur.execute("INSERT INTO stat_nodes VALUES (%s, %s, %s, %s)", (42, 'foo', False, False) )
        cur.execute("INSERT INTO stat_nodes VALUES (%s, %s, %s, %s)", (42, 'foo', False, False) )
        cur.execute("INSERT INTO stat_nodes VALUES (%s, %s, %s, %s)", (42, 'foo', False, False) )
        cur.execute("INSERT INTO stat_nodes VALUES (%s, %s, %s, %s)", (44, 'foo', False, False) )
        cur.execute("INSERT INTO stat_nodes VALUES (%s, %s, %s, %s)", (45, 'foo', False, False) )
        conn.close()
        odb = WmmDatabase('foobar',None,None)
        fix = TagFix(odb)
        fix.api = FakeOsmApi()
        fix.mark_node_got(42,'foo')
        conn = psycopg2.connect("dbname=foobar")
        conn.set_isolation_level(0)
        cur = conn.cursor()
        cur.execute('SELECT count(*) FROM stat_nodes WHERE got=%s', (True, ))
        res = cur.fetchall()
        conn.close()
        self.assertTrue(res[0][0] == 3, "Nodes got")
