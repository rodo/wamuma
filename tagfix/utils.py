# -*- coding: utf-8 -*-
#
# Copyright (c) 2011 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""OSM objects manipulations"""

def keyreplace(src, dst, obj):
    """Replace a key by another one in an osm object

    - if dst key exists and has the same value as src key, 
    the src key will be deleted


    Return the osm object modified or not
    """
    vals = obj.copy()
    if 'tag' in vals and src != dst: 
        tags = vals['tag'].copy()
        if src in tags and not dst in tags:
            tags[dst] = tags[src]
            del tags[src]
            vals['tag'] = tags
        if src in tags and dst in tags:
            if tags[dst] == tags[src]:
                del tags[src]
                vals['tag'] = tags
    return vals
