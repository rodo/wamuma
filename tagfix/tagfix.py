# -*- coding: utf-8 -*-
#
# Copyright (c) 2011 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""Import OSM pbject and make modification on them"""

from utils import keyreplace

class TagFix():
    """Main class"""

    _version = u"1.0.1"
    _changes = {}


    def __init__(self, odb):
        """Initialisation"""
        self.odb = odb
        self.api = None
        self.nodechanged = 0
        self.waychanged = 0
        

    def getversion(self):
        """Return the class version number"""
        return self._version


    def getnodes(self, limit=1000):
        """Read nodes to modify from database

        Return an array of nodes
        """
        query = "SELECT src, dst, osmid FROM node_tagsub LIMIT %s"
        rows = self.odb.fetchquery(query, (limit , ))
        return rows


    def getways(self, limit=1000):
        """Read ways to modify from database 

        Return an array of ways
        """
        query = "SELECT src, dst, osmid FROM way_tagsub LIMIT %s"
        rows = self.odb.fetchquery(query, (limit , ))
        return rows


    def mark_node_fixed(self, oid, key):
        """Mark node as fixed in database"""
        qry = "UPDATE stat_nodes SET fixed=true, got=true "
        qry += " WHERE node_id = %s AND k=%s"
        return self.odb.exeq(qry, (str(oid) , key))
        

    def mark_way_fixed(self, oid, key):
        """Mark way as fixed in database"""
        qry = "UPDATE stat_ways SET fixed=true, got=true "
        qry += " WHERE node_id = %s AND k=%s"
        return self.odb.exeq(qry, (str(oid) , key ))


    def mark_node_got(self, oid, key):
        """Mark node as fixed in database"""
        qry = "UPDATE stat_nodes SET got=true WHERE node_id = %s AND k=%s"
        self.odb.exeq(qry, (str(oid) , key))

    def mark_way_got(self, oid, key):
        """Mark way as fixed in database"""
        qry = "UPDATE stat_ways SET got=true WHERE node_id = %s AND k=%s"
        self.odb.exeq(qry, (str(oid) , key))
        
    def nb_nodes_left(self, key):
        """Mark node as fixed in database"""
        qry = "SELECT count(*) FROM stat_nodes WHERE got=false AND k=%s"
        return self.odb.fetchquery(qry, (key , ))




    def getnode(self, nodeid):
        """Get a node from api
        
        Return an OsmApi node        
        """
        node = self.api.NodeGet(nodeid)
        return node

    def getway(self, wayid):
        """Get a way from api
        
        Return an OsmApi way        
        """
        way = self.api.WayGet(wayid)
        return way


    def update(self):
        """Main method that is called to do all the updates
        """
        for row in self.getnodes():
            self.update_obj('node', row[0], row[1], row[2])
        for way in self.getways():
            self.update_obj('way', way[0], way[1], way[2])


    def update_obj(self, osmtype, keysrc, keydst, oid):
        """Call the right update method for a generic object"""
        if osmtype == 'node':
            osmobject = self.update_node(keysrc, keydst, oid)
        elif osmtype == 'way':
            osmobject = self.update_way(keysrc, keydst, oid)
        
        return osmobject


    def update_node(self, keysrc, keydst, oid):
        """Change a key in a node by another one"""
        chgnode = None
        osmobject = self.getnode( oid )
        if osmobject != None:
            chgnode = keyreplace(keysrc, keydst, osmobject)
            if chgnode != osmobject:
                self.mark_node_fixed( oid, keysrc )
                self.nodechanged += 1
                if self.api != None:
                    self.api.NodeUpdate(chgnode)
                    self.addchange('node', keysrc, keydst)
                    print '%4d Update node %s : %s -> %s' % (self.nodechanged,
                                                             oid, keysrc,
                                                             keydst)
            else:
                self.mark_node_got( oid, keysrc )
        return chgnode

    def update_way(self, keysrc, keydst, oid):
        """Change a key in a way by another"""
        chgway = None
        osmobject = self.getway( oid )
        if osmobject != None:
            chgway = keyreplace(keysrc, keydst, osmobject)
            if chgway != osmobject:
                self.mark_way_fixed( oid, keysrc )
                self.waychanged += 1
                if self.api != None:
                    self.api.WayUpdate(chgway)
                    self.addchange('way', keysrc, keydst)
                    print '%4d Update way %s : %s -> %s' % (self.waychanged, 
                                                            oid, keysrc, keydst)
            else:
                self.mark_way_got( oid, keysrc )
        return chgway



    def addchange(self, obj, keysrc, keydst):
        
        if keysrc in self._changes:
            self._changes[keysrc][obj] = 1 + self._changes[keysrc][obj]
        else:
            self._changes[keysrc] = {}
            self._changes[keysrc]['node'] = 0
            self._changes[keysrc]['way'] = 0
            self._changes[keysrc]['new'] = keydst
            self._changes[keysrc][obj] = 1 + self._changes[keysrc][obj]

        return self._changes[keysrc]['node'] + self._changes[keysrc]['way']

    def printoutput(self, changeset = None):
        out = "== http://www.openstreetmap.org/browse/changeset/%s ==\n\n" % changeset
        node = 0
        way = 0
        for key in self._changes:
            out = out + " * Nodes : %3d, Ways : %3d, key replace %s -> '''%s'''\n" % (self._changes[key]['node'],
                                                                                      self._changes[key]['way'],
                                                                                      key, self._changes[key]['new'])
            way = way + self._changes[key]['way']
            node = node + self._changes[key]['node']
            
        out = out + " - %3d nodes changed\n" % node
        out = out + " - %3d ways changed\n" % way

        self.output = out
