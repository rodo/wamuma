# -*- coding: utf-8 -*-
#
# Copyright (c) 2011 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""Functions to read and write compressed files"""

import os
import syslog
import zipfile
import bz2

def unbzip2(infilename, outfilename):
    """Uncompress a bz2 compressed file"""
    if os.path.exists(infilename):
        osmbz2 =  bz2.BZ2File(infilename, 'r')
        datas = None
        try:
            datas = osmbz2.read()
        except IOError:
            syslog.syslog("Error unbzip2 %s to %s" % (infilename, outfilename))

        if datas:
            f_out = open(outfilename, 'w')
            f_out.write(datas)
            f_out.close()
        else:
            logmsg = """No data read in %s, remove the file""" % infilename
            syslog.syslog(logmsg)
            os.unlink(infilename)
    else:
        syslog.syslog("""File %s does not exists""" % infilename)

def zipnewfile(zipfilename, filename):
    """Create a new zip file and add it onefile"""
    return zip_file(zipfilename, 'w', filename)

def zipaddfile(zipfilename, filename):
    """And a file to an existent zip file"""
    return zip_file(zipfilename, 'a', filename)

def zip_file(zipfilename, mode, filename):
    """Compress a file"""
    syslog.syslog("Compress %s in %s" % (filename, zipfilename))
    if os.path.exists(filename):
        f_zip = zipfile.ZipFile(zipfilename, mode, zipfile.ZIP_DEFLATED)
        f_zip.write(filename, os.path.basename(filename))
        res = True
    else:
        syslog.syslog("Error in zipFile : file %s does not exists" % filename)
        res = False

    return res
