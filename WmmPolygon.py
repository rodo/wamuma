# -*- coding: utf-8 -*-
#
# Copyright (c) 2011 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""Polygon file manipulation"""

import re
import threading
import syslog

class WmmPolygon(threading.Thread):
    """Options in config file"""

    def __init__(self, country, polyfile, bboxfile):
        """Initialisation"""
        threading.Thread.__init__(self)

        self.boundingbox = None
        reg = r"(-?\d+\.?\d+E?\+?\d+)[\s|\t]+(-?\d+\.?\d+E?\+?\d+)"
        self.relonlat = re.compile(reg, re.DOTALL)
        self.lowerleft = (180, 180)
        self.upperright = (-180, -180)
        self.country = country
        self.polyfile = polyfile
        self.bboxfile = bboxfile


    def run(self):
        """Do the job"""
        self.readfile(self.polyfile)
        self.wbboxfile(self.bboxfile)

    def match(self, value):
        """Check if line match the regexp"""
        return self.relonlat.search(value)

    def readfile(self, filename):
        """Read the polygon file"""
        try:
            with open(filename, 'r') as poly:
                for line in poly.readlines():
                    vals = self.match(line)
                    if vals != None:
                        lon = vals.group(1)
                        lat = vals.group(2)
                        self.evalbb( (float(lon), float(lat)) )
        except IOError, error:
            syslog.syslog("%s : %s (%s)" 
                          % (self.country, filename, error[1]))

    def wbboxfile(self, filename):
        """Write the bbox file"""
        try:
            with open(filename, 'w') as bbox:
                bbox.write("%s,%s,%s,%s\n" % (self.lowerleft[0],
                                              self.lowerleft[1],
                                              self.upperright[0],
                                              self.upperright[1]))
        except IOError, error:
            return error


    def evalbb(self, lonlat):
        """Eval the bbox"""
        self.evallowerleft( (lonlat[0], lonlat[1]) )
        self.evalupperright( (lonlat[0], lonlat[1]) )

    def evallowerleft(self, lonlat):
        """Define new corner lower left"""
        lon = min( self.lowerleft[0], lonlat[0] )
        lat = min( self.lowerleft[1], lonlat[1] )
        self.lowerleft = ( lon, lat)

    def evalupperright(self, lonlat):
        """Define new point upper right"""
        lon = max( self.upperright[0], lonlat[0] )
        lat = max( self.upperright[1], lonlat[1] )
        self.upperright = ( lon, lat)


if __name__ == "__main__":
    
    CONF = WmmPolygon("benin.poly")
