# -*- coding: utf-8 -*-
#
# Copyright (c) 2011 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""Generate some informational files"""
import os
import WmmPsql


def countries_by_continent(dirname, dbname):
    '''Read continent'''
    rows = WmmPsql.readcountries_by_continent(dbname)
    indexwrite(rows, os.path.join(dirname, 'countries_by_continent.txt'))


def countries_by_name(dirname, dbname):
    '''Read continent'''
    rows = WmmPsql.readcountries_by_name(dbname)
    indexwrite(rows, os.path.join(dirname, 'countries_by_name.txt'))


def countries_by_priority(dirname, dbname):
    '''Read continent'''
    rows = WmmPsql.readcountries_by_priority(dbname)
    indexwrite(rows, os.path.join(dirname, 'countries_by_priority.txt'))


def indexwrite(datas, filename):
    """Write indexes files in webserver root"""
    with open(filename, 'w') as fih:

        fih.write("%-15s %-32s %s %s\n" % ('Continent',
                                           'Country',
                                           'Prio',
                                           'Directory'))
        fih.write("-" * 80)
        fih.write("\n")

        for row in datas:
            fih.write("%-15s %-32s %4d %s\n" % row)

