#!/bin/sh

# Install all necessary package under debian/ubuntu

# This list is built for Debian/Squeeze

aptitude install \
    openjdk-6-jre \
    postgresql-8.4 \
    postgresql-8.4-postgis \
    postgresql-contrib \
    postgis \
    python \
    python-psycopg2 \
    python-sqlite \
    subversion \
    wget \
    libbz2-dev \
    automake \
    autoconf \
    libtool g++ make \
    libxml2-dev \
    libgeos-dev \
    libpq-dev \
    libbz2-dev \
    proj \
    apache2-mpm-prefork \
    libapache2-mod-php5 \
    memcached \
    php5-memcache \
    php5-pgsql \
    php5-sqlite

