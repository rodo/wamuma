# -*- coding: utf-8 -*-
#
# Copyright (c) 2011 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""Download class"""

import os, syslog
import threading

import LibExosm
import LibHDM
import LibCompress

class WmmDownloadContinent(threading.Thread):
    """Download continent data's files"""

    def __init__(self, url, dirname, name):
        '''Initialization'''
        threading.Thread.__init__(self)

        self.url = url
        self.name = name
        self.dirname = dirname


    def run(self):
        """Main process"""
        LibExosm.checkdir(self.dirname)
        filename = os.path.join(self.dirname, "%s.osm.bz2" % self.name)
        logfile = "%s.log" % (filename)
        if ( LibHDM.fresherfile(self.url, filename) ):
            LibHDM.wgetfile(self.url, filename, logfile)

class WmmDownloadCountry(threading.Thread):
    """Download country data's files"""

    def __init__(self, urlbase, country, dirname, fullurl=None):
        '''Initialization'''
        threading.Thread.__init__(self)
        self.urlbase = urlbase
        self.country = country
        self.dirname = dirname
        self.fullurl = fullurl


    def run(self):
        syslog.syslog("%s : download files" % self.country)
        LibExosm.checkdir(self.dirname)
        LibExosm.checkdir("%s/logs" % self.dirname)
        self.downloaddatas()
        self.uncompressdatas()
        #self.downloadpoly()


    def downloadpoly(self):
        """Download polygon file"""
        syslog.syslog("%s : download polygon files" % self.country)
        if self.fullurl == None:
            url = "%s/%s.poly" % (self.urlbase, self.country)
        else:
            url = self.fullurl[:-7] + "poly" # horrible hack for just a moment
        filename = "%s/%s.poly" % (self.dirname, self.country)
        logfile = "%s/logs/download.poly.log" % (self.dirname)
        LibHDM.refreshfile(url, filename, logfile)

    def downloaddatas(self):
        """Download osm data file"""
        if self.fullurl == None:
            url = "%s/%s.osm.bz2" % (self.urlbase, self.country)
        else:
            url = self.fullurl
        filename = "%s/%s.osm.bz2" % (self.dirname, self.country)
        logfile = "%s/logs/download.bz2.log" % (self.dirname)
        LibHDM.refreshfile(url, filename, logfile)


    def uncompressdatas(self):
        """Uncompress the bz2 file in the same directory"""
        osmfilename = "%s/%s.osm" % (self.dirname, self.country)
        composmfilename = "%s/%s.osm.bz2" % (self.dirname, self.country)
        if LibHDM.newgeneration(composmfilename, osmfilename):
            LibCompress.unbzip2(composmfilename, osmfilename)

