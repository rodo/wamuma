# -*- coding: utf-8 -*-
#
# Copyright (c) 2011 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# pylint: disable=R0902
"""Country object"""

from WmmDatabase import WmmDatabase

class Country():
    """The country"""
    _id = 0
    logid = 0
    odb = None
    country_id = 0
    _verbose = False

    def __init__(self, cid, maindb, verbose=False):
        """Initialisation"""
        country_id = cid
        self._verbose = verbose
        self._id = cid
        self.maindb = maindb
        self.dir = None
        self.url = None
        self.dirname = None
        self.dbname = None
        self.dbstatname = None
        self.continent = None
        self.langs = []

        self.readdb(country_id)

    def readdb(self, cid):
        """Read the dedicated dbname"""
        parms = self.maindb.readcountry(cid)
        self.dirname = parms[1]
        continentid = parms[2]
        self.dbname = self.dirname
        self.dbstatname = self.dirname + "_stat"
        parms = self.maindb.readcontinent(continentid)
        self.continent = parms[1]
        self.specialurl(cid)
        self.readlangs()

    def specialurl(self, cid):
        """Define the special url"""
        url = self.maindb.readcountryurl(cid)
        if url != None:
            if len(url) > 0:
                self.url = url


    def readlangs(self):
        """Read the langs defined from database"""
        self.langs = self.maindb.readcountrylangs(self._id)

    def jsmapcenter(self):
        """Read the langs defined from database"""
        pos = self.maindb.readcountrymap(self._id)
        return "%s,%s,%s" % (pos[0], pos[1], pos[2])

    def mapcenter(self):
        """Read the map center from db"""
        qry = "SELECT st_x(c),st_y(c) FROM (SELECT st_centroid(st_extent(geom))"
        qry = qry + " AS c FROM nodes) as nodes"
        odb = WmmDatabase(self.dbstatname, None, None)
        pos = odb.fetchquery(qry)[0]
        return "12,%s,%s" % (pos[0], pos[1] )

    def addurl(self, url):
        """Define the url"""
        if url != None:
            if len(url) > 0:
                self.url = url


    def initdb(self):
        """Initialize the related database"""
        self.initdbcore()
        self.initdbstat()
        self.updatedbstat()

    def initdbcore(self):
        """Initialize the core database for shape generation"""
        if not self.maindb.exists(self.dbname):
            self.maindb.create(self.dbname)
            odb = WmmDatabase(self.dbname, self.maindb.files, None)
            odb.initnew()

    def initdbstat(self):
        """Initialize the stat database for crowd fixing"""
        dbname = self.dbname + "_stat"
        if not self.maindb.exists(dbname):
            if self._verbose:
                print "%s does not exists create it" % dbname
            self.maindb.create(dbname)
            files = self.maindb.files
            odb = WmmDatabase(dbname, files, self.maindb.filesopt)
            odb.initnew()
            odb.initstat()

    def updatedbstat(self):
        """Initialize the stat database for crowd fixing"""
        dbname = self.dbname + "_stat"
        if self.maindb.exists(dbname):
            odb = WmmDatabase(dbname, None, None)
            odb.updatestat()



if __name__ == "__main__":
    
    CONF = Country(1)
    print CONF.url
    print CONF.continent
