
pylint=/usr/bin/pylint 
pylintrc=--rcfile=pylintrc -f html
pycheck=/usr/bin/pychecker

pyfiles=$(shell find . -maxdepth 1 -type f -name "*.py" | grep -v '__init__.py' | grep -v OsmApi.py | sed -e 's/py/html/')

MAKE=make

dirs = extractor tagfix

all: $(pyfiles)
	cd tests ; $(MAKE)

test:
	cd tests ; $(MAKE) test
	cd extractor/ ; $(MAKE) test

clean: $(dirs)
	rm -f *.pyc *.html
	cd tests ; $(MAKE) clean


$(dirs):
	cd $@ ; make clean

%.html: %.py
	$(pylint) $(pylintrc) $< > $@

