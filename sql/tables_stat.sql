--                                                                                                                    
SET client_min_messages = error;
--
--
-- Tables schema for town
--
--
DROP TABLE IF EXISTS stat_nodes CASCADE;
DROP TABLE IF EXISTS stat_ways CASCADE;
DROP TABLE IF EXISTS stat CASCADE;
--
--
CREATE TABLE stat (
       id serial,
       node_id integer,
       lon float,
       lat float,
       k varchar(255),
       v varchar(255),
       fixed boolean DEFAULT False,
       got boolean DEFAULT False
);
--
CREATE TABLE stat_nodes () INHERITS (stat);
CREATE INDEX stat_nodes_k ON stat_nodes (k);
--
CREATE TABLE stat_ways () INHERITS (stat);
CREATE INDEX stat_ways_k ON stat_ways (k);
--
--
DROP TABLE IF EXISTS stat_tags CASCADE;
CREATE TABLE stat_tags (
       k varchar(255),
       iteration integer
);
-- 
-- This table will be removed when code 
-- won't use it
--
DROP TABLE IF EXISTS official_tags CASCADE;
CREATE TABLE official_tags (
       official_tags_id serial PRIMARY KEY,
       k varchar(255),
       nback integer
);
CREATE UNIQUE INDEX official_tags_ku ON official_tags (k);
--
--
--
DROP TABLE IF EXISTS official_keys CASCADE;
CREATE TABLE official_keys (
       official_keys_id serial PRIMARY KEY,
       k varchar(255),
       nback integer
);
CREATE UNIQUE INDEX official_keys_ku ON official_keys (k);
--
--
----
DROP VIEW IF EXISTS badtags;

CREATE VIEW badtags AS (
       SELECT s.k, s.iteration FROM stat_tags AS s
       WHERE s.k NOT IN (SELECT k FROM official_tags)
       ORDER BY s.iteration ASC, s.k ASC
);

DROP TABLE IF EXISTS official_key_value CASCADE;
CREATE TABLE official_key_value (
       k varchar(255),
       v varchar(255)
);
CREATE UNIQUE INDEX official_key_value_ku ON official_key_value (k, v);

--
--
--
DROP TABLE IF EXISTS tag_substitution CASCADE;
CREATE TABLE tag_substitution (
       src varchar(255),
       dst varchar(255)
);
CREATE UNIQUE INDEX tag_substitution_ku ON tag_substitution (src, dst);
COMMENT ON TABLE tag_substitution IS 'Use by tagfix robot to change old tags by new ones';
--
--
--
-- Shapes generation based on SQL queries definition
--
--
DROP TABLE IF EXISTS query_shapes CASCADE;
CREATE TABLE query_shapes (
       label varchar(25),
       tgeom varchar(10),
       stylefile varchar(255),
       query text
);
CREATE UNIQUE INDEX query_shapes_label_ux ON query_shapes (label);
--
--
--
DROP TABLE IF EXISTS query_shapes_fields CASCADE;
CREATE TABLE query_shapes_fields (
       query varchar(25),
       field varchar(10)
);
CREATE UNIQUE INDEX query_shapes_fields_ux ON query_shapes_fields (query, field);
ALTER TABLE query_shapes_fields ADD CONSTRAINT fk_query_shapes_label FOREIGN KEY (query) REFERENCES query_shapes(label) ;