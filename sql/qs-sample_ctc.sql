--
-- Shapes definitions based on sql queries
--

INSERT INTO query_shapes (label, tgeom, stylefile, query) VALUES ('sample_ctc', 'point', 'hdm_5.0.style',
'hum_use IN (''cholera_treatment_center'',''cholera_treatment_unit'',''oral_rehydratation_centre'') ' );

INSERT INTO query_shapes_fields (query, field) VALUES ('sample_ctc','osm_id');
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_ctc','x');
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_ctc','y');
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_ctc','way');

INSERT INTO query_shapes_fields (query, field) VALUES ('sample_ctc','name'); 
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_ctc','name_en'); 
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_ctc','name_fr'); 
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_ctc','name_kr'); 

INSERT INTO query_shapes_fields (query, field) VALUES ('sample_ctc','addr_city'); 
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_ctc','addr_full'); 

INSERT INTO query_shapes_fields (query, field) VALUES ('sample_ctc','access'); 
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_ctc','amenity'); 

INSERT INTO query_shapes_fields (query, field) VALUES ('sample_ctc','bhrl'); 
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_ctc','bhrl_open');
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_ctc','bhrl_clsd');


INSERT INTO query_shapes_fields (query, field) VALUES ('sample_ctc','blad');
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_ctc','blad_open');
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_ctc','blad_clsd');

INSERT INTO query_shapes_fields (query, field) VALUES ('sample_ctc','cap_area'); 

INSERT INTO query_shapes_fields (query, field) VALUES ('sample_ctc','cap_ind_b1'); 
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_ctc','cap_ind_b2'); 
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_ctc','cap_ind_b3'); 
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_ctc','cap_are_b1'); 
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_ctc','cap_are_b2'); 
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_ctc','cap_are_b3'); 

INSERT INTO query_shapes_fields (query, field) VALUES ('sample_ctc','court_area');

INSERT INTO query_shapes_fields (query, field) VALUES ('sample_ctc','fax');

INSERT INTO query_shapes_fields (query, field) VALUES ('sample_ctc','hum_use');

INSERT INTO query_shapes_fields (query, field) VALUES ('sample_ctc','geom_date');
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_ctc','geom_name');
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_ctc','well_clsd');
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_ctc','well_open');
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_ctc','well_bad');

INSERT INTO query_shapes_fields (query, field) VALUES ('sample_ctc','id_db_ctc');
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_ctc','id_uid'); 
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_ctc','id_uuid');
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_ctc','id_ssid');

INSERT INTO query_shapes_fields (query, field) VALUES ('sample_ctc','hws_gd'); 
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_ctc','hws_med');
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_ctc','hws_bad');

INSERT INTO query_shapes_fields (query, field) VALUES ('sample_ctc','op_status'); 

INSERT INTO query_shapes_fields (query, field) VALUES ('sample_ctc','rehab_done');
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_ctc','rehab_work');
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_ctc','rehab_need');

INSERT INTO query_shapes_fields (query, field) VALUES ('sample_ctc','shower'); 
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_ctc','shower_gd'); 
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_ctc','shower_med');
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_ctc','shower_bad');

INSERT INTO query_shapes_fields (query, field) VALUES ('sample_ctc','tank'); 
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_ctc','tank_open');
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_ctc','tank_clsd');

INSERT INTO query_shapes_fields (query, field) VALUES ('sample_ctc','tap'); 
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_ctc','tap_status'); 

INSERT INTO query_shapes_fields (query, field) VALUES ('sample_ctc','wtr_strge'); 