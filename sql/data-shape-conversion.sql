--
-- shp_field_conv
SET client_min_messages = error;
--
-- Fields name conversion
--
DROP TABLE IF EXISTS shp_field_conv;
CREATE TABLE shp_field_conv (
       osm varchar(50) NOT NULL,
       shp varchar(10) NOT NULL
);

CREATE UNIQUE INDEX shp_field_conv_osm_ux ON shp_field_conv (osm);
CREATE UNIQUE INDEX shp_field_conv_shp_ux ON shp_field_conv (shp);


INSERT INTO shp_field_conv (shp, osm) VALUES ('addr_city',  'addr:city');
INSERT INTO shp_field_conv (shp, osm) VALUES ('addr_cntry', 'addr:country');
INSERT INTO shp_field_conv (shp, osm) VALUES ('addr_cnc',   'addr:country_national_code');
INSERT INTO shp_field_conv (shp, osm) VALUES ('addr_full',  'addr:full');
INSERT INTO shp_field_conv (shp, osm) VALUES ('addr_house', 'addr:housenumber');
INSERT INTO shp_field_conv (shp, osm) VALUES ('addr_pstcd', 'addr:postal_code');
INSERT INTO shp_field_conv (shp, osm) VALUES ('addr_stree', 'addr:street');
INSERT INTO shp_field_conv (shp, osm) VALUES ('addr_ward',  'addr:ward');

INSERT INTO shp_field_conv (shp, osm) VALUES ('atr_src_dt', 'attribute_source_date');
INSERT INTO shp_field_conv (shp, osm) VALUES ('atr_src_nm', 'attribute_source_name');
INSERT INTO shp_field_conv (shp, osm) VALUES ('atr_src_tp', 'attribute_source_type');

INSERT INTO shp_field_conv (shp, osm) VALUES ('admin_1_nm', 'boundary_admin_level1_name');
INSERT INTO shp_field_conv (shp, osm) VALUES ('admin_2_nm', 'boundary_admin_level2_name');
INSERT INTO shp_field_conv (shp, osm) VALUES ('admin_3_nm', 'boundary_admin_level3_name');
INSERT INTO shp_field_conv (shp, osm) VALUES ('admin_4_nm', 'boundary_admin_level4_name');

INSERT INTO shp_field_conv (shp, osm) VALUES ('bldg_descr', 'building_desc');
INSERT INTO shp_field_conv (shp, osm) VALUES ('bldg_stdsc', 'building_status_desc');

INSERT INTO shp_field_conv (shp, osm) VALUES ('bldg_good',  'building_good');
INSERT INTO shp_field_conv (shp, osm) VALUES ('bldg_mediu', 'building_medium');
INSERT INTO shp_field_conv (shp, osm) VALUES ('bldg_bad',   'building_bad');

INSERT INTO shp_field_conv (shp, osm) VALUES ('building_u', 'building_use');
INSERT INTO shp_field_conv (shp, osm) VALUES ('buildin_01', 'building_stucture');

INSERT INTO shp_field_conv (shp, osm) VALUES ('blad',       'bladder');
INSERT INTO shp_field_conv (shp, osm) VALUES ('blad_open',  'bladder_open');
INSERT INTO shp_field_conv (shp, osm) VALUES ('blad_clsd',  'bladder_closed');

INSERT INTO shp_field_conv (shp, osm) VALUES ('bhrl',       'borehole');
INSERT INTO shp_field_conv (shp, osm) VALUES ('bhrl_open',  'borehole_open');
INSERT INTO shp_field_conv (shp, osm) VALUES ('bhrl_clsd',  'borehole_closed');

INSERT INTO shp_field_conv (shp, osm) VALUES ('cap_ind',    'capacity_individuals');
INSERT INTO shp_field_conv (shp, osm) VALUES ('cap_area',   'capacity_area');
INSERT INTO shp_field_conv (shp, osm) VALUES ('cap_ind_b1', 'capacity_individuals_building_level_1');
INSERT INTO shp_field_conv (shp, osm) VALUES ('cap_ind_b2', 'capacity_individuals_building_level_2');
INSERT INTO shp_field_conv (shp, osm) VALUES ('cap_ind_b3', 'capacity_individuals_building_level_3');
INSERT INTO shp_field_conv (shp, osm) VALUES ('cap_are_b1',  'capacity_area_building_level_1');
INSERT INTO shp_field_conv (shp, osm) VALUES ('cap_are_b2',  'capacity_area_building_level_2');
INSERT INTO shp_field_conv (shp, osm) VALUES ('cap_are_b3',  'capacity_area_building_level_3');

INSERT INTO shp_field_conv (shp, osm) VALUES ('cntct_pers', 'contact_person');

INSERT INTO shp_field_conv (shp, osm) VALUES ('dmg_ass_dt', 'damage_assessment_date');
INSERT INTO shp_field_conv (shp, osm) VALUES ('dmg_status', 'damage_status');
INSERT INTO shp_field_conv (shp, osm) VALUES ('desc',       'description');

INSERT INTO shp_field_conv (shp, osm) VALUES ('rthquk_dmg', 'earthquake_damage');

INSERT INTO shp_field_conv (shp, osm) VALUES ('floodable_', 'floodable_bldg');

INSERT INTO shp_field_conv (shp, osm) VALUES ('geom_date',  'geometry_source_date');
INSERT INTO shp_field_conv (shp, osm) VALUES ('geom_name',  'geometry_source_name');
INSERT INTO shp_field_conv (shp, osm) VALUES ('geom_type',  'geometry_source_type');

INSERT INTO shp_field_conv (shp, osm) VALUES ('has_courty', 'has_courtyard');
INSERT INTO shp_field_conv (shp, osm) VALUES ('has_kitche', 'has_kitchen');
INSERT INTO shp_field_conv (shp, osm) VALUES ('has_toilet', 'has_toilets');

INSERT INTO shp_field_conv (shp, osm) VALUES ('hws_gd',     'handwashing_station_good');
INSERT INTO shp_field_conv (shp, osm) VALUES ('hws_med',    'handwashing_station_medium');
INSERT INTO shp_field_conv (shp, osm) VALUES ('hws_bad',    'handwashing_station_bad');

INSERT INTO shp_field_conv (shp, osm) VALUES ('hum_use',    'humanitarian_use');

INSERT INTO shp_field_conv (shp, osm) VALUES ('id_db_shel', 'id:db_shelter');
INSERT INTO shp_field_conv (shp, osm) VALUES ('id_db_ctc',  'id:db_ctc');
INSERT INTO shp_field_conv (shp, osm) VALUES ('id_ssid',    'id:ssid');
INSERT INTO shp_field_conv (shp, osm) VALUES ('id_uid',     'id:uid');
INSERT INTO shp_field_conv (shp, osm) VALUES ('id_uuid',    'id:uuid');

INSERT INTO shp_field_conv (shp, osm) VALUES ('name_en',    'name:en');
INSERT INTO shp_field_conv (shp, osm) VALUES ('name_fr',    'name:fr');
INSERT INTO shp_field_conv (shp, osm) VALUES ('name_kr',    'name:kr');

INSERT INTO shp_field_conv (shp, osm) VALUES ('op_status',  'operational_status');
INSERT INTO shp_field_conv (shp, osm) VALUES ('op_statusq', 'operational_status_quality');

INSERT INTO shp_field_conv (shp, osm) VALUES ('opr_damage', 'operator_damage');
INSERT INTO shp_field_conv (shp, osm) VALUES ('opr_id',     'operator_id');
INSERT INTO shp_field_conv (shp, osm) VALUES ('oper_type',  'operator_type');


INSERT INTO shp_field_conv (shp, osm) VALUES ('plc_area',   'place:area');
INSERT INTO shp_field_conv (shp, osm) VALUES ('plc_a_alt',  'place:area_alternate');
INSERT INTO shp_field_conv (shp, osm) VALUES ('plc_nearmk', 'place:nearestlandmark');


INSERT INTO shp_field_conv (shp, osm) VALUES ('power_src',  'power_source');

INSERT INTO shp_field_conv (shp, osm) VALUES ('rehab_done', 'rehabilitation_done');
INSERT INTO shp_field_conv (shp, osm) VALUES ('rehab_work', 'rehabilitation_work');
INSERT INTO shp_field_conv (shp, osm) VALUES ('rehab_need', 'rehabilitation_need');

INSERT INTO shp_field_conv (shp, osm) VALUES ('shower_gd',  'shower_good');
INSERT INTO shp_field_conv (shp, osm) VALUES ('shower_med', 'shower_medium');

INSERT INTO shp_field_conv (shp, osm) VALUES ('tank_clsd',  'tank_closed');

INSERT INTO shp_field_conv (shp, osm) VALUES ('toilet_gd',  'toilet_good');
INSERT INTO shp_field_conv (shp, osm) VALUES ('toilet_med', 'toilet_medium');

INSERT INTO shp_field_conv (shp, osm) VALUES ('well_clsd',  'well_closed');

INSERT INTO shp_field_conv (shp, osm) VALUES ('whs_unit',   'warehouse_unit');
INSERT INTO shp_field_conv (shp, osm) VALUES ('whs_area',   'warehouse_unit_area');

INSERT INTO shp_field_conv (shp, osm) VALUES ('wtr_strge',  'water_storage');