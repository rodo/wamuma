--
-- Shapes definitions based on sql queries
--

INSERT INTO query_shapes (label, tgeom, stylefile, query) VALUES ('sample_shelter', 'point', 'hdm_5.0.style',
'hum_use = (''disaster_shelter'') ' );

INSERT INTO query_shapes_fields (query, field) VALUES ('sample_shelter','osm_id');
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_shelter','x');
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_shelter','y');
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_shelter','way');

INSERT INTO query_shapes_fields (query, field) VALUES ('sample_shelter','name'); 
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_shelter','name_en'); 
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_shelter','name_fr'); 
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_shelter','name_kr'); 

INSERT INTO query_shapes_fields (query, field) VALUES ('sample_shelter','addr_city'); 
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_shelter','addr_full'); 

INSERT INTO query_shapes_fields (query, field) VALUES ('sample_shelter','access'); 
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_shelter','amenity'); 

INSERT INTO query_shapes_fields (query, field) VALUES ('sample_shelter','bhrl'); 
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_shelter','bhrl_open');
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_shelter','bhrl_clsd');


INSERT INTO query_shapes_fields (query, field) VALUES ('sample_shelter','blad');
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_shelter','blad_open');
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_shelter','blad_clsd');

INSERT INTO query_shapes_fields (query, field) VALUES ('sample_shelter','cap_area'); 

INSERT INTO query_shapes_fields (query, field) VALUES ('sample_shelter','cap_ind_b1'); 
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_shelter','cap_ind_b2'); 
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_shelter','cap_ind_b3'); 
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_shelter','cap_are_b1'); 
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_shelter','cap_are_b2'); 
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_shelter','cap_are_b3'); 

INSERT INTO query_shapes_fields (query, field) VALUES ('sample_shelter','court_area');

INSERT INTO query_shapes_fields (query, field) VALUES ('sample_shelter','fax');

INSERT INTO query_shapes_fields (query, field) VALUES ('sample_shelter','hum_use');

INSERT INTO query_shapes_fields (query, field) VALUES ('sample_shelter','geom_date');
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_shelter','geom_name');
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_shelter','well_clsd');
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_shelter','well_open');
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_shelter','well_bad');

INSERT INTO query_shapes_fields (query, field) VALUES ('sample_shelter','id_db_shel');
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_shelter','id_uid'); 
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_shelter','id_uuid');
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_shelter','id_ssid');

INSERT INTO query_shapes_fields (query, field) VALUES ('sample_shelter','hws_gd'); 
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_shelter','hws_med');
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_shelter','hws_bad');

INSERT INTO query_shapes_fields (query, field) VALUES ('sample_shelter','op_status'); 

INSERT INTO query_shapes_fields (query, field) VALUES ('sample_shelter','rehab_done');
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_shelter','rehab_work');
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_shelter','rehab_need');

INSERT INTO query_shapes_fields (query, field) VALUES ('sample_shelter','shower'); 
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_shelter','shower_gd'); 
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_shelter','shower_med');
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_shelter','shower_bad');

INSERT INTO query_shapes_fields (query, field) VALUES ('sample_shelter','tank'); 
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_shelter','tank_open');
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_shelter','tank_clsd');

INSERT INTO query_shapes_fields (query, field) VALUES ('sample_shelter','tap'); 
INSERT INTO query_shapes_fields (query, field) VALUES ('sample_shelter','tap_status'); 

INSERT INTO query_shapes_fields (query, field) VALUES ('sample_shelter','wtr_strge'); 