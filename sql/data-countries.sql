--                                                                                                                    
SET client_min_messages = error;
--
--

--
--
--
INSERT INTO continent (name,dir) VALUES ('Asia', 'asia');
--
--
INSERT INTO continent (name,dir) VALUES ('Central America', 'central_america');
--
INSERT INTO country (name, dir, priority, continent_id)  VALUES ('Haiti','haiti', 10, (SELECT currval('continent_id_seq') ) );
INSERT INTO country_lang (lang_code, country_id)  VALUES ('en', (SELECT currval('country_id_seq') ) );
INSERT INTO country_lang (lang_code, country_id)  VALUES ('fr', (SELECT currval('country_id_seq') ) );
--
--
INSERT INTO continent (name,dir) VALUES ('Europe', 'europe');
--
INSERT INTO country (name, dir, priority, continent_id)  VALUES ('France','france', 0, (SELECT currval('continent_id_seq') ) );
INSERT INTO country_lang (lang_code, country_id)  VALUES ('fr', (SELECT currval('country_id_seq') ) );
--
--
INSERT INTO continent (name,dir) VALUES ('North America', 'north_america');
--
--
--
INSERT INTO continent (name,dir) VALUES ('South America', 'south_america');
--
--

INSERT INTO country (name,dir,continent_id) VALUES ('Uruguay', 'uruguay', (SELECT currval('continent_id_seq') ) );
INSERT INTO country (name,dir,continent_id) VALUES ('Venezuela', 'venezuela', (SELECT currval('continent_id_seq') ) );


INSERT INTO continent (name,dir) VALUES ('Africa', 'africa');
--
--
INSERT INTO country (name,dir,continent_id) VALUES ('Algeria', 'algeria', (SELECT currval('continent_id_seq') ) );
INSERT INTO country (name,dir,continent_id) VALUES ('Angola', 'angola', (SELECT currval('continent_id_seq') ) );
INSERT INTO country (name,dir,continent_id) VALUES ('Benin', 'benin', (SELECT currval('continent_id_seq') ) );
INSERT INTO country (name,dir,continent_id) VALUES ('Botswana', 'botswana', (SELECT currval('continent_id_seq') ) );
INSERT INTO country (name,dir,continent_id) VALUES ('Chad', 'chad', (SELECT currval('continent_id_seq') ) );


--
INSERT INTO country (name, dir, priority, continent_id)  VALUES ('Libya','libya', 10, (SELECT currval('continent_id_seq') ) );
INSERT INTO country_lang (lang_code, country_id)  VALUES ('en', (SELECT currval('country_id_seq') ) );
INSERT INTO country_lang (lang_code, country_id)  VALUES ('fr', (SELECT currval('country_id_seq') ) );




INSERT INTO country (name,dir,continent_id) VALUES ('Burkina Faso', 'burkina_faso', (SELECT currval('continent_id_seq') ) );
INSERT INTO country (name,dir,continent_id) VALUES ('Burundi', 'burundi', (SELECT currval('continent_id_seq') ) );
INSERT INTO country (name,dir,continent_id) VALUES ('Cameroon', 'cameroon', (SELECT currval('continent_id_seq') ) );
INSERT INTO country (name,dir,continent_id) VALUES ('Cape Verde', 'cape_verde', (SELECT currval('continent_id_seq') ) );
INSERT INTO country (name,dir,continent_id) VALUES ('Central African Republic', 'central_africa_republic', (SELECT currval('continent_id_seq') ) );

INSERT INTO country (name,dir,continent_id) VALUES ('Comoros', 'comoros', (SELECT currval('continent_id_seq') ) );
INSERT INTO country (name,dir,continent_id) VALUES ('Democratic Republic of the Congo', 'congo', (SELECT currval('continent_id_seq') ) );
INSERT INTO country (name,dir,continent_id) VALUES ('Djibouti', 'djibouti', (SELECT currval('continent_id_seq') ) );
INSERT INTO country (name,dir,continent_id) VALUES ('Egypt', 'egypt', (SELECT currval('continent_id_seq') ) );
INSERT INTO country (name,dir,continent_id) VALUES ('Equatorial Guinea', 'equatorial_guinea', (SELECT currval('continent_id_seq') ) );
INSERT INTO country (name,dir,continent_id) VALUES ('Eritrea', 'eritrea', (SELECT currval('continent_id_seq') ) );
INSERT INTO country (name,dir,continent_id) VALUES ('Ethiopia', 'ethiopia', (SELECT currval('continent_id_seq') ) );
INSERT INTO country (name,dir,continent_id) VALUES ('Gabon', 'gabon', (SELECT currval('continent_id_seq') ) );
INSERT INTO country (name,dir,continent_id) VALUES ('Gambia', 'gambia', (SELECT currval('continent_id_seq') ) );
INSERT INTO country (name,dir,continent_id) VALUES ('Ghana', 'ghana', (SELECT currval('continent_id_seq') ) );
--INSERT INTO country (name,dir,continent_id) VALUES ('Glorioso Islands', '', (SELECT currval('continent_id_seq') ) );
INSERT INTO country (name,dir,continent_id) VALUES ('Guinea', 'guinea', (SELECT currval('continent_id_seq') ) );
INSERT INTO country (name,dir,continent_id) VALUES ('Guinea-Bissau', 'guinea-bissau', (SELECT currval('continent_id_seq') ) );
--INSERT INTO country (name,dir,continent_id) VALUES ('Juan De Nova Island', '', (SELECT currval('continent_id_seq') ) );
INSERT INTO country (name,dir,continent_id) VALUES ('Kenya', 'kenya', (SELECT currval('continent_id_seq') ) );
INSERT INTO country (name,dir,continent_id) VALUES ('Lesotho', 'lesotho', (SELECT currval('continent_id_seq') ) );
INSERT INTO country (name,dir,continent_id) VALUES ('Liberia', 'liberia', (SELECT currval('continent_id_seq') ) );

INSERT INTO country (name,dir,continent_id) VALUES ('Madagascar', 'madagascar', (SELECT currval('continent_id_seq') ) );
INSERT INTO country (name,dir,continent_id) VALUES ('Malawi', 'malawi', (SELECT currval('continent_id_seq') ) );
INSERT INTO country (name,dir,continent_id) VALUES ('Mali', 'mali', (SELECT currval('continent_id_seq') ) );
INSERT INTO country (name,dir,continent_id) VALUES ('Mauritania', 'mauritania', (SELECT currval('continent_id_seq') ) );
INSERT INTO country (name,dir,continent_id) VALUES ('Mauritius', 'mauritius', (SELECT currval('continent_id_seq') ) );
INSERT INTO country (name,dir,continent_id) VALUES ('Mayotte', 'mayotte', (SELECT currval('continent_id_seq') ) );
INSERT INTO country (name,dir,continent_id) VALUES ('Morocco', 'morocco', (SELECT currval('continent_id_seq') ) );
INSERT INTO country (name,dir,continent_id) VALUES ('Mozambique', 'mozambique', (SELECT currval('continent_id_seq') ) );
INSERT INTO country (name,dir,continent_id) VALUES ('Namibia', 'namibia', (SELECT currval('continent_id_seq') ) );
INSERT INTO country (name,dir,continent_id) VALUES ('Niger', 'niger', (SELECT currval('continent_id_seq') ) );
INSERT INTO country (name,dir,continent_id) VALUES ('Nigeria', 'nigeria', (SELECT currval('continent_id_seq') ) );
--INSERT INTO country (name,dir,continent_id) VALUES ('Republic of Congo', '', (SELECT currval('continent_id_seq') ) );
INSERT INTO country (name,dir,continent_id) VALUES ('Reunion', 'reunion', (SELECT currval('continent_id_seq') ) );
INSERT INTO country (name,dir,continent_id) VALUES ('Rwanda', 'rwanda', (SELECT currval('continent_id_seq') ) );
INSERT INTO country (name,dir,continent_id) VALUES ('Saint Helena', '', (SELECT currval('continent_id_seq') ) );
INSERT INTO country (name,dir,continent_id) VALUES ('Sao Tome and Principe', 'sao_tome_and_principe', (SELECT currval('continent_id_seq') ) );
INSERT INTO country (name,dir,continent_id) VALUES ('Senegal', 'senegal', (SELECT currval('continent_id_seq') ) );
INSERT INTO country (name,dir,continent_id) VALUES ('Seychelles', 'seychelles', (SELECT currval('continent_id_seq') ) );
INSERT INTO country (name,dir,continent_id) VALUES ('Sierra Leone', 'sierra_leone', (SELECT currval('continent_id_seq') ) );
INSERT INTO country (name,dir,continent_id) VALUES ('Somalia', 'somalia', (SELECT currval('continent_id_seq') ) );
INSERT INTO country (name,dir,continent_id) VALUES ('South Africa', 'south-africa', (SELECT currval('continent_id_seq') ) );
INSERT INTO country (name,dir,continent_id) VALUES ('Sudan', 'sudan', (SELECT currval('continent_id_seq') ) );
INSERT INTO country (name,dir,continent_id) VALUES ('Swaziland', 'swaziland', (SELECT currval('continent_id_seq') ) );
INSERT INTO country (name,dir,continent_id) VALUES ('Tanzania', 'tanzania', (SELECT currval('continent_id_seq') ) );
INSERT INTO country (name,dir,continent_id) VALUES ('Togo', 'togo', (SELECT currval('continent_id_seq') ) );
INSERT INTO country (name,dir,continent_id) VALUES ('Tunisia', 'tunisia', (SELECT currval('continent_id_seq') ) );
INSERT INTO country (name,dir,continent_id) VALUES ('Uganda', 'uganda', (SELECT currval('continent_id_seq') ) );
INSERT INTO country (name,dir,continent_id) VALUES ('Zambia', 'zambia', (SELECT currval('continent_id_seq') ) );
INSERT INTO country (name,dir,continent_id) VALUES ('Zimbabwe', 'zimbabwe', (SELECT currval('continent_id_seq') ) );