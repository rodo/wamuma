--                                                                                                                    
SET client_min_messages = error;
--
--
-- Tables schema for town
--
--
DROP TABLE IF EXISTS town CASCADE;
DROP TABLE IF EXISTS departement CASCADE;
DROP TABLE IF EXISTS region CASCADE;
DROP TABLE IF EXISTS country CASCADE;
DROP TABLE IF EXISTS country_url CASCADE;
DROP TABLE IF EXISTS country_lang CASCADE;
DROP TABLE IF EXISTS continent CASCADE;

create table continent (
       id  serial PRIMARY KEY,
       name varchar(50) NOT NULL,
       dir varchar(50) NOT NULL,
       url varchar(255)
);

create table country (
       id  serial PRIMARY KEY,
       name varchar(50) NOT NULL,
       dir  varchar(50) UNIQUE NOT NULL,
       continent_id int NOT NULL,
       priority smallint default 0 NOT NULL,
       genbegin timestamp,
       map_zoom integer default 12,
       map_lon float,
       map_lat float,
       genend timestamp
);
--
--
--
CREATE TABLE country_url (
       country_url_id  serial PRIMARY KEY,
       country_id  integer,
       url varchar(252)
);

ALTER TABLE country_url ADD CONSTRAINT fk_country_id FOREIGN KEY (country_id) REFERENCES country(id) ;
--
--
--
create table country_lang (
       country_lang_id  serial PRIMARY KEY,
       country_id  integer,
       lang_code varchar(4)
);

ALTER TABLE country_lang ADD CONSTRAINT fk_country_id FOREIGN KEY (country_id) REFERENCES country(id) ;
CREATE UNIQUE INDEX country_id_lang_code_unik ON country_lang (country_id, lang_code);

--
--
--
CREATE TABLE region (
       id  serial PRIMARY KEY,
       name varchar(50) NOT NULL,
       dir  varchar(50) UNIQUE NOT NULL,
       country_id int NOT NULL,
       relation int,
       urlsource varchar(255),
       lon_min float,
       lat_min float,
       lon_max float,
       lat_max float,
       lastgen time
);

create table departement (
       id  serial PRIMARY KEY,
       name varchar(50) NOT NULL,
       dir  varchar(50) NOT NULL,
       region_id int NOT NULL,
       relation int,
       lastgen time
);

create table town (
       id  serial,
       departement_id int NOT NULL,
       name varchar(50) NOT NULL,
       dir  varchar(50) NOT NULL,
       relation int UNIQUE NOT NULL,
       lon_min float,
       lat_min float,
       lon_max float,
       lat_max float,
       lastgen time
);

CREATE UNIQUE INDEX town_dir_dept ON town (departement_id, dir);
