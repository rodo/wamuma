--
-- Officials keys used in OSM, this is a non exhaust list
--
INSERT INTO official_keys (k) VALUES ('admin_level');
INSERT INTO official_keys (k) VALUES ('amenity');

INSERT INTO official_keys (k) VALUES ('highway');
INSERT INTO official_keys (k) VALUES ('created_by');
INSERT INTO official_keys (k) VALUES ('barrier');
INSERT INTO official_keys (k) VALUES ('is_in');

INSERT INTO official_keys (k) VALUES ('natural');
INSERT INTO official_keys (k) VALUES ('name');
INSERT INTO official_keys (k) VALUES ('name:en');
INSERT INTO official_keys (k) VALUES ('name:bg');
INSERT INTO official_keys (k) VALUES ('name:ar');
INSERT INTO official_keys (k) VALUES ('name:af');
INSERT INTO official_keys (k) VALUES ('name:bo');
INSERT INTO official_keys (k) VALUES ('name:br');
INSERT INTO official_keys (k) VALUES ('name:de');
INSERT INTO official_keys (k) VALUES ('name:fr');
INSERT INTO official_keys (k) VALUES ('name:ht');
INSERT INTO official_keys (k) VALUES ('name:pt');
INSERT INTO official_keys (k) VALUES ('oneway');
INSERT INTO official_keys (k) VALUES ('place');
INSERT INTO official_keys (k) VALUES ('power');
INSERT INTO official_keys (k) VALUES ('source');

INSERT INTO official_keys (k) VALUES ('wikipedia:de');
--
-- Below official keys used in OIM context
--
INSERT INTO official_keys (k) VALUES ('humanitarian_use');
INSERT INTO official_keys (k) VALUES ('tap');
INSERT INTO official_keys (k) VALUES ('tap_open');
INSERT INTO official_keys (k) VALUES ('tap_closed');
INSERT INTO official_keys (k) VALUES ('contact_person');

--
-- Officials keys used in OSM, this is a non exhaust list
--
INSERT INTO official_tags (k) VALUES ('admin_level');
INSERT INTO official_tags (k) VALUES ('amenity');

INSERT INTO official_tags (k) VALUES ('highway');
INSERT INTO official_tags (k) VALUES ('created_by');
INSERT INTO official_tags (k) VALUES ('barrier');
INSERT INTO official_tags (k) VALUES ('is_in');

INSERT INTO official_tags (k) VALUES ('natural');
INSERT INTO official_tags (k) VALUES ('name');
INSERT INTO official_tags (k) VALUES ('name:en');
INSERT INTO official_tags (k) VALUES ('name:bg');
INSERT INTO official_tags (k) VALUES ('name:ar');
INSERT INTO official_tags (k) VALUES ('name:af');
INSERT INTO official_tags (k) VALUES ('name:bo');
INSERT INTO official_tags (k) VALUES ('name:br');
INSERT INTO official_tags (k) VALUES ('name:de');
INSERT INTO official_tags (k) VALUES ('name:fr');
INSERT INTO official_tags (k) VALUES ('name:ht');
INSERT INTO official_tags (k) VALUES ('name:pt');
INSERT INTO official_tags (k) VALUES ('oneway');
INSERT INTO official_tags (k) VALUES ('place');
INSERT INTO official_tags (k) VALUES ('power');
INSERT INTO official_tags (k) VALUES ('source');

INSERT INTO official_tags (k) VALUES ('wikipedia:de');
--
-- Below official keys used in OIM context
--
INSERT INTO official_tags (k) VALUES ('humanitarian_use');
INSERT INTO official_tags (k) VALUES ('tap');
INSERT INTO official_tags (k) VALUES ('tap_open');
INSERT INTO official_tags (k) VALUES ('tap_closed');