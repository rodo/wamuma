--                                                                                                                    
SET client_min_messages = error;
--
--
--    
--
--
DROP VIEW IF EXISTS countries;

CREATE VIEW countries as (
       SELECT c.dir as  continent, cc.dir as country, cc.priority, country_url.url
       FROM continent as c, country as cc 
       LEFT JOIN country_url ON cc.id = country_url.country_id
       WHERE c.id=cc.continent_id AND cc.priority > 0
       ORDER BY priority DESC
);
--
--
DROP VIEW IF EXISTS active_continents;

CREATE VIEW active_continents as (
       SELECT distinct(c.dir) as  continent, c.url as url
       FROM continent as c, country as cc 
       WHERE c.id=cc.continent_id AND cc.priority > 0
       GROUP BY c.dir, c.url
);



--
--
DROP VIEW IF EXISTS langs;

CREATE VIEW langs as (
       SELECT cc.id, cc.name as country, l.lang_code
       FROM country as cc, country_lang as l
       WHERE cc.id = l.country_id
       ORDER BY cc.name ASC, l.lang_code
);

--
--
DROP VIEW IF EXISTS countries_by_priority;

CREATE VIEW countries_by_priority as (
       SELECT c.name as continent, cc.name as country, cc.priority, c.dir || '/' || cc.dir as directory
       FROM continent as c, country as cc 
       WHERE c.id=cc.continent_id AND cc.priority > 0
       ORDER BY priority DESC, cc.name ASC
);


--
--
DROP VIEW IF EXISTS countries_by_name;

CREATE VIEW countries_by_name as (
       SELECT c.name as continent, cc.name as country, cc.priority, c.dir || '/' || cc.dir as directory
       FROM continent as c, country as cc 
       WHERE c.id=cc.continent_id AND cc.priority > 0
       ORDER BY cc.name ASC
);

DROP VIEW IF EXISTS countries_by_continent;

CREATE VIEW countries_by_continent as (
       SELECT c.name as continent, cc.name as country, cc.priority, c.dir || '/' || cc.dir as directory
       FROM continent as c, country as cc 
       WHERE c.id=cc.continent_id AND cc.priority > 0
       ORDER BY c.name ASC, cc.name ASC
);


DROP VIEW IF EXISTS lastgeneration;

CREATE VIEW lastgeneration as (
       SELECT name, priority, genend as end, genend - genbegin as duration
       FROM country
       ORDER BY genend DESC
);

DROP VIEW IF EXISTS specialurls;

CREATE VIEW specialurls as (
       SELECT c.name, c.priority, cu.url
       FROM country as c, country_url as cu
       WHERE c.id = cu.country_id
       ORDER BY c.name ASC
);




DROP VIEW IF EXISTS towns ;

CREATE OR REPLACE VIEW towns AS (

       SELECT e.dir as continent, c.dir as country, r.dir as region, d.dir as departement, t.dir as town, t.relation

       FROM continent as e,
       	    country as c,
       	    region as r,
       	    departement as d,
	    town as t

       WHERE e.id = c.continent_id
       AND   c.id = r.country_id
       AND   r.id = d.region_id
       AND   d.id = t.departement_id

);
--
--
DROP VIEW IF EXISTS departements ;

CREATE OR REPLACE VIEW departements AS (

       SELECT e.dir as continent, c.dir as country, r.dir as region, d.dir as departement, d.relation

       FROM continent as e,
       	    country as c,
       	    region as r,
       	    departement as d

       WHERE e.id = c.continent_id
       AND   c.id = r.country_id
       AND   r.id = d.region_id
);

--
--
DROP VIEW IF EXISTS regions ;

CREATE OR REPLACE VIEW regions AS (

       SELECT e.dir as continent, c.dir as country, r.dir as region, r.relation, r.urlsource

       FROM continent as e,
       	    country as c,
       	    region as r

       WHERE e.id = c.continent_id
       AND   c.id = r.country_id
);

