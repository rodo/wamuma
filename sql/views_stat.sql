DROP VIEW IF EXISTS node_tagsub;

CREATE VIEW node_tagsub AS (
       SELECT t.src, t.dst, s.node_id as osmid FROM stat_nodes AS s, tag_substitution as t
       WHERE s.k = t.src 
       	     AND s.fixed = false 
	     AND s.got = false
       ORDER BY  s.k ASC
);

DROP VIEW IF EXISTS way_tagsub;

CREATE VIEW way_tagsub AS (
       SELECT t.src, t.dst, s.node_id as osmid FROM stat_ways AS s, tag_substitution as t
       WHERE s.k = t.src
       	     AND s.fixed = false  
       	     AND s.got = false
       ORDER BY  s.k ASC
);
--
-- TODO REMOVE *ed VIEWS
--
DROP VIEW IF EXISTS wmm_orphaned_nodes;

CREATE VIEW wmm_orphaned_nodes AS (
       SELECT count(id) FROM nodes  
       LEFT JOIN  way_nodes on node_id = id 
       WHERE way_id IS NULL AND array_dims(akeys(tags)) = '[1:0]'
);

DROP VIEW IF EXISTS wmm_duplicated_nodes;

CREATE VIEW wmm_duplicated_nodes AS (

       SELECT  geom, count(*)  from nodes group by geom order by count(*) desc

);

DROP VIEW IF EXISTS wmm_nodes_duplicate;

CREATE VIEW wmm_nodes_duplicate AS (

       SELECT  geom, count(*) as nb
       FROM nodes 
       GROUP BY geom ORDER BY count(*) desc

);


DROP VIEW IF EXISTS wmm_nodes_orphan;

CREATE VIEW wmm_nodes_orphan AS (
       SELECT id, version, user_id, tstamp, changeset_id, geom  FROM nodes  
       LEFT JOIN  way_nodes ON node_id = id 
       WHERE way_id IS NULL AND tags::text = ''
);

--
--

DROP VIEW IF EXISTS wmm_users_nbnodes;

CREATE VIEW wmm_users_nbnodes AS (
       SELECT u.id, u.name, count(n.id) as nbnodes 
       FROM users as u, nodes  as n
       WHERE n.user_id = u.id
       GROUP BY u.id, u.name
);


DROP VIEW IF EXISTS duplicate_shelter;
CREATE VIEW duplicate_shelter AS (

     SELECT stat_nodes.node_id, nodes.lon, nodes.lat, nodes.cc FROM 
       	      (
       	      SELECT lon,lat, count(*) as cc FROM stat_nodes where k = 'humanitarian_use' and v='disaster_shelter' GROUP BY lon,lat ORDER BY count(*) desc 
	      ) as nodes, stat_nodes 

     WHERE nodes.lon = stat_nodes.lon 
     	   AND nodes.lat = stat_nodes.lat 
	   AND nodes.cc > 1 

     GROUP BY stat_nodes.node_id, nodes.lon, nodes.lat, nodes.cc 
     ORDER BY nodes.lon
);

DROP VIEW IF EXISTS duplicate_humanitarian;
CREATE VIEW duplicate_humanitarian AS (

     SELECT stat_nodes.node_id, nodes.lon, nodes.lat, nodes.cc FROM 
       	      (
       	      SELECT lon,lat, count(*) as cc FROM stat_nodes where k = 'humanitarian_use' GROUP BY lon,lat ORDER BY count(*) desc 
	      ) as nodes, stat_nodes 

     WHERE nodes.lon = stat_nodes.lon 
     	   AND nodes.lat = stat_nodes.lat 
	   AND nodes.cc > 1 

     GROUP BY stat_nodes.node_id, nodes.lon, nodes.lat, nodes.cc 
     ORDER BY nodes.lon
);

DROP VIEW IF EXISTS duplicate_ways;
CREATE VIEW duplicate_ways AS (

     SELECT ways.id, cways.cc FROM 
       	      (
       	      SELECT linestring, count(*) as cc FROM ways GROUP BY linestring ORDER BY count(*) desc 
	      ) AS cways, ways

     WHERE cways.linestring = ways.linestring
	   AND cways.cc > 1 

     GROUP BY ways.id, cways.cc
     ORDER BY cways.cc DESC
);



