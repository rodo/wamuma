-- Auteur : Rodolphe Quiédeville
--
SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = error;
SET escape_string_warning = off;

--
-- Vues systemes
-- 
-- base : toutes les bases

DROP VIEW IF EXISTS vs_table_size ;

CREATE OR REPLACE VIEW vs_table_size AS (
       SELECT pc.relname, pg_size_pretty(relpages::bigint * 8 * 1024) as size,
       	      reltuples::bigint as rows, relpages, relfilenode, ps.last_vacuum
       FROM pg_class as pc , pg_stat_user_tables as ps
       WHERE pc.relname = ps.relname 
       ORDER BY relpages DESC, relname ASC
);

COMMENT ON VIEW vs_table_size IS 'Vue système de volumétrie des tables';
--
--
--
DROP VIEW IF EXISTS vs_table_stat ;

CREATE OR REPLACE VIEW vs_table_stat AS (
       SELECT relname, seq_scan, seq_tup_read, idx_scan as is_scan, 
       	      idx_tup_fetch as ix_fetc, n_tup_ins as t_ins, n_tup_upd as t_up, n_tup_del as t_del, n_tup_hot_upd, n_live_tup, n_dead_tup
       FROM pg_stat_user_tables
       ORDER BY relname ASC
);

COMMENT ON VIEW vs_table_stat IS 'Vue système d''utilisation des tables';
--
--
--
DROP VIEW IF EXISTS vs_table_vacuum ;

CREATE OR REPLACE VIEW vs_table_vacuum AS (
       SELECT relname, 
       	      last_vacuum, last_autovacuum, last_analyze, last_autoanalyze
       FROM pg_stat_user_tables
       ORDER BY relname ASC
);

COMMENT ON VIEW vs_table_vacuum IS 'Vue système d''administration des tables';
