--                                                                                                                    
SET client_min_messages = error;
--
--1,Bretagne,-5.16,48.90,-1.00,47.45
--1,Lorient,-3.42,47.78,-3.29,47.71  
--1,Rennes,-1.73,48.17,-1.58,48.07
--1,Nantes,-1.66,47.32,-1.45,47.12
--1,Brest,-4.56,48.44,-4.42,48.36
--1,Chambery,5.887,45.59,5.969,45.54
--1,Strasbourg,7.67,48.64,7.80,48.52



--
-- Data for Name: country; Type: TABLE DATA; Schema: public; Owner: rodo
--



INSERT INTO continent (name,dir) VALUES ('Europe','europe');

INSERT INTO country (name, dir, continent_id)  VALUES ('France','france',  (SELECT currval('continent_id_seq')));
--
--
--
INSERT INTO region (name, dir, country_id)  VALUES ('Bretagne','bretagne',  (SELECT currval('country_id_seq')));

INSERT INTO departement (name, dir, region_id, relation)  VALUES ('Finistère','finistere',  (SELECT currval('region_id_seq')), 102430);

INSERT INTO town (name, dir, departement_id, relation)  VALUES ('Brest','brest',  (SELECT currval('departement_id_seq')), 1076124);
INSERT INTO town (name, dir, departement_id, relation)  VALUES ('Bohars','bohars',  (SELECT currval('departement_id_seq')), 1112845);
INSERT INTO town (name, dir, departement_id, relation)  VALUES ('Gouesnou','gouesnou',  (SELECT currval('departement_id_seq')), 1189517);
INSERT INTO town (name, dir, departement_id, relation)  VALUES ('Guipavas','guipavas',  (SELECT currval('departement_id_seq')), 1189520);

INSERT INTO town (name, dir, departement_id, relation)  VALUES ('Guilers','guilers',  (SELECT currval('departement_id_seq')), 1112848);
INSERT INTO town (name, dir, departement_id, relation)  VALUES ('Plouzané','plouzane',  (SELECT currval('departement_id_seq')), 1112843);
INSERT INTO town (name, dir, departement_id, relation)  VALUES ('Le Relecq-Kerhuon','le-relecq-kerhuon',  (SELECT currval('departement_id_seq')), 87221);

INSERT INTO town (name, dir, departement_id, relation)  VALUES ('Plougastel-Daoulas','plougastel-daoulas',  (SELECT currval('departement_id_seq')), 85684);

INSERT INTO town (name, dir, departement_id, relation)  VALUES ('Concarneau','concarneau',  (SELECT currval('departement_id_seq')), 166492);
INSERT INTO town (name, dir, departement_id, relation)  VALUES ('Quimper','quimper',  (SELECT currval('departement_id_seq')), 296095);
INSERT INTO town (name, dir, departement_id, relation)  VALUES ('Quimperlé','quimperle',  (SELECT currval('departement_id_seq')), 75600);
INSERT INTO town (name, dir, departement_id, relation)  VALUES ('Morlaix','morlaix',  (SELECT currval('departement_id_seq')), 297387);
--
INSERT INTO departement (name, dir, region_id, relation)  VALUES ('Morbihan','morbihan',  (SELECT currval('region_id_seq')), 7447);

INSERT INTO town (name, dir, departement_id, relation)  VALUES ('Auray','auray',  (SELECT currval('departement_id_seq')), 140383);
INSERT INTO town (name, dir, departement_id, relation)  VALUES ('Lorient','lorient',  (SELECT currval('departement_id_seq')), 30305);
INSERT INTO town (name, dir, departement_id, relation)  VALUES ('Plumergat','plumergat',  (SELECT currval('departement_id_seq')), 140419);
INSERT INTO town (name, dir, departement_id, relation)  VALUES ('Pluneret','pluneret',  (SELECT currval('departement_id_seq')), 140404);
INSERT INTO town (name, dir, departement_id, relation)  VALUES ('Vannes','vannes',  (SELECT currval('departement_id_seq')), 192306);
--

INSERT INTO departement (name, dir, region_id, relation)  VALUES ('Ille et Vilaine','ille-et-vilaine',  (SELECT currval('region_id_seq')), 7465);

INSERT INTO town (name, dir, departement_id, relation)  VALUES ('Rennes','rennes',  (SELECT currval('departement_id_seq')), 54517);

--
--
--
INSERT INTO region (name, dir, country_id)  VALUES ('Pays de la Loire','pays-de-la-loire',  (SELECT currval('country_id_seq')));

INSERT INTO departement (name, dir, region_id, relation)  VALUES ('Loire Atlantique','loire-atlantique',  (SELECT currval('region_id_seq')), 7432);

INSERT INTO town (name, dir, departement_id, relation)  VALUES ('Nantes Métropôle','nantes-metropole',  (SELECT currval('departement_id_seq')), 420326);

INSERT INTO town (name, dir, departement_id, relation)  VALUES ('Bouaye','bouaye',  (SELECT currval('departement_id_seq')), 68343);
INSERT INTO town (name, dir, departement_id, relation)  VALUES ('Bouguenais','bouguenais',  (SELECT currval('departement_id_seq')), 65561);
INSERT INTO town (name, dir, departement_id, relation)  VALUES ('Brains','brains',  (SELECT currval('departement_id_seq')), 71170);
INSERT INTO town (name, dir, departement_id, relation)  VALUES ('Carquefou','carquefou',  (SELECT currval('departement_id_seq')), 68336);
INSERT INTO town (name, dir, departement_id, relation)  VALUES ('Indre','indre',  (SELECT currval('departement_id_seq')), 65571);

INSERT INTO town (name, dir, departement_id, relation)  VALUES ('Nantes','nantes',  (SELECT currval('departement_id_seq')), 59874);
INSERT INTO town (name, dir, departement_id, relation)  VALUES ('Mauves-sur-Loire','mauves-sur-loire',  (SELECT currval('departement_id_seq')), 77011);
INSERT INTO town (name, dir, departement_id, relation)  VALUES ('Saint-Herblain','saint-herblain',  (SELECT currval('departement_id_seq')), 64362);
INSERT INTO town (name, dir, departement_id, relation)  VALUES ('Vertou','vertou',  (SELECT currval('departement_id_seq')), 63642);
INSERT INTO town (name, dir, departement_id, relation)  VALUES ('Sautron','sautron',  (SELECT currval('departement_id_seq')), 74611);

INSERT INTO town (name, dir, departement_id, relation)  VALUES ('Orvault','orvault', (SELECT currval('departement_id_seq')), 60972);
INSERT INTO town (name, dir, departement_id, relation)  VALUES ('Rezé','reze', (SELECT currval('departement_id_seq')), 60966);
INSERT INTO town (name, dir, departement_id, relation)  VALUES ('Basse-Goulaine','basse-goulaine', (SELECT currval('departement_id_seq')), 61000);
INSERT INTO town (name, dir, departement_id, relation)  VALUES ('Couëron','coueron', (SELECT currval('departement_id_seq')), 77288);
INSERT INTO town (name, dir, departement_id, relation)  VALUES ('La Chapelle-sur-Erdre','la-chapelle-sur-erdre', (SELECT currval('departement_id_seq')), 73554);

INSERT INTO town (name, dir, departement_id, relation)  VALUES ('La Montagne','la-montagne', (SELECT currval('departement_id_seq')), 63653);
INSERT INTO town (name, dir, departement_id, relation)  VALUES ('Le Pellerin','le-pellerin', (SELECT currval('departement_id_seq')), 77709);
INSERT INTO town (name, dir, departement_id, relation)  VALUES ('Les Sorinières','les-sorinieres', (SELECT currval('departement_id_seq')), 77714);
INSERT INTO town (name, dir, departement_id, relation)  VALUES ('Saint-Aignan-de-Grand-Lieu','saint-aignan-de-grand-lieu', (SELECT currval('departement_id_seq')), 77717);
INSERT INTO town (name, dir, departement_id, relation)  VALUES ('Saint-Jean-de-Boiseau','saint-jean-de-boiseau', (SELECT currval('departement_id_seq')), 74608);

INSERT INTO town (name, dir, departement_id, relation)  VALUES ('Saint-Léger-les-Vignes','saint-leger-les-vignes', (SELECT currval('departement_id_seq')), 77723);
INSERT INTO town (name, dir, departement_id, relation)  VALUES ('Saint-Sébastien-sur-Loire','saint-sebastien-sur-loire', (SELECT currval('departement_id_seq')), 61051);
INSERT INTO town (name, dir, departement_id, relation)  VALUES ('Sainte-Luce-sur-Loire','sainte-luce-sur-loire', (SELECT currval('departement_id_seq')), 61057);
INSERT INTO town (name, dir, departement_id, relation)  VALUES ('Thouaré-sur-Loire','thouare-sur-loire', (SELECT currval('departement_id_seq')), 74604);
--
--
--
INSERT INTO region (name, dir, country_id)  VALUES ('Basse-Normandie','basse-normandie',  (SELECT currval('country_id_seq')));

INSERT INTO departement (name, dir, region_id, relation)  VALUES ('Manche','manche',  (SELECT currval('region_id_seq')), 7404);

INSERT INTO town (name, dir, departement_id, relation)  VALUES ('Cherbourg-Octeville','cherbourg-octeville',  (SELECT currval('departement_id_seq')), 9070);
