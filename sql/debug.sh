#!/bin/sh

# Author : Rodolphe Quiédeville <rodolphe@quiedeville.org>
# Create the main database for wamuma.py and load all sql files
#
# This file is purpose for debug only
#
createdb osm
psql -d osm -f tables.sql
psql -d osm -f views.sql
psql -d osm -f datas.sql
