-- -*- coding: utf-8 -*-
--
-- Copyright (c) 2011 Rodolphe Quiédeville <rodolphe@quiedeville.org>
--
--     This program is free software: you can redistribute it and/or modify
--     it under the terms of the GNU General Public License as published by
--     the Free Software Foundation, either version 3 of the License, or
--     (at your option) any later version.
--
--     This program is distributed in the hope that it will be useful,
--     but WITHOUT ANY WARRANTY; without even the implied warranty of
--     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--     GNU General Public License for more details.
--
--     You should have received a copy of the GNU General Public License
--     along with this program.  If not, see <http://www.gnu.org/licenses/>.

-- All the views must select the object id as osm_id in first, the geom as
--  second and can support a count
-- on them, they must not return all objects
--
-- They are named as wmc_ways_ or wmc_nodes_

--
-- Checking views
--
DROP VIEW IF EXISTS wmc_ways_surface;

CREATE VIEW wmc_ways_surface AS (
       SELECT id as osm_id, linestring as geom, st_area(  st_makepolygon(linestring)  ) as surface
       FROM ways 
       WHERE st_isclosed(linestring)='t' AND st_numpoints(linestring) > 3
       AND  st_area(  st_makepolygon(linestring)  ) < 1e-10
       ORDER by st_area(  st_makepolygon(linestring)  ) ASC
);
COMMENT ON VIEW wmc_ways_surface IS 'Ways order by small surface';
--
--
--
DROP VIEW IF EXISTS wmc_ways_building_open;
CREATE VIEW wmc_ways_building_open AS (
       SELECT id as osm_id, linestring as geom  FROM ways
       WHERE st_isclosed(linestring) = 'f' 
       AND tags::text like '%"building"=>%'
);
COMMENT ON VIEW wmc_ways_building_open IS 'Building with an open area';


CREATE TABLE t_ways_building_open

--
--
--
DROP VIEW IF EXISTS wmc_ways_highways_with_two_nodes;
CREATE VIEW wmc_ways_highways_with_two_nodes AS (
       SELECT id as osm_id, linestring as geom  FROM ways
       WHERE st_numpoints(linestring) = 2
       AND tags::text like '%"highway"=>%'
);
COMMENT ON VIEW wmc_ways_highways_with_two_nodes IS 'Highways with only two nodes';

--
--
--
