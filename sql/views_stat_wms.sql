--
-- Views used by auto stats
--
-- The view's name must begin by wms_
-- The first field is the statistic key name, and the second the value
--
--
--
DROP VIEW IF EXISTS wms_highway_length_sum_by_values;

CREATE VIEW wms_highway_length_sum_by_values AS (

       SELECT s.v, sum(st_length(st_transform(linestring, 3948))) / 1000
       FROM ways as w, stat_ways as s
       WHERE w.id = s.node_id
       AND s.k = 'highway'
       GROUP BY s.v
);

--
-- Count the places
--
DROP VIEW IF EXISTS wms_nodes_place;

CREATE VIEW wms_nodes_place AS (

       SELECT v, count(*)
       FROM stat_nodes 
       WHERE k = 'place' 
       GROUP BY v
);



--
--
--
DROP VIEW IF EXISTS wms_nodes_keys;

CREATE VIEW wms_nodes_keys AS (

       SELECT s.k, count(*)
       FROM stat_nodes as s
       GROUP BY s.k
);
--
--
--
DROP VIEW IF EXISTS wms_ways_keys;

CREATE VIEW wms_ways_keys AS (

       SELECT s.k, count(*)
       FROM stat_ways as s
       GROUP BY s.k
);
