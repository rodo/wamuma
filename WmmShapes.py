# -*- coding: utf-8 -*-
#
# Copyright (c) 2011 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""Shapes generation"""
import os
import threading

import LibExosm
import LibHDM

class GenShapes(threading.Thread):
    """Generate shapes files"""

    _stylesloaded = False
    _styles = []
    _stylesdir = "styles/"

    def __init__(self, conf=None, country=None):
        """Initialization

        Parms :
         - conf : (object WmmConfig)
         - country : (object WmmCountry)

        """
        threading.Thread.__init__(self)
        if conf != None:
            self.conf = conf
            if conf.styldir != None:
                self._stylesdir = conf.styldir
        if not self._stylesdir.endswith('/'):
            self._stylesdir = self._stylesdir + '/'
        if country != None:
            self.country = country
            self.continent = country.continent


    def run(self):
        """Generate shape files for all styles presents in stylesdir"""

        dirname = os.path.join(self.conf.wwwdir, 
                               LibExosm.dircountries(self.continent, 
                                                     self.country.dirname))
        self.shapesfromstyle(dirname)
        self.shapesfromsql(dirname)


    def shapesfromstyle(self, dirname):
        """Generate shape files from style"""
        for style in readstyles(self._stylesdir):
            LibHDM.hdm(dirname, self.country, self.conf,
                       self._stylesdir + style + ".style",
                       style)


    def shapesfromsql(self, dirname):
        """Generate shapefiles from sql query"""
        LibHDM.hdmsql(dirname, self.country, self.conf)


    def getstyles(self, path=None):
        """Read the styles list from directory

        Return an array of style names
        """
        fpath = self._stylesdir
        if path != None:
            fpath = path
        if not self._stylesloaded:
            self._styles = readstyles(fpath)
            self._stylesloaded = True
        return self._styles


def readstyles(path):
    """Return the list of style file in a directory"""
    files = []
    for infile in os.listdir(path):
        if filterfile(infile) and not os.path.isdir(infile):
            files.append(infile[:-6])
    return files


def filterfile(filename):
    """Check if a filename has the right extension"""
    if (len(filename) > 6) and (filename.endswith('.style')):
        return True
    else:
        return False
