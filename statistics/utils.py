# -*- coding: utf-8 -*-
#
# Copyright (c) 2011 Rodolphe Quiédeville <rodolphe@quiedeville.org>
# Copyright (c) 2011 Pilot Systems <contact@pilotsystems.net>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""Generate statistics in sqlite files from views present in a postgresql 
database
"""

from time import time
import os
import sqlite3

def stats_auto(odb, path):
    """Read the views names and run stats on them"""
    views = stat_listviews(odb)
    stats_run_auto(odb, views, path)
    
def stats_run_auto(odb, views, path):
    """Run the stat for each view"""
    for view in views:
        qry = "SELECT * FROM %s" % view
        datas = odb.fetchquery(qry)
        store_lite(path, view, datas)

def stat_listviews(odb):
    """Return an array of the view names present in a database"""
    views = []
    qry = """SELECT viewname FROM pg_catalog.pg_views """
    qry = qry + """ WHERE viewname like 'wms_%'"""
    for row in odb.fetchquery(qry):
        views.append(row[0])
    return views

def stat_read_datas(odb, qry):
    """Return the datas"""
    rows = odb.fetchquery(qry)
    return rows

def store_lite(path, name, datas):
    """Store datas in a sqlite database """
    dbname = os.path.join(path, "%s.db" % name[4:])
    if not os.path.exists(dbname):
        store_create_db(dbname)
    if os.path.exists(dbname):
        store_infile(dbname, datas)

def store_infile(fpath, datas):
    """Store the values in a sqlite file"""
    conn = sqlite3.connect(fpath)
    cur = conn.cursor()
    tstamp = int(time())
    for data in datas:
        try:
            qry = "INSERT INTO stat VALUES (?, ?, ?)"
            cur.execute(qry, (tstamp, data[0] , data[1]) )
        except sqlite3.Error, err:
            print "An error occurred:", err.args[0]

    conn.commit()
    cur.close()

def store_create_db(fpath):
    """Create a sqlite db"""
    qry = '''create table stat (date int, key text, value real)'''        
    try:
        conn = sqlite3.connect(fpath)
        cur = conn.cursor()
        cur.execute(qry)
        conn.commit()
        cur.close()
        return 0
    except sqlite3.Error, err:
        print "An error occurred:", err.args[0]
        return 1
