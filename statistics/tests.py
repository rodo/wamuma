#! /usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2011 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""Tests for tagfix.py command line tool"""
import os
import unittest

import utils
import sqlite3

class utilsTests(unittest.TestCase):
    """Test class"""
    def setUp (self):
        for fi in ['/dev/shm/foo.sqlite', '/dev/shm/bar.db']:
            if os.path.exists(fi):
                os.unlink(fi)


    def test_store_create_db(self):
        """Test if we can load the class"""
        fpath = '/dev/shm/foo.sqlite'
        self.assertFalse( os.path.exists(fpath) )
        res = utils.store_create_db(fpath)
        self.assertTrue(res == 0, "Version number")
        self.assertTrue(os.path.exists(fpath), 'File does not exists')

    def test_store_infile_onevalue(self):
        """Test if we can load the class"""
        datas = [ ('foo',12.3),
                  ('bar',12) ]
        fpath = '/dev/shm/foo.sqlite'
        res = utils.store_create_db(fpath)
        res = utils.store_infile(fpath, datas)
        self.assertTrue(os.path.exists(fpath), 'File does not exists')

        conn = sqlite3.connect(fpath)
        c = conn.cursor()
        c.execute("select sum(value) from stat")
        for row in c:
            self.assertTrue(row[0] == 24.3, 'All the values were not right stored')
        conn.close()

    def test_store_infile_twovalues(self):
        """Test if we can load the class"""
        datas = [ ('foo',12.333) ]
        fpath = '/dev/shm/foo.sqlite'
        res = utils.store_create_db(fpath)
        res = utils.store_infile(fpath, datas)
        self.assertTrue(os.path.exists(fpath), 'File does not exists')

        conn = sqlite3.connect(fpath)
        c = conn.cursor()
        c.execute("select sum(value) from stat")
        for row in c:
            self.assertTrue(row[0] == 12.333, 'All the values were not right stored')
        conn.close()

    def test_store_lite(self):
        """Store datas in an existente or not db file"""
        datas = [ ('foo',12.333) ]
        path = '/dev/shm/'
        filename = 'wms_bar'
        dbname = os.path.join(path,"bar.db")

        res = utils.store_lite(path, filename, datas)
        self.assertTrue(os.path.exists(dbname), 'File does not exists')

        conn = sqlite3.connect(dbname)
        c = conn.cursor()
        c.execute("select sum(value) from stat")
        for row in c:
            self.assertTrue(row[0] == 12.333, 'All the values were not right stored found %s' % row[0])
        conn.close()


if __name__ == '__main__':
    unittest.main()
